<?
class CP_Www_Widgets_Social_Twitter_Controller extends CP_Common_Lib_WidgetControllerAbstract
{
    var $username = 'cnni';
    var $count = 5;
    var $handle = 'tweets';
    var $showProfileImages   = true;
    var $showUserScreenNames = true;
    var $showUserFullNames   = true;
    var $showActionReply     = true;
    var $showActionRetweet   = true;
    var $showActionFavorite  = true;   
}