<?
class CP_Www_Widgets_Media_NivoSlider_Controller extends CP_Common_Lib_WidgetControllerAbstract
{
    var $speed     = 5;
    var $showNav   = true;
    var $appendCls = '';
    var $handle    = 'slider';
    var $type      = 'picture';
    var $showCaption = true;
    var $controlNav  = true;
    var $directionNav = true;
    var $effect       = 'random';

    /** options:  **/
    var $theme     = '';
}