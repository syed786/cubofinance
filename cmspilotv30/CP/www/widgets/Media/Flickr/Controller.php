<?
class CP_Www_Widgets_Media_Flickr_Controller extends CP_Common_Widgets_Media_Flickr_Controller
{
    var $imageSize = 'square';    // square, large_square, thumbnail, small, small320, medium, medium640, large
    var $zoomImageSize = 'medium';
    var $hidePrimaryImage = false; //if the primary image is shown seperately then make this true

}