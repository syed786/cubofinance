<?
class CP_Www_Widgets_Media_S3Slider_Controller extends CP_Common_Lib_WidgetControllerAbstract
{
    var $speed  = 5;
    var $handle = 's3Slider';
    var $type   = 'banner';
    var $showCaption = false;
    var $sectionId = '';
    var $width  = '';
    var $height = '';
    var $captionLocation = 'bottom';
}