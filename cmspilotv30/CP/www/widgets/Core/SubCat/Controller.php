<?
class CP_Www_Widgets_Core_SubCat_Controller extends CP_Common_Lib_WidgetControllerAbstract
{
    var $ulClass    = '';
    var $surroundUl = true;
    var $sortBy     = 'sc.sort_order';
    var $category_id = '';
    var $categoryType = '';
    var $showThumbForGrid = true;
    var $showDefaultImgForThumb = false;
    var $heading     = '';
    var $includeDescription = false;    
}