<?
$cpCfg = array();

$cpCfg['cp.viewRecType.home.pageCSSClass'] = 'hideboth';
$cpCfg['m.webBasic.content.showCloudTags'] = false;
$cpCfg['m.webBasic.content.showOrphanRecords'] = true;
$cpCfg['m.webBasic.content.showOrphanRecordsMobile'] = false;
$cpCfg['m.webBasic.content.zoomListImages'] = true;
$cpCfg['m.webBasic.content.includeCommentCount'] = false;

//if section room is clicked then all records are displayed by default
$cpCfg['m.webBasic.content.showAllRecordsByDefault'] = false;

$cpCfg['m.webBasic.gallery.slideshowWidth'] = 750;
$cpCfg['m.webBasic.gallery.slideshowHeight'] = 500;

$cpCfg['m.webBasic.content.showAddThis'] = false;

$cpCfg['m.webBasic.contactUs.showEnqFormBelowList'] = false;
$cpCfg['m.webBasic.contactUs.showEnqFormAboveList'] = false;

$cpCfg['m.webBasic.contactUs.hideCountryDropdown'] = false;
$cpCfg['m.webBasic.contactUs.hideCaptcha'] = false;
$cpCfg['m.webBasic.contactUs.hideCancelButton'] = false;
$cpCfg['m.webBasic.contactUs.showEnquiryType'] = false;

return $cpCfg;