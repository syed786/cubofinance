<?
$cpCfg = array();

//------------- PRODUCT -------------//
$cpCfg['m.ecommerce.product.showLatest']         = 1;
$cpCfg['m.ecommerce.product.showFavourite']      = 0;
$cpCfg['m.ecommerce.product.hasRelatedProduct']  = 0;
$cpCfg['m.ecommerce.product.isCountryBased']     = 0;
$cpCfg['m.ecommerce.product.hasProductItem']     = 0;
$cpCfg['m.ecommerce.product.hasCountry']         = 1;
$cpCfg['m.ecommerce.product.hasContentHistory']  = 0;
$cpCfg['m.ecommerce.product.hasSection']         = false;
$cpCfg['m.ecommerce.product.hasSortOrderFld']    = false;
return $cpCfg;
