<?
class CP_Common_Widgets_Common_Language_Controller extends CP_Common_Lib_WidgetControllerAbstract
{
    var $class;
    var $ulClass      = '';
    var $title        = '';
    var $showAsMenu   = false;
    var $hideCurrLang = false;
    var $surroundUl = true;
    var $menuTitle  = 'w.common.language.menuTitle';
    var $moveCurrentLangToEnd = false;
    var $countryId = '';
}
