<?
$cpCfg = array();
$cpCfg['cp.fullWidthTemplte'] = true;
$cpCfg['cp.googleFontsString'] = 'Oswald';
$cpCfg['cp.fixMainPanelHeightInDetail']= true;
$cpCfg['m.common.dashboard.pageCSSClass'] = 'hidecol2';
$cpCfg['cp.imsLoginText'] = false;
$cpCfg['cp.manpowerLoginText'] = false;
$cpCfg['cp.engextTadingLoginText'] = false;
$cpCfg['cp.defaultPageCssClass'] = 'hidecol2';
$cpCfg['p.common.login.adminHasPaypalSubscription'] = false;
$cpCfg['cp.hasAccessModuleOtherAction'] = true;

return $cpCfg;