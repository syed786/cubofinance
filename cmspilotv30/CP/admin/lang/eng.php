<?
$LANGARR['cp.lbl.back'] = "Back";
$LANGARR['cp.lbl.backToList'] = "Back to List";
$LANGARR['cp.pager.next'] = "Next >";
$LANGARR['cp.pager.previous'] = "< Previous";
$LANGARR['cp.form.lbl.pleaseSelect'] = "Please Select";

$LANGARR['cp.form.fld.email.err']    = "Please enter a valid email";
$LANGARR['cp.form.fld.password.err'] = "Please enter a valid password";
$LANGARR['p.member.login.form.err.invalidLogin'] = "Invalid username or password";
$LANGARR['loginIntro'] = "
<p>Welcome to {$cpCfg['cp.frameworkName']} {$cpCfg['cp.version']}</p>
<p>Please use a valid email and password to gain access to the administration console.</p>
";

$LANGARR['cp.lbl.submit']  = "Submit";
$LANGARR['cp.lbl.confirm'] = "Confirm";
$LANGARR['cp.lbl.cancel']  = "Cancel";
$LANGARR['cp.lbl.close']   = "Close";
$LANGARR['cp.lbl.save']    = "Save";

$LANGARR['cp_lbl_updatedSuccessfully'] = "Updated Successfully";
$LANGARR['cp.pager.lbl.totalRecords'] = "Total Records";
$LANGARR['changePassword'] = "Change Password";
