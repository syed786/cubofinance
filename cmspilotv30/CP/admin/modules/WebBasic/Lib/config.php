<?
$cpCfg = array();

//------------ WEB BASIC COMMON --------------//
$cpCfg['m.webBasic.displayShowInNavFld'] = true;

$cpCfg['m.webBasic.cssStyleNamesArr'] = array (
);
//------------ SECTION --------------//
$cpCfg['m.webBasic.section.showExtLinkField'] = 1;
$cpCfg['m.webBasic.section.showIntLinkField'] = 1;
$cpCfg['m.webBasic.section.hasCSSStyleName']  = 0;
$cpCfg['m.webBasic.section.hasMemberOnly']    = 0;
$cpCfg['m.webBasic.section.hasNonMember']     = 0;
$cpCfg['m.webBasic.section.hasBanner']        = 0;
$cpCfg['m.webBasic.section.hasPicture']       = 0;
$cpCfg['m.webBasic.section.showMetaData']     = 1;
$cpCfg['m.webBasic.section.showCountry']      = 0;
$cpCfg['m.webBasic.section.showDescription']  = 0;
$cpCfg['m.webBasic.section.showExtLinkField'] = 1;
$cpCfg['m.webBasic.section.hasCaption']       = 0;
$cpCfg['m.webBasic.section.showOkForWeb']     = 0;
$cpCfg['m.webBasic.section.showOkForMobile']  = 0;
$cpCfg['m.webBasic.section.showSpecialSearch']= 0;
$cpCfg['m.webBasic.section.recordTypeArr']    = array (
     'Content'
    ,'Home'
    ,'Site Search'
    ,'Contact Us'
);

$cpCfg['m.webBasic.section.btnPosArr'] = array (
     'Top'
    ,'Left'
    ,'Other'
);

$cpCfg['m.webBasic.section.specialSearchArr'] = array(
     'Published'
    ,'Not-Published'
    ,'Member Sections'
    ,'Public Sections'
);

$cpCfg['m.webBasic.section.showAdsBannerLink'] = false;
$cpCfg['m.webBasic.section.hasMultiSites'] = false;
$cpCfg['m.webBasic.section.showRefTitle']  = false;
$cpCfg['m.webBasic.section.showAliasToSection'] = false;
$cpCfg['m.webBasic.section.showSectionsFromAllSitesInAliasToSecFld'] = true;
$cpCfg['m.webBasic.section.showNoFollow'] = false;
$cpCfg['m.webBasic.section.hasAdSpaces'] = false;

//------------ CATEGORY -------------//
$cpCfg['m.webBasic.category.showIntLinkField']      = 1;
$cpCfg['m.webBasic.subCategory.hasCategoryFilter']  = 0;
$cpCfg['m.webBasic.category.hasBanner']             = 0;
$cpCfg['m.webBasic.category.hasPicture']            = 0;
$cpCfg['m.webBasic.category.showDescription']       = 0;
$cpCfg['m.webBasic.category.showShortDescription']  = 0;
$cpCfg['m.webBasic.category.showFCKForShortDescription']  = 0;
$cpCfg['m.webBasic.category.hasMemberOnly']         = 0;
$cpCfg['m.webBasic.category.hasNonMemberOnly']      = 0;
$cpCfg['m.webBasic.category.showMetaData']          = 1;
$cpCfg['m.webBasic.category.hasCaption']            = 0;
$cpCfg['m.webBasic.category.showCountry']           = 0;
$cpCfg['m.webBasic.category.hasCSSStyleName']       = 0;
$cpCfg['m.webBasic.category.showOkForWeb']          = 0;
$cpCfg['m.webBasic.category.showOkForMobile']       = 0;
$cpCfg['m.webBasic.category.displayTradingmassProductGroup'] = 0;
$cpCfg['m.webBasic.category.recordTypeArr'] = array (
    'Content'
   ,'Enquiry Form'
);

$cpCfg['m.webBasic.category.showRefTitle'] = false;
$cpCfg['m.webBasic.category.defaultSectionType'] = '';
$cpCfg['m.webBasic.category.showNoFollow'] = false;

//--------- SUB CATEGORY ------------//
$cpCfg['m.webBasic.subCategory.defaultSectionType']                 = '';
$cpCfg['m.webBasic.subCategory.hasBanner']                          = 0;
$cpCfg['m.webBasic.subCategory.hasPicture']                         = 0;
$cpCfg['m.webBasic.subCategory.hasPortal']                          = 1;
$cpCfg['m.webBasic.subCategory.showDescription']                    = 0;
$cpCfg['m.webBasic.subCategory.showMetaData']                       = 1;
$cpCfg['m.webBasic.subCategory.showDescription']                    = false;
$cpCfg['m.webBasic.subCategory.showShortDescription']               = 0;
$cpCfg['m.webBasic.subCategory.showFCKShortDescription']            = 0;
$cpCfg['m.webBasic.subCategory.showCountry']                        = 0;
$cpCfg['m.webBasic.subCategory.showExtLinkField']                   = 1;
$cpCfg['m.webBasic.subCategory.showIntLinkField']                   = 1;
$cpCfg['m.webBasic.subCategory.showOkForWeb']                       = 0;
$cpCfg['m.webBasic.subCategory.showOkForMobile']                    = 0;
$cpCfg['m.webBasic.subCategory.hasMemberOnly']                      = 0;
$cpCfg['m.webBasic.subCategory.hasNonMemberOnly']                   = 0;
$cpCfg['m.webBasic.subCategory.recordTypeArr'] = array (
     'Content'
    ,'Enquiry Form'
    ,'Regisration'
);

$cpCfg['m.webBasic.subCategory.showRefTitle'] = false;
$cpCfg['m.webBasic.subCategory.hasCategory2'] = false;
$cpCfg['m.webBasic.subCategory.hasCategory3'] = false;
$cpCfg['m.webBasic.subCategory.showNoFollow'] = false;
//-------------- CONTENT ------------//
$cpCfg['m.webBasic.content.showExtLinkField']   = 1;
$cpCfg['m.webBasic.content.showIntLinkField']   = 1;
$cpCfg['m.webBasic.content.showShortDesc']      = 0;
$cpCfg['m.webBasic.content.showFCKShortDesc']   = 0;
$cpCfg['m.webBasic.content.showLatest']         = 1;
$cpCfg['m.webBasic.content.showFavourite']      = 0;
$cpCfg['m.webBasic.content.showMemberOnly']     = 0;
$cpCfg['m.webBasic.content.showMetaData']       = 0;
$cpCfg['m.webBasic.content.showCloudTags']      = 0;
$cpCfg['m.webBasic.content.showAuthor']         = 0;
$cpCfg['m.webBasic.content.hasPreview']         = 0;
$cpCfg['m.webBasic.content.hasHistory']         = 0;
$cpCfg['m.webBasic.content.hasTab']             = 0;
$cpCfg['m.webBasic.content.hasRelatedContent']  = 0;
$cpCfg['m.webBasic.content.showRecordUrl']      = 0;
$cpCfg['m.webBasic.content.showCountry']        = 0;
$cpCfg['m.webBasic.content.showMemberOnly']     = 0;
$cpCfg['m.webBasic.content.showOtherPicture']   = 0;
$cpCfg['m.webBasic.content.showEmbedCode']      = 0;
$cpCfg['m.webBasic.content.showDownloadBrochure'] = 0;
$cpCfg['m.webBasic.content.showOkForWeb']       = 0;
$cpCfg['m.webBasic.content.showOkForMobile']    = 0;
$cpCfg['m.webBasic.content.showMonthYearFilter']    = 1;
$cpCfg['m.webBasic.content.recordTypeArr']      = array(
     'Record'
    ,'Callout'
    ,'Disclaimer'
    ,'Privacy Policy'
    ,'Social Media Icons'
    ,'Get Started'
);

$cpCfg['m.webBasic.content.showComments']     = false;
$cpCfg['m.webBasic.content.specialSearchArr'] = array(
     'Published'
    ,'Not-Published'
    ,'Latest'
    ,'Favourite'
    ,'Flagged'
    ,'Not-Flagged'
);

$cpCfg['m.webBasic.content.hasRelatedPics']    = 0;
$cpCfg['m.webBasic.content.showCorrespondent'] = 0;
$cpCfg['m.webBasic.content.showJurisdiction']  = 0;
$cpCfg['m.webBasic.content.showRefTitle']      = false;

//------------- ENQUIRY -------------//
$cpCfg['m.webBasic.enquiry.showStaff']          = 0;
$cpCfg['m.webBasic.enquiry.showClientType']     = 0;
$cpCfg['m.webBasic.enquiry.showPrefContactTime']= 0;
$cpCfg['m.webBasic.enquiry.hasCountry']         = 0;
$cpCfg['m.webBasic.enquiry.showCountry']        = 0;
$cpCfg['m.webBasic.enquiry.showCountryCode']    = 0;
$cpCfg['m.webBasic.enquiry.showCountryFilter']  = 0;
$cpCfg['m.webBasic.enquiry.showEnquiryType']    = 0;
$cpCfg['m.webBasic.enquiry.showSubject']        = 0;

return $cpCfg;