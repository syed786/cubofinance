<?
$cpCfg = Zend_Registry::get('cpCfg');
$fn    = Zend_Registry::get('fn');
$modulesArr = Zend_Registry::get('modulesArr');

$dashboard = getCPModuleObj('common_dashboard')->model;

$themePath = CP_THEMES_PATH_LOCAL_ALIAS . $cpCfg['cp.theme'] . '/';

$modulesArr['tradingsg_callRegistry']['title'] = 'Lead';

$arr = array();
$userGroupType = $fn->getSessionParam('userGroupType');

$arr[] = $dashboard->getDasboardObj('tradingsg_dailyCollectionChart', array('cssClass' => 'c100l'));
$arr[] = $dashboard->getDasboardObj('tradingsg_topCustomerOutstanding', array('cssClass' => 'c100l'));
$arr[] = $dashboard->getDasboardObj('tradingsg_salesByMonthChart', array('cssClass' => 'c100l'));

$cpCfg['cp.dashboardArr'] = $arr;

$cssFilesArr = array();
$cssFilesArr[] = $themePath.'css/bootstrap.min.css';
$cssFilesArr[] = $themePath.'css/bootstrap-theme.min.css';
$jsFilesArr = array();
$jsFilesArr[] = $themePath.'js/bootstrap-modal.js';
$jsFilesArr[] = $themePath.'js/jquery.min.js';
$jssKeys = array('fontAwesome-4.3.0');

CP_Common_Lib_Registry::arrayMerge('jsFilesArr', $jsFilesArr);
CP_Common_Lib_Registry::arrayMerge('jssKeys', $jssKeys);
CP_Common_Lib_Registry::arrayMerge('cssFilesArr', $cssFilesArr);
CP_Common_Lib_Registry::arrayMerge('cpCfg', $cpCfg);
CP_Common_Lib_Registry::arrayMerge('modulesArr', $modulesArr);
