<?
$cpCfg = array();
$cpCfg['cp.theme'] = 'Angle';
$cpCfg['cp.jqVersion'] = '1.8.2';
$cpCfg['cp.jqUiVersion'] = '1.9.2';
$cpCfg['cp.tradingLoginText'] = 1;
$cpCfg['cp.hasAccessModule'] = true;
$cpCfg['cp.assetVersion'] = '100';
//$cpCfg['cp.multiLang'] = true;

$cpCfg['cp.availableLanguages'] = array(
    // ONlY TWO LANGUAGES ARE ADDED.//
     'eng' => 'English'
    ,'tam' => 'Tamil'
);

$cpCfg['cp.hasAdminInterfaceLangs'] = true;
$cpCfg['cp.adminInterfaceLangs'] = array(
    'eng' => 'English',
    'tam' => 'Tamil',
);
$cpCfg['cp.topRooms'] = array(
    'order' => array(
        'title' => 'Finance'
       ,'modules' => array(
             'common_dashboard'
            ,'tradingsg_investor'
            ,'tradingsg_customer'
            ,'tradingsg_reports'
       )
       ,'default' => 'tradingsg_customer'
    )

    ,'admin' => array(
        'title' => 'Admin'
       ,'modules' => array(
             'core_userGroup'
            ,'core_staff'
            //,'tradingsg_staffAttendance'
            //,'webBasic_category'
            //,'webBasic_subCategory'
            //,'webBasic_content'
            ,'core_valuelist'
            ,'core_setting'
            //,'core_adminTranslation'
            ,'core_translation'
       )
       ,'default' => 'core_staff'
    )
);

$hiddenModules = array(
     'common_contactLink'
    ,'common_testRecipientLink'
    ,'common_interestLink'
    ,'ecommerce_orderItemLink'
    ,'ecommerce_product'
    ,'tradingsg_contactLink'
 );

$tmpName = &$cpCfg['cp.topRooms'];
$cpCfg['cp.availableModules'] = array_merge(
      $tmpName['order']['modules']
    , $tmpName['admin']['modules']
    , $hiddenModules
);

$cpCfg['cp.availableModGroups'] = array(
     'core'
    ,'common'
    ,'webBasic'
    ,'ecommerce'
    ,'tradingsg'
);

$cpCfg['cp.availableWidgets'] = array(
     'tradingsg_dailyCollectionReport'
    ,'tradingsg_investorReport'
    ,'common_adminTranslation'
    ,'tradingsg_dailyCollectionChart'
    ,'tradingsg_topCustomerOutstanding'
    ,'tradingsg_salesByMonthChart'
    ,'tradingsg_financeReport'
);

$cpCfg['cp.availablePlugins'] = array(
     'common_comment'
    ,'common_media'
    ,'common_login'
    ,'member_forgotPassword'
);

return $cpCfg;