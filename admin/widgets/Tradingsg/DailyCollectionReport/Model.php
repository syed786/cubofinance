<?
class CPL_Admin_Widgets_Tradingsg_DailyCollectionReport_Model extends CP_Common_Lib_WidgetModelAbstract
{
    /**
     *
     */
    function getSQL(){
        $cpCfg = Zend_Registry::get('cpCfg');

        $SQL = "
        SELECT c.*
              ,SUM(cp.amount) as amount_paid
              ,cp.date
        FROM company c
        LEFT JOIN (`customer_payment` cp) ON (cp.company_id = c.company_id)
        ";

        return $SQL;
    }

    /**
     *
     */
    function setSearchVar() {
        $fn = Zend_Registry::get('fn');
        $cpCfg = Zend_Registry::get('cpCfg');
        $searchVar = $this->searchVar;
        $searchVar->mainTableAlias = 'c';

        $start_date 	= $fn->getReqParam('start_date');
        $end_date   	= $fn->getReqParam('end_date');
        $month      	= $fn->getReqParam('month');
        $year       	= $fn->getReqParam('year');
        $current_date 	= date('Y-m-d');
        $month        	= date('m');
        $year		  	= date('Y');
        $record_type    = $fn->getReqParam('record_type');
        $location_id    = $fn->getReqParam('location_id');

        if ($start_date != '' && $end_date == '') {
  	        $searchVar->sqlSearchVar[] = "cp.date >= '{$start_date}' AND cp.date <= '{$current_date}'";
        } else if ($start_date == '' && $end_date != ''){
            $start_date = $year . '-' . $month . '-' . '01';
  	        $searchVar->sqlSearchVar[] = "cp.date >= '{$start_date}' AND cp.date <= '{$end_date}'";
        } else if ($start_date != '' && $end_date != '') {
  	        $searchVar->sqlSearchVar[] = "cp.date >= '{$start_date}' AND cp.date <= '{$end_date}'";
        } else {
            $start_date = $year . '-' . $month . '-' . '01';
            $end_date = $year . '-' . $month . '-' . '31';
            $searchVar->sqlSearchVar[] = "cp.date >= '{$current_date}' AND cp.date <= '{$current_date}'";
        }

        $searchVar->groupBy = "c.company_name, cp.date";
        $searchVar->sortOrder = 'cp.date DESC';

    }

    /**
     *
     * @param <type> $SQL
     * @return <type>
     */
    function getDataArray() {
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');

        $modelHelper = Zend_Registry::get('modelHelper');
        $dataArray = $modelHelper->getWidgetDataArray($this->controller, 'tradingsg_dailyCollectionReport');

        $this->dataArray = $dataArray;
        return $this->dataArray;
    }

    /**
     */
    function getExportToExcel(){
        $db = Zend_Registry::get('db');
        $cpCfg = Zend_Registry::get('cpCfg');
        $tv = Zend_Registry::get('tv');
        $cpUtil = Zend_Registry::get('cpUtil');
        $dateUtil = Zend_Registry::get('dateUtil');
        $fn = Zend_Registry::get('fn');

        set_time_limit(50000);
        ini_set('memory_limit', '512M');

        require_once("PHPExcel.php");
        include 'PHPExcel/IOFactory.php';

        $file_name = "DailyCollectionReport_" . date("d-m-Y") . ".xls";

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename={$file_name}");
        header("Content-Transfer-Encoding: binary ");

        $objPHPExcel = new PHPExcel();

        //--------------------------------------------------//
        $rowc = 1;
        $colc = 0;
        $start_date     = $fn->getReqParam('start_date');
        $end_date       = $fn->getReqParam('end_date');
        $month          = $fn->getReqParam('month');
        $year           = $fn->getReqParam('year');
        $current_date   = date('Y-m-d');
        $month          = date('m');
        $year           = date('Y');
        $record_type    = $fn->getReqParam('record_type');
        $actSheet = &$objPHPExcel->getActiveSheet();

        $actSheet->setCellValueByColumnAndRow($colc++, $rowc, 'Date');
        $actSheet->setCellValueByColumnAndRow($colc++, $rowc, 'Customer Name');
        $actSheet->setCellValueByColumnAndRow($colc++, $rowc, 'Amount Paid');
        $actSheet->setCellValueByColumnAndRow($colc++, $rowc, 'Outstanding Amount');

        /******************** FORMAT HEADER *******************/
        $headStyle = array(
            'font' => array('bold' => true)
        );

        $lastCol    = $actSheet->getHighestColumn();
        $lastColInd = PHPExcel_Cell::columnIndexFromString($lastCol);
        $actSheet->getStyle("A1:{$lastCol}1")->applyFromArray($headStyle);

        for ($i=0; $i < $lastColInd; $i++){
            $colAlphabet = PHPExcel_Cell::stringFromColumnIndex($i);
            $actSheet->getColumnDimension($colAlphabet)->setAutoSize(true);
        }

        $discount_sum_percent = 0;
        $discount_sum_value = 0;

        if ($start_date != '' && $end_date == '') {
            $startDateAppendSql = "cp.date >= '{$start_date}' AND cp.date <= '{$current_date}'";
        } else if ($start_date == '' && $end_date != ''){
            $start_date = $year . '-' . $month . '-' . '01';
            $startDateAppendSql = "cp.date >= '{$start_date}' AND cp.date <= '{$end_date}'";
        } else if ($start_date != '' && $end_date != '') {
            $startDateAppendSql = "cp.date >= '{$start_date}' AND cp.date <= '{$end_date}'";
        } else {
            $start_date = $year . '-' . $month . '-' . '01';
            $end_date = $year . '-' . $month . '-' . '31';
            $startDateAppendSql = "cp.date >= '{$current_date}' AND cp.date <= '{$current_date}'";
        }

        $linkToStock = '' ;

        if($cpCfg['cp.excludeStock'] == 1){
            $linkToStock = "AND o.link_stock = 1";
        }

        $siteTitle = '' ;

        if ($cpCfg['cp.hasMultiUniqueSites']  == 1) {
            $siteTitle = ",o.site_id" ;
        }

        $SQL = "
        SELECT c.*
              ,SUM(cp.amount) as amount_paid
              ,cp.date
        FROM company c
        LEFT JOIN (`customer_payment` cp) ON (cp.company_id = c.company_id)
        WHERE
        {$startDateAppendSql}
        GROUP BY c.company_name, cp.date
        ORDER BY cp.date DESC
        ";

        $result = $db->sql_query($SQL);

        $grand_total = 0;
        $grand_totalfrm = 0;

        while ($row = $db->sql_fetchrow($result)) {
            $colc = 0;

            $creationDate = $fn->getCPDate($row['date'],"d-m-Y");

            $rowc++;
            $colc = 0;

            $amount_paid = number_format($row['amount_paid'], 2);
            $dateFormatted = $dateUtil->formatDate($row['date'], 'DD/MM/YYYY');

            if ($start_date != '' && $end_date == '') {
                $startDateAppendSql = "date >= '{$start_date}' AND date <= '{$current_date}'";
            } else if ($start_date == '' && $end_date != ''){
                $start_date = $year . '-' . $month . '-' . '01';
                $startDateAppendSql = "date >= '{$start_date}' AND date <= '{$end_date}'";
            } else if ($start_date != '' && $end_date != '') {
                $startDateAppendSql = "date >= '{$start_date}' AND date <= '{$end_date}'";
            } else {
                $start_date = $year . '-' . $month . '-' . '01';
                $end_date = $year . '-' . $month . '-' . '31';
                $startDateAppendSql = "date >= '{$current_date}' AND date <= '{$current_date}'";
            }

            $SQLcf = "
            SELECT SUM(amount) as outstanding_amount
            FROM customer_finance
            WHERE {$startDateAppendSql}
              AND company_id = {$row['company_id']}
            ";
            $resultcf = $db->sql_query($SQLcf);
            $rowcf = $db->sql_fetchrow($resultcf);

            $SQLPaid = "
            SELECT SUM(amount) as paid_amount
            FROM customer_payment
            WHERE date <= '{$row['date']}'
              AND company_id = {$row['company_id']}
            ";
            $resultPaid = $db->sql_query($SQLPaid);
            $rowPaid = $db->sql_fetchrow($resultPaid);

            $outstanding_amount = $rowcf['outstanding_amount'] - $rowPaid['paid_amount'];
            $outstanding_amount_formatted = number_format($outstanding_amount, 2);

            $actSheet->setCellValueByColumnAndRow($colc++, $rowc, $dateFormatted);
            $actSheet->setCellValueByColumnAndRow($colc++, $rowc, $row['company_name']);
            $actSheet->setCellValueByColumnAndRow($colc++, $rowc, $amount_paid);
            $actSheet->setCellValueByColumnAndRow($colc++, $rowc, $outstanding_amount_formatted);
        }

        $colc = 0;
        $rowc++;

        $rowc++;

        if($cpCfg['cp.hasMultiUniqueSites'] == 1){
            if($location_id ==''){
                $actSheet->getStyle("A{$rowc}:F{$rowc}")->applyFromArray($headStyle);
            }else{
                $actSheet->getStyle("A{$rowc}:C{$rowc}")->applyFromArray($headStyle);
            }
        }else {
            $actSheet->getStyle("A{$rowc}:B{$rowc}")->applyFromArray($headStyle);
        }

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}