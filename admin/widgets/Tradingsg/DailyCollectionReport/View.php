<?
class CPL_Admin_Widgets_Tradingsg_DailyCollectionReport_View extends CP_Common_Lib_WidgetViewAbstract
{
    //==================================================================//
    function getWidget() {
        $db = Zend_Registry::get('db');
        $fn = Zend_Registry::get('fn');
        $dateUtil = Zend_Registry::get('dateUtil');
        $cpCfg = Zend_Registry::get('cpCfg');

	// **** THIS CONDITION HAS BEEN USED ONLY FOR MULTI LOCATION SITE IN BLOSSOMS **** \\
		
        $start_date     = $fn->getReqParam('start_date');
        $end_date       = $fn->getReqParam('end_date');
        $month          = $fn->getReqParam('month');
        $year           = $fn->getReqParam('year');
        $current_date   = date('Y-m-d');
        $month          = date('m');
        $year           = date('Y');

        if ($start_date != '' && $end_date == '') {
            $start_date = $start_date;
            $end_date   = $current_date;
        } else if ($start_date == '' && $end_date != ''){
            $start_date = $year . '-' . $month . '-' . '01';
            $start_date = $start_date;
            $end_date   = $end_date;
        } else if ($start_date != '' && $end_date != '') {
            $start_date = $start_date;
            $end_date   = $end_date;
        } else {
            $start_date = $current_date;
            $end_date   = $current_date;
        }

        $start_date_formatted = $dateUtil->formatDate($start_date, 'DD/MM/YYYY');
        $end_date_formatted   = $dateUtil->formatDate($end_date, 'DD/MM/YYYY');

        $text = "
        <h2>Daily Collection Report</h2>
		<div class = 'tableOuter scroll-pane'>
			<table class='thinlist'>
				<thead>
					<tr>
						<th>Date</th>
                        <th>Customer Name</th>
						<th class='txtRight'>Amount Paid</th>
                        <th class='txtRight'>Outstanding Amount</th>
					</tr>
				</thead>
				<tbody>
					{$this->getRowsHTML()}
				</tbody>
			</table>
		</div>
        ";
        return $text;
    }

    function getRowsHTML() {
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $cpCfg = Zend_Registry::get('cpCfg');

        $rows = '';
		$siteTitle = '' ;
        $start_date     = $fn->getReqParam('start_date');
        $end_date       = $fn->getReqParam('end_date');
        $current_date   = date('Y-m-d');
        $month          = date('m');
        $year           = date('Y');
        $outstanding_amount = 0;

        foreach($this->model->dataArray as $row){
			if($row['date']){
				$creationDate = $fn->getCPDate($row['date'],"d-m-Y");
				$amount_paid = number_format($row['amount_paid'], 2);

                if ($start_date != '' && $end_date == '') {
                    $startDateAppendSql = "date >= '{$start_date}' AND date <= '{$current_date}'";
                } else if ($start_date == '' && $end_date != ''){
                    $start_date = $year . '-' . $month . '-' . '01';
                    $startDateAppendSql = "date >= '{$start_date}' AND date <= '{$end_date}'";
                } else if ($start_date != '' && $end_date != '') {
                    $startDateAppendSql = "date >= '{$start_date}' AND date <= '{$end_date}'";
                } else {
                    $start_date = $year . '-' . $month . '-' . '01';
                    $end_date = $year . '-' . $month . '-' . '31';
                    $startDateAppendSql = "date >= '{$current_date}' AND date <= '{$current_date}'";
                }

                $SQL = "
                SELECT SUM(amount) as outstanding_amount
                FROM customer_finance
                WHERE {$startDateAppendSql}
                  AND company_id = {$row['company_id']}
                ";
                $result = $db->sql_query($SQL);
                $rowcf = $db->sql_fetchrow($result);

                $SQLPaid = "
                SELECT SUM(amount) as paid_amount
                FROM customer_payment
                WHERE date <= '{$row['date']}'
                  AND company_id = {$row['company_id']}
                ";
                $resultPaid = $db->sql_query($SQLPaid);
                $rowPaid = $db->sql_fetchrow($resultPaid);

                $outstanding_amount = $rowcf['outstanding_amount'] - $rowPaid['paid_amount'];
                $outstanding_amount_formatted = number_format($outstanding_amount, 2);

			    $rows .= "
				<tr>
					<td>{$creationDate}</td>
                    <td>{$row['company_name']}</td>
					<td class='txtRight'>{$amount_paid}</td>
                    <td class='txtRight'>{$outstanding_amount_formatted}</td>
				</tr>
				";
			}
        }

        $text = "
        {$rows}
        ";

        return $text;
    }

}