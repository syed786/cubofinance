<?
class CPL_Admin_Widgets_Tradingsg_InvestorReport_Model extends CP_Common_Lib_WidgetModelAbstract
{
    /**
     *
     */
    function getSQL(){
        $cpCfg = Zend_Registry::get('cpCfg');

        $SQL = "
        SELECT i.*
              ,i.first_name AS investor_name
        FROM investor i
        ";

        return $SQL;
    }

    /**
     *
     */
    function setSearchVar() {
        $fn = Zend_Registry::get('fn');
        $cpCfg = Zend_Registry::get('cpCfg');
        $searchVar = $this->searchVar;
        $searchVar->mainTableAlias = 'i';

        $start_date 	= $fn->getReqParam('start_date');
        $end_date   	= $fn->getReqParam('end_date');
        $month      	= $fn->getReqParam('month');
        $year       	= $fn->getReqParam('year');
        $current_date 	= date('Y-m-d');
        $month        	= date('m');
        $year		  	= date('Y');

        /*if ($start_date != '' && $end_date == '') {
  	        $searchVar->sqlSearchVar[] = "ip.date >= '{$start_date}' AND ip.date <= '{$current_date}'";
        } else if ($start_date == '' && $end_date != ''){
            $start_date = $year . '-' . $month . '-' . '01';
  	        $searchVar->sqlSearchVar[] = "ip.date >= '{$start_date}' AND ip.date <= '{$end_date}'";
        } else if ($start_date != '' && $end_date != '') {
  	        $searchVar->sqlSearchVar[] = "ip.date >= '{$start_date}' AND ip.date <= '{$end_date}'";
        } else {
            $start_date = $year . '-' . $month . '-' . '01';
            $end_date = $year . '-' . $month . '-' . '31';
            $searchVar->sqlSearchVar[] = "ip.date >= '{$current_date}' AND ip.date <= '{$current_date}'";
        }

        $searchVar->groupBy   = "ip.date";*/
        $searchVar->sortOrder = 'investor_name ASC';

    }

    /**
     *
     * @param <type> $SQL
     * @return <type>
     */
    function getDataArray() {
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');

        $modelHelper = Zend_Registry::get('modelHelper');
        $dataArray = $modelHelper->getWidgetDataArray($this->controller, 'tradingsg_investorReport');

        $this->dataArray = $dataArray;
        return $this->dataArray;
    }

    /**
     */
    function getExportToExcel(){
        $db = Zend_Registry::get('db');
        $cpCfg = Zend_Registry::get('cpCfg');
        $tv = Zend_Registry::get('tv');
        $cpUtil = Zend_Registry::get('cpUtil');
        $dateUtil = Zend_Registry::get('dateUtil');
        $fn = Zend_Registry::get('fn');

        set_time_limit(50000);
        ini_set('memory_limit', '512M');

        require_once("PHPExcel.php");
        include 'PHPExcel/IOFactory.php';

        $file_name = "InvestorReport_" . date("d-m-Y") . ".xls";

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename={$file_name}");
        header("Content-Transfer-Encoding: binary ");

        $objPHPExcel = new PHPExcel();

        //--------------------------------------------------//
        $rowc = 1;
        $colc = 0;
        $start_date     = $fn->getReqParam('start_date');
        $end_date       = $fn->getReqParam('end_date');
        $month          = $fn->getReqParam('month');
        $year           = $fn->getReqParam('year');
        $current_date   = date('Y-m-d');
        $month          = date('m');
        $year           = date('Y');
        $record_type    = $fn->getReqParam('record_type');
        $actSheet = &$objPHPExcel->getActiveSheet();

        $actSheet->setCellValueByColumnAndRow($colc++, $rowc, 'Investor Name');
        $actSheet->setCellValueByColumnAndRow($colc++, $rowc, 'Investment Amount');
        $actSheet->setCellValueByColumnAndRow($colc++, $rowc, 'Amount Paid');
        $actSheet->setCellValueByColumnAndRow($colc++, $rowc, 'Outstanding Amount');

        /******************** FORMAT HEADER *******************/
        $headStyle = array(
            'font' => array('bold' => true)
        );

        $lastCol    = $actSheet->getHighestColumn();
        $lastColInd = PHPExcel_Cell::columnIndexFromString($lastCol);
        $actSheet->getStyle("A1:{$lastCol}1")->applyFromArray($headStyle);

        for ($i=0; $i < $lastColInd; $i++){
            $colAlphabet = PHPExcel_Cell::stringFromColumnIndex($i);
            $actSheet->getColumnDimension($colAlphabet)->setAutoSize(true);
        }

        $SQL = "
        SELECT i.*
              ,i.first_name AS investor_name
        FROM investor i
        ORDER BY investor_name
        ";

        $result = $db->sql_query($SQL);

        $grand_total    = 0;
        $grand_totalfrm = 0;

        while ($row = $db->sql_fetchrow($result)) {
            $colc = 0;

            $rowc++;
            $colc = 0;

            $SQLcf = "
            SELECT SUM(amount) as investment_amount
            FROM investor_investments
            WHERE investor_id = {$row['investor_id']}
            ";
            $resultcf = $db->sql_query($SQLcf);
            $rowcf = $db->sql_fetchrow($resultcf);

            $SQLPaid = "
            SELECT SUM(amount) as paid_amount
            FROM investor_payments
            WHERE investor_id = {$row['investor_id']}
            ";
            $resultPaid = $db->sql_query($SQLPaid);
            $rowPaid = $db->sql_fetchrow($resultPaid);

            $outstanding_amount = $rowcf['investment_amount'] - $rowPaid['paid_amount'];
            $outstanding_amount_formatted = number_format($outstanding_amount, 2);

            if($rowcf['investment_amount'] == 0){
                $rowcf['investment_amount'] = 0;
            }

            if($rowPaid['paid_amount'] == 0){
                $rowPaid['paid_amount'] = 0;
            }

            $investment_amount = number_format($rowcf['investment_amount'], 2);
            $paid_amount = number_format($rowPaid['paid_amount'], 2);

            $actSheet->setCellValueByColumnAndRow($colc++, $rowc, $row['investor_name']);
            $actSheet->setCellValueByColumnAndRow($colc++, $rowc, $investment_amount);
            $actSheet->setCellValueByColumnAndRow($colc++, $rowc, $paid_amount);
            $actSheet->setCellValueByColumnAndRow($colc++, $rowc, $outstanding_amount_formatted);
        }

        $colc = 0;
        $rowc++;

        $actSheet->getStyle("A{$rowc}:D{$rowc}")->applyFromArray($headStyle);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
}