<?
class CPL_Admin_Widgets_Tradingsg_InvestorReport_View extends CP_Common_Lib_WidgetViewAbstract
{
    //==================================================================//
    function getWidget() {
        $db = Zend_Registry::get('db');
        $fn = Zend_Registry::get('fn');
        $dateUtil = Zend_Registry::get('dateUtil');
        $cpCfg = Zend_Registry::get('cpCfg');

        $text = "
        <h2>Investor Report</h2>
		<div class = 'tableOuter scroll-pane'>
			<table class='thinlist'>
				<thead>
					<tr>
                        <th>Investor Name</th>
                        <th class='txtRight'>Investment Amount</th>
						<th class='txtRight'>Amount Paid</th>
                        <th class='txtRight'>Outstanding Amount</th>
					</tr>
				</thead>
				{$this->getRowsHTML()}
			</table>
		</div>
        ";
        return $text;
    }

    function getRowsHTML() {
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $cpCfg = Zend_Registry::get('cpCfg');

        $rows = '';
        $outstanding_amount = 0;

        foreach($this->model->dataArray as $row){
            $SQL = "
            SELECT SUM(amount) as investment_amount
            FROM investor_investments
            WHERE investor_id = {$row['investor_id']}
            ";
            $result = $db->sql_query($SQL);
            $rowcf = $db->sql_fetchrow($result);

            $SQLPaid = "
            SELECT SUM(amount) as paid_amount
            FROM investor_payments
            WHERE investor_id = {$row['investor_id']}
            ";
            $resultPaid = $db->sql_query($SQLPaid);
            $rowPaid = $db->sql_fetchrow($resultPaid);

            $outstanding_amount = $rowcf['investment_amount'] - $rowPaid['paid_amount'];
            $outstanding_amount_formatted = number_format($outstanding_amount, 2);

            if($rowcf['investment_amount'] == 0){
                $rowcf['investment_amount'] = 0;
            }

            if($rowPaid['paid_amount'] == 0){
                $rowPaid['paid_amount'] = 0;
            }

            $investment_amount = number_format($rowcf['investment_amount'], 2);
            $paid_amount = number_format($rowPaid['paid_amount'], 2);

            $SQLPaidDetails = "
            SELECT SUM(amount) as paid_amount
                  ,date AS date_paid
            FROM investor_payments
            WHERE investor_id = {$row['investor_id']}
            GROUP BY date
            ";
            $resultPaidDetails  = $db->sql_query($SQLPaidDetails);
            $numRowsPaidDetails = $db->sql_numrows($resultPaidDetails);
            $PaidDetails = '';
            while ($rowPaidDetails = $db->sql_fetchrow($resultPaidDetails)) {
                $amountFormatted = number_format($rowPaidDetails['paid_amount'], 2);
                $dateFormatted   = $fn->getCPDate($rowPaidDetails['date_paid'], 'd-m-Y');

                $PaidDetails .= "
                <tr>
                    <td width='22%'>{$dateFormatted}</td>
                    <td width='40%' class='txtRight'>{$amountFormatted}</td>
                </tr>
                ";
            }

            $paidAmountDetails = "
            <tr class='InvestmentAmountPaidDetails'>
                <td></td>
                <td colspan='2'>
                    <div class='InvestmentAmountPaidDetails2'>
                        <table class='thinlist tableInvesPaidDetail' width='90%'>
                            <thead>
                                <th width='22%'>Date</th>
                                <th width='40%' class='txtRight'>Amount Paid</th>
                            </thead>
                            <tbody>
                                {$PaidDetails}
                            </tbody>
                        </table>
                    </div>
                </td>
                <td></td>
            </tr>
            ";

            $SQLInvestmentDetails = "
            SELECT SUM(amount) as Investment_amount
                  ,date AS date_Investment
            FROM investor_investments
            WHERE investor_id = {$row['investor_id']}
            GROUP BY date
            ";
            $resultInvestmentDetails  = $db->sql_query($SQLInvestmentDetails);
            $numRowsInvestmentDetails = $db->sql_numrows($resultInvestmentDetails);
            $InvestmentDetails = '';
            while ($rowInvestmentDetails = $db->sql_fetchrow($resultInvestmentDetails)) {
                $amountFormatted = number_format($rowInvestmentDetails['Investment_amount'], 2);
                $dateFormatted   = $fn->getCPDate($rowInvestmentDetails['date_Investment'], 'd-m-Y');

                $InvestmentDetails .= "
                <tr>
                    <td width='22%'>{$dateFormatted}</td>
                    <td width='40%' class='txtRight'>{$amountFormatted}</td>
                </tr>
                ";
            }

            $InvestmentAmountDetails = "
            <tr class='InvestmentAmountInvestDetails'>
                <td></td>
                <td colspan='2'>
                    <div class='InvestmentAmountInvestDetails2'>
                        <table class='thinlist tableInvesInvestDetail' width='90%'>
                            <thead>
                                <th width='22%'>Date</th>
                                <th width='40%' class='txtRight'>Amount Paid</th>
                            </thead>
                            <tbody>
                                {$InvestmentDetails}
                            </tbody>
                        </table>
                    </div>
                </td>
                <td></td>
            </tr>
            ";

		    $rows .= "
            <tbody class='investorDetailsTableTbody'>
    			<tr>
                    <td>{$row['investor_name']}</td>
    				<td class='txtRight investmentToggle'>{$investment_amount}</td>
                    <td class='txtRight investmentPaymentToggle'>{$paid_amount}</td>
                    <td class='txtRight'>{$outstanding_amount_formatted}</td>
    			</tr>
                {$InvestmentAmountDetails}
                {$paidAmountDetails}
            </tbody>
			";
        }

        $text = "
        {$rows}
        ";

        return $text;
    }

}