<?
class CPL_Admin_Widgets_Tradingsg_TopCustomerOutstanding_Model extends CP_Common_Lib_WidgetModelAbstract
{
    /**
     *
     */
    function getSQL(){
        
        $SQL = "
        SELECT c.*
        FROM company c
        ";

        return $SQL;
    }

    /**
     *
     */
    function setSearchVar() {
        $searchVar = $this->searchVar;
        $searchVar->mainTableAlias = 'inv';

        $today       = date('Y-m-d');

        /*$searchVar->sqlSearchVar[] = "inv.status != 'Cancelled'";
        $searchVar->sqlSearchVar[] = "inv.invoice_amount > 0";
        $searchVar->sqlSearchVar[] = "o.company_id > 0";
        $searchVar->groupBy   = "c.company_id";*/
        $searchVar->sortOrder = "c.company_name";
    }

    /**
     *
     * @param <type> $SQL
     * @return <type>
     */
    function getDataArray() {
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');

        $modelHelper = Zend_Registry::get('modelHelper');
        $dataArray = $modelHelper->getWidgetDataArray($this->controller, 'tradingsg_topCustomerOutstanding');

        $this->dataArray = $dataArray;
        return $this->dataArray;
    }

}