<?
class CPL_Admin_Widgets_Tradingsg_TopCustomerOutstanding_View extends CP_Common_Lib_WidgetViewAbstract
{
    /**
     *
     */
    function getWidget() {
        $db = Zend_Registry::get('db');
        $fn = Zend_Registry::get('fn');

        $text = "
        <h2>Customer Outstanding</h2>
        <div class = 'tableOuter scroll-pane'>
            <table class='thinlist'>
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Customer Name</th>
                        <th class='txtRight'>Loan Amount</th>
                        <th class='txtRight'>Amount Outstanding</th>
                    </tr>
                </thead>
                <tbody>
                    {$this->getRowsHTML()}
                </tbody>
            </table>
        </div>
        ";
        return $text;
    }

    /**
     *
     */
    function getRowsHTML() {
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        
        $rows        = '';
        $count       = 1;
        $totalAmount = 0;
        foreach($this->model->dataArray as $row){
            $start_date     = $fn->getReqParam('start_date');
            $end_date       = $fn->getReqParam('end_date');
            $month          = date('m');
            $year           = date('Y');

            $SQLOverallTotal = "
            SELECT  SUM(amount) AS overall_Total
            FROM customer_finance
            WHERE company_id = '{$row['company_id']}'
            ";
            $resultOverallTotal = $db->sql_query($SQLOverallTotal);
            $rowOverallTotal    = $db->sql_fetchrow($resultOverallTotal);

            $overall_Total = number_format($rowOverallTotal['overall_Total'], 2);

            $SQLOverallBalance = "
            SELECT  SUM(amount) AS overall_paid
            FROM customer_payment
            WHERE company_id = '{$row['company_id']}'
            ";
            $resultOverallBalance = $db->sql_query($SQLOverallBalance);
            $rowOverallBalance    = $db->sql_fetchrow($resultOverallBalance);

            $overallBalance = $rowOverallTotal['overall_Total'] - $rowOverallBalance['overall_paid'];
            $overallBalance = number_format($overallBalance, 2);

            if($overallBalance > 0){
                $rows .= "
                <tr>
                    <td>{$count}</td>
                    <td>{$row['company_name']}</td>
                    <td class='txtRight'>{$overall_Total}</td>
                    <td class='txtRight'>{$overallBalance}</td>
                </tr>
                ";
                $count++;
            }

        }
        
        $text = "
        {$rows}
        ";

        return $text;
    }
}