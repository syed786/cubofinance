<?
class CPL_Admin_Widgets_Tradingsg_DailyCollectionChart_Model extends CP_Common_Lib_WidgetModelAbstract
{
    /**
     *
     */
    function getSQL(){
        
        $SQL = "
        SELECT r.*
              ,SUM(r.amount) as receipt_amount
              ,o.record_type
              ,o.order_id
              ,SUM(srh.qty_return * srh.price) As sales_return_amount
        FROM receipt r
        LEFT JOIN (`order` o) ON (r.order_id = o.order_id)
        LEFT JOIN (`sales_return_history` srh) ON (r.order_id = srh.order_id)
        ";
        $SQL = "
        SELECT SUM(cp.amount) as amount_paid
              ,cp.date
        FROM customer_payment cp
        ";

        return $SQL;
    }

    /**
     *
     */
    function setSearchVar() {
        $searchVar = $this->searchVar;
        $searchVar->mainTableAlias = 'cp';
        $month      = date('m');
        $year       = date('Y');

        $start_date = date('Y-m-d', mktime (0,0,0,date("m")-1, date("d"), date("Y")));
        $end_date = date('Y-m-d');

        if($_SERVER['HTTP_HOST'] == 'cubobillpro.usoftdev.com'){
            $searchVar->sqlSearchVar[] = "(cp.date BETWEEN '2019-07-01' AND '2019-07-31')";
        } else {
            $searchVar->sqlSearchVar[] = "(cp.date BETWEEN '{$start_date}' AND '{$end_date}')";
        }
        $searchVar->groupBy = "cp.date";
    }

    /**
     *
     * @param <type> $SQL
     * @return <type>
     */
    function getDataArray() {
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');

        $modelHelper = Zend_Registry::get('modelHelper');
        $dataArray = $modelHelper->getWidgetDataArray($this->controller, 'tradingsg_dailyCollectionChart');

        $this->dataArray = $dataArray;
        return $this->dataArray;
    }

}