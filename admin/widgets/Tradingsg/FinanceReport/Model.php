<?
class CPL_Admin_Widgets_Tradingsg_FinanceReport_Model extends CP_Common_Lib_WidgetModelAbstract
{
    /**
     *
     */
    function getSQL(){
        
        $SQL = "
        SELECT DATE_FORMAT(cf.date, '%b %Y') AS finance_month
              ,(SUM(amount)) AS amount_monthly
        FROM customer_finance cf
        ";

        return $SQL;
    }

    /**
     *
     */
    function setSearchVar() {
        $searchVar = $this->searchVar;
        $searchVar->mainTableAlias = 'cf';
        $fn = Zend_Registry::get('fn');

        $month          = $fn->getReqParam('month');
        $year           = $fn->getReqParam('year');
        $current_date   = date('Y-m-d');

        if ($month != '') {
            $searchVar->sqlSearchVar[] = "DATE_FORMAT(cf.date, '%m') = '{$month}'" ;
        }

        if ($year != '') {
            $searchVar->sqlSearchVar[] = "DATE_FORMAT(cf.date, '%Y') = '{$year}'" ;
        }
    }

    /**
     *
     * @param <type> $SQL
     * @return <type>
     */
    function getDataArray() {
        $ln = Zend_Registry::get('ln');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');

        $modelHelper = Zend_Registry::get('modelHelper');
        $dataArray = $modelHelper->getWidgetDataArray($this->controller, 'tradingsg_topCustomerOutstanding');

        $this->dataArray = $dataArray;
        return $this->dataArray;
    }

}