<?
class CPL_Admin_Widgets_Tradingsg_FinanceReport_View extends CP_Common_Lib_WidgetViewAbstract
{
    /**
     *
     */
    function getWidget() {
        $db = Zend_Registry::get('db');
        $fn = Zend_Registry::get('fn');

        $text = "
        <h2>Finance Report</h2>
        <div class = 'tableOuter scroll-pane'>
            <table class='thinlist'>
                <thead>
                    <tr>
                        <th>Month</th>
                        <th class='txtRight'>Finance</th>
                    </tr>
                </thead>
                <tbody>
                    {$this->getRowsHTML()}
                </tbody>
            </table>
        </div>
        ";
        return $text;
    }

    /**
     *
     */
    function getRowsHTML() {
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        
        $rows        = '';
        $count       = 1;
        $totalAmount = 0;
        foreach($this->model->dataArray as $row){
            $rows .= "
            <tr>
                <td>{$row['finance_month']}</td>
                <td class='txtRight'>{$row['amount_monthly']}</td>
            </tr>
            ";
            $count++;
        }
        
        $text = "
        {$rows}
        ";

        return $text;
    }
}