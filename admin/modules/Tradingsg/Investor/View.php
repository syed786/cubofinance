<?
class CPL_Admin_Modules_Tradingsg_Investor_View extends CP_Common_Lib_ModuleViewAbstract
{
    /**
     *
     */
    function getList($dataArray){
        $listObj = Zend_Registry::get('listObj');

        $count   = 0;
        $rows    = '';

        foreach ($dataArray as $row){
            $email     = $row['email'];
            $website   = $row['website'];

            $rows .= "
            {$listObj->getListRowHeader($row, $count)}
            {$listObj->getGoToDetailText($count, $row['first_name'])}
            {$listObj->getListDataCell($row['phone'])}
            {$listObj->getListDataCell($row['alternate_phone'])}
            {$listObj->getListRowEnd($row['investor_id'])}
            ";

            $count++ ;
        }

        $text = "
        {$listObj->getListHeader()}
        {$listObj->getListHeaderCell('Name', 'c.first_name')}
        {$listObj->getListHeaderCell('Main Phone Number', 'a.phone')}
        {$listObj->getListHeaderCell('Alternate Phone Number', 'a.alternate_phone' )}
        {$listObj->getListHeaderEnd()}
        {$rows}
        {$listObj->getListFooter()}
        ";

        return $text;
    }

    /**
     *
     */
    function getNew(){
        $formObj = Zend_Registry::get('formObj');

        $fielset1 = "
        {$formObj->getTBRow('Name', 'first_name')}
        ";

        $text = "
        {$formObj->getFieldSetWrapped('Key Details', $fielset1)}
        ";

        return $text;
    }
    /**
     *
     */
    function getEdit($row){
        $formObj = Zend_Registry::get('formObj');
        $cpCfg = Zend_Registry::get('cpCfg');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');

        $formObj->mode = $tv['action'];

        $discountPercent = '';
        $cstNo = '';
        $tinNo = '';

        $sqlInvestor = $fn->getValueListSQL('investorType');
        $sqlIndustry = $fn->getValueListSQL('companyIndustry');
        $sqlSize     = $fn->getValueListSQL('companySize');
        $sqlSource   = $fn->getValueListSQL('companySource');

        $sqlCountry = getCPModelObj('common_geoCountry')->getCountryDDSQL();
        $expCountry = array('detailValue' => $row['country_name']);

        $expVl = array('sqlType' => 'OneField');

        $createLogin = '';
        $creation_date = $fn->getCPDate($row['creation_date'], 'd-m-Y H:i:s');
        $modification_date = $fn->getCPDate($row['modification_date'], 'd-m-Y H:i:s');

        $statusArr = array(
             "Active"
            ,"Archive"
        );

        $text = "
        <div class='linkPortalWrapper'>
            <div expanded='1' class='header'>
                <div class='floatbox'>
                    <div class='float_left'>Investor Details</div>
                    <div class='toggle'></div>
                    <div class='float_right'>Creation : {$row['created_by']} on {$creation_date} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Modified : {$row['modified_by']} {$modification_date}</div>
                    {$createLogin}
                </div>
            </div>
            <div>
                <div class='linkPortalDataWrapper'>
                    <table>
                        <tbody>
                            <div class='col-md-2'>{$formObj->getTBRow('Name', 'first_name', $row['first_name'])}</div>
                            <div class='col-md-1'>{$formObj->getDDRowByArr('Status', 'status', $statusArr, $row['status'])}</div>
                            <div class='col-md-2'>{$formObj->getTBRow('Main Phone', 'phone', $row['phone'])}</div>
                            <div class='col-md-2'>{$formObj->getTBRow('Alternate Phone', 'alternate_phone', $row['alternate_phone'])}</div>
                            <div class='col-md-2'>{$formObj->getTBRow('Street Address', 'address_street', $row['address_street'])}</div>
                            <div class='col-md-2'>{$formObj->getTBRow('Town', 'address_town', $row['address_town'])}</div>
                            <div class='col-md-2'>{$formObj->getTBRow('City', 'address2', $row['address2'])}</div>
                            <div class='col-md-2'>{$formObj->getTBRow('State', 'address_state', $row['address_state'])}</div>
                            <div class='col-md-2'>{$formObj->getTBRow('Postal Code', 'address_po_code', $row['address_po_code'])}</div>
                            <div class='col-md-3'>{$formObj->getDDRowBySQL('Country', 'address_country', $sqlCountry, $row['address_country'], $expCountry)}</div>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        ";

        return $text;
    }


    /**
     *
     */
    function getPrintDetail($row){
        $db = Zend_Registry::get('db');
        return $this->getDetail($row);
    }

    /**
     *
     */
    function getSearch(){
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $sqlCategory = $fn->getValueListSQL('companyCategory');
        $sqlStatus   = $fn->getValueListSQL('companyStatus');
        $expVl = array('sqlType' => 'OneField');

        $spArray = array(
            "Flagged"
           ,"Not-Flagged"
        );

        $fielset = "
        {$formObj->getTBRow('Name', 'first_name')}
        {$formObj->getDDRowBySQL('Choose Category', 'category', $sqlCategory, 'Client', $expVl)}
        {$formObj->getDDRowBySQL('Status', 'status', $sqlStatus, 'Current', $expVl)}
        {$formObj->getDDRowByArr('Special Search', 'special_search', $spArray)}
        ";

        $text = "
        {$formObj->getFieldSetWrapped('Investor Details', $fielset)}
        ";

        return $text;
    }

    /**
     *
     */
    function getRightPanel($row){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $displayLinkData = Zend_Registry::get('displayLinkData');
        $media = Zend_Registry::get('media');

        $record_id = $fn->getIssetParam($row, 'investor_id');

        $sqlInvestor = "
        SELECT s.*
        FROM investor s
        WHERE s.investor_id = {$row['investor_id']}
        ";

        $resultInvestor = $db->sql_query($sqlInvestor);
        $rowInvestor = $db->sql_fetchrow($resultInvestor);

        $text = "
        {$media->getRightPanelMediaDisplay('Attachments', 'tradingsg_investor', 'attachment', $row)}
        <div id='InvestmentsLinkPortal'>{$this->getAddInvestmentsPayment($record_id)}</div>
        <div id='PaymentsLinkPortal'>{$this->getAddInvestorPayment($record_id)}</div>
        ";

        return $text;
    }

    /**
     *
     */
    function getQuickSearch() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');

        $status   = $fn->getReqParam('status');

        $sqlStatus = $fn->getValueListSQL('companyStatus');

        $spArray = array(
            "Flagged"
           ,"Not-Flagged"
        );

        $statusArr = array(
            "Active"
           ,"Archive"
        );

        $text = "
        <td>
            <select name='status'>
                <option value=''>Status</option>
                {$cpUtil->getDropDown1($statusArr, $status)}
            </select>
        </td>
        <td>
            <select name='special_search'>
                <option value=''>Special Search</option>
                {$cpUtil->getDropDown1($spArray, $tv['special_search'])}
           </select>
        </td>
        ";

        return $text;
    }

    /**
     *
     */
    function getNewInvestor(){
        $formObj = Zend_Registry::get('formObj');
        $fn = Zend_Registry::get('fn');
        $tv = Zend_Registry::get('tv');

        $formAction = "index.php?_spAction=addInvestor&lnkRoom=tradingsg_investor&showHTML=0";
        $sqlCountry = getCPModelObj('common_geoCountry')->getCountryDDSQL();
        
        $text = "
        <form id='portalForm' class='yform columnar' method='post' action='{$formAction}'>
            <fieldset>
                {$formObj->getTBRow('Name', 'first_name')}
                {$formObj->getTBRow('Website', 'website')}
                {$formObj->getTBRow('Phone', 'phone')}
                {$formObj->getTBRow('Gst No', 'gst_no')}
                {$formObj->getTBRow('Office Address', 'address_flat')}
                {$formObj->getTBRow('Street Address', 'address_street')}
                {$formObj->getTBRow('District/ Town', 'address_town')}
                {$formObj->getTBRow('State/ Zip', 'address_state')}
                {$formObj->getDDRowBySQL('Country', 'address_country', $sqlCountry)}
            </fieldset>
            
        </form>
        ";

        return $text;
    }

    /**
     *
     */
    function getAddInvestmentsPayment($investor_id=''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($investor_id == ''){
            $investor_id = $fn->getReqParam('investor_id');
        }

        $InvestmentsPayment = $this->getAddInvestmentsPaymentDetail($investor_id);

        $recCount = $fn->getRecordCount('investor_investments', "investor_id = '{$investor_id}'");

        $header ="
        <thead>
            <tr>
                <th>Date</th>
                <th class='txtRight'>Amount</th>
                <th>Notes</th>
                <th>Status</th>
                <th class='portalActBtns'></th>
                <th class='portalActBtns'></th>
            </tr>
        </thead>
        ";

        if($recCount == 0){
            $header = "<tr><td align='center'>No Records Linked<br/><br/></td></tr>";
        }

        $formActionInvestmentsPayment = "index.php?module=tradingsg_investor&_spAction=InvestmentsPayment&investor_id={$investor_id}&showHTML=0";

        $add = "<div class='btn btn-info'>
                    <a id='AddInvestmentsPayment' href='{$formActionInvestmentsPayment}' investor_id='{$investor_id}'>
                        Add Investments
                    </a>
                </div>
                ";


        $SQLOverallTotal = "
        SELECT  SUM(amount) AS overall_Total
        FROM investor_investments
        WHERE investor_id = '{$investor_id}'
        ";
        $resultOverallTotal = $db->sql_query($SQLOverallTotal);
        $rowOverallTotal    = $db->sql_fetchrow($resultOverallTotal);

        $SQLPayments = "
        SELECT SUM(amount) AS Payments
        FROM investor_payments
        WHERE investor_id = {$investor_id}
        ";
        $resultPayments = $db->sql_query($SQLPayments);
        $rowPayments    = $db->sql_fetchrow($resultPayments);

        $BalanceAmount = $rowOverallTotal['overall_Total'] - $rowPayments['Payments'];

        if($BalanceAmount == ''){
            $BalanceAmount = 0;
        }

        $BalanceAmount = number_format($BalanceAmount, 2);
        $overall_Total = number_format($rowOverallTotal['overall_Total'], 2);

        $text = "
        <div class='linkPortalWrapper tradingsg_investor_paymentLink'>
            <div class='header'>
                <div class='floatbox'>
                    {$add}
                </div>
                <div class='float_right grandTotalPurchasePo'>
                    Total Investments: {$overall_Total} | Balance: {$BalanceAmount}
                </div>
            </div>
            <div class='header' expanded='1'>
                <div class='floatbox'>
                    <div class='float_left rightpanelHeading'>Investments</div>
                    <div class='txtRight'>
                        <span class='count'>({$recCount})</span>
                    </div>
                </div>
            </div>
            <div class='linkPortalDataWrapper'>
                <form>
                    <table class='renewallist'>
                        {$header}
                        <tbody id='AddInvestmentsPaymentPortal'>
                            {$InvestmentsPayment}
                        </tbody>
                    </table>
                    <input type='hidden' name='investor_id' value='{$investor_id}' />
                </form>
            </div>
        </div>
        ";

        return $text;

    }

    /**
     *
     */
    function getAddInvestmentsPaymentDetail($investor_id = ''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($investor_id == ''){
            $investor_id = $fn->getReqParam('investor_id');
        }

        $investor_investments_id = $fn->getReqParam('investor_investments_id');

        $rows  = "";

        $SQL = "
        SELECT cp.*
        FROM investor_investments cp
        WHERE investor_id = '{$investor_id}'
        ";
        $result  = $db->sql_query($SQL);
        $numRows = $db->sql_numrows($result);
        $amount  = 0;
        $count   = 1;
        while ($row = $db->sql_fetchrow($result)) {

            $formActionEditInvestmentsPayment   = "index.php?module=tradingsg_investor&_spAction=EditInvestmentsPayment&investor_investments_id={$row['investor_investments_id']}&investor_id={$investor_id}&showHTML=0";

            $deleteIcon = "
            <div class='float_right'>
                <a class='deleteInvestmentsPayment' href='#'  investor_investments_id='{$row['investor_investments_id']}' investor_id='{$row['investor_id']}'>
                    <img src='/cmspilotv30/CP/admin/images/icons/btn_remove.png'>
                </a>
            </div>
            ";

            $editIcon = "
            <div class='float_right'>
                <a class='EditInvestmentsPayment' href='{$formActionEditInvestmentsPayment}' investor_investments_id='{$row['investor_investments_id']}'  investor_id='{$row['investor_id']}'>
                    <img src='/cmspilotv30/CP/admin/images/icons/btn_edit.png'>
                </a>
            </div>
            ";

            $amount = number_format($row['amount'], 2);

            $rows .= "
                <tr>
                    <td>{$fn->getCPDate($row['date'], 'd-m-Y')}</td>
                    <td class='txtRight'>{$amount}</td>
                    <td>{$row['description']}</td>
                    <td>{$row['status']}</td>
                    <td>
                        {$editIcon}
                    </td>
                    <td>
                        {$deleteIcon}
                    </td>
                </tr>
            ";
            $count++;
        }

        $text="{$rows}";

        return $text;
    }

    /**
     *
     */
    function getInvestmentsPayment() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $expVl = array('sqlType' => 'OneField');

        $investor_id  = $fn->getReqParam('investor_id');

        $chitTypeArr = array(
             "Daily"
            ,"Monthly"
        );

        $statusArr = array(
             "Due"
            ,"Partially Paid"
            ,"Paid"
            ,"Cancelled"
        );

        $date = date('Y-m-d');

        $formAction = "index.php?module=tradingsg_investor&_spAction=InvestmentsPaymentFormSubmit&showHTML=0";

        $text = "
        <form id='InvestmentsPaymentPortalForm' class='yform columnar' method='post' action='{$formAction}'>
            {$formObj->getDateRow('Date', 'date', $date)}
            {$formObj->getTBRow('Amount', 'amount')}
            {$formObj->getTARow('Notes', 'description')}
            {$formObj->getDDRowByArr('Status', 'status', $statusArr, 'Due')}
            <input type='hidden' name='investor_id' value='{$investor_id}' />
        </form>
        ";
        return $text;
    }

    /**
     *
     */
    function getEditInvestmentsPayment() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $expVl = array('sqlType' => 'OneField');
        $investor_id  = $fn->getReqParam('investor_id');
        $investor_investments_id  = $fn->getReqParam('investor_investments_id');

        if($investor_investments_id == ''){
            $investor_investments_id  = $fn->getReqParam('investor_investments_id');
        }

        $chitTypeArr = array(
             "Daily"
            ,"Monthly"
        );

        $statusArr = array(
             "Due"
            ,"Partially Paid"
            ,"Paid"
            ,"Cancelled"
        );

        $rows  = "";

        $formAction = "index.php?module=tradingsg_investor&_spAction=EditInvestmentsPaymentFormSubmit&showHTML=0&investor_investments_id={$investor_investments_id}";

        $SQLPayment ="
        SELECT cp.*
        FROM investor_investments cp
        WHERE investor_investments_id = '{$investor_investments_id}'
        ";
        $resultPayment   = $db->sql_query($SQLPayment);
        $rowPayment = $db->sql_fetchrow($resultPayment);

        $rows .= "
        <form id='portalForm' class='yform columnar' method='post' action='{$formAction}'>
            {$formObj->getDateRow('Date', 'date', $rowPayment['date'])}
            {$formObj->getTBRow('Amount', 'amount', $rowPayment['amount'])}
            {$formObj->getTARow('Notes', 'description', $rowPayment['description'])}
            {$formObj->getDDRowByArr('Status', 'status', $statusArr, $rowPayment['status'])}
            <input type='hidden' name='investor_investments_id' value='{$investor_investments_id}' />
            <input type='hidden' name='investor_id' value='{$investor_id}' />
        </form>
        ";        

        $text="{$rows}";

        return $text;
    }

    /**
     *
     */
    function getAddInvestorPayment($investor_id=''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($investor_id == ''){
            $investor_id = $fn->getReqParam('investor_id');
        }

        $InvestorPayment = $this->getAddInvestorPaymentDetail($investor_id);

        $recCount = $fn->getRecordCount('investor_payments', "investor_id = '{$investor_id}'");

        $header ="
        <thead>
            <tr>
                <th>Date</th>
                <th class='txtRight'>Amount</th>
                <th>Notes</th>
                <th class='portalActBtns'></th>
                <th class='portalActBtns'></th>
            </tr>
        </thead>
        ";

        if($recCount == 0){
            $header = "<tr><td align='center'>No Records Linked<br/><br/></td></tr>";
        }

        $formActionInvestorPayment = "index.php?module=tradingsg_investor&_spAction=investorPayment&investor_id={$investor_id}&showHTML=0";

        $add = "<div class='btn btn-info'>
                    <a id='AddInvestorPayment' href='{$formActionInvestorPayment}' investor_id='{$investor_id}'>
                        Add Payments
                    </a>
                </div>
                ";


        $SQLOverallTotal = "
        SELECT  SUM(amount) AS overall_Total
        FROM investor_payments
        WHERE investor_id = '{$investor_id}'
        ";
        $resultOverallTotal = $db->sql_query($SQLOverallTotal);
        $rowOverallTotal    = $db->sql_fetchrow($resultOverallTotal);

        $overall_Total = number_format($rowOverallTotal['overall_Total'], 2);

        $text = "
        <div class='linkPortalWrapper tradingsg_investor_paymentLink'>
            <div class='header'>
                <div class='floatbox'>
                    {$add}
                </div>
                <div class='float_right grandTotalPurchasePo'>
                    Total Payments: {$overall_Total}
                </div>
            </div>
            <div class='header' expanded='1'>
                <div class='floatbox'>
                    <div class='float_left rightpanelHeading'>Payments</div>
                    <div class='txtRight'>
                        <span class='count'>({$recCount})</span>
                    </div>
                </div>
            </div>
            <div class='linkPortalDataWrapper'>
                <form>
                    <table class='renewallist'>
                        {$header}
                        <tbody id='AddInvestorPaymentPortal'>
                            {$InvestorPayment}
                        </tbody>
                    </table>
                    <input type='hidden' name='investor_id' value='{$investor_id}' />
                </form>
            </div>
        </div>
        ";

        return $text;

    }

    /**
     *
     */
    function getAddInvestorPaymentDetail($investor_id = ''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($investor_id == ''){
            $investor_id = $fn->getReqParam('investor_id');
        }

        $investor_payments_id = $fn->getReqParam('investor_payments_id');

        $rows  = "";

        $SQL = "
        SELECT cp.*
        FROM investor_payments cp
        WHERE investor_id = '{$investor_id}'
        ";
        $result  = $db->sql_query($SQL);
        $numRows = $db->sql_numrows($result);
        $amount  = 0;
        $count   = 1;
        while ($row = $db->sql_fetchrow($result)) {

            $formActionEditInvestorPayment = "index.php?module=tradingsg_investor&_spAction=EditInvestorPayment&investor_payments_id={$row['investor_payments_id']}&investor_id={$investor_id}&showHTML=0";

            $deleteIcon = "
            <div class='float_right'>
                <a class='deleteInvestorPayment' href='#'  investor_payments_id='{$row['investor_payments_id']}' investor_id='{$row['investor_id']}'>
                    <img src='/cmspilotv30/CP/admin/images/icons/btn_remove.png'>
                </a>
            </div>
            ";

            $editIcon = "
            <div class='float_right'>
                <a class='EditInvestorPayment' href='{$formActionEditInvestorPayment}' investor_payments_id='{$row['investor_payments_id']}'  investor_id='{$row['investor_id']}'>
                    <img src='/cmspilotv30/CP/admin/images/icons/btn_edit.png'>
                </a>
            </div>
            ";

            $amount = number_format($row['amount'], 2);

            $rows .= "
                <tr>
                    <td>{$fn->getCPDate($row['date'], 'd-m-Y')}</td>
                    <td class='txtRight'>{$amount}</td>
                    <td>{$row['description']}</td>
                    <td>
                        {$editIcon}
                    </td>
                    <td>
                        {$deleteIcon}
                    </td>
                </tr>
            ";
            $count++;
        }

        $text="{$rows}";

        return $text;
    }

    /**
     *
     */
    function getInvestorPayment() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $expVl = array('sqlType' => 'OneField');

        $investor_id  = $fn->getReqParam('investor_id');

        $chitTypeArr = array(
             "Daily"
            ,"Monthly"
        );

        $date = date('Y-m-d');

        $formAction = "index.php?module=tradingsg_investor&_spAction=InvestorPaymentFormSubmit&showHTML=0";

        $text = "
        <form id='InvestorPaymentPortalForm' class='yform columnar' method='post' action='{$formAction}'>
            {$formObj->getDateRow('Date', 'date', $date)}
            {$formObj->getTBRow('Amount', 'amount')}
            {$formObj->getTARow('Notes', 'description')}
            <input type='hidden' name='investor_id' value='{$investor_id}' />
        </form>
        ";
        return $text;
    }

    /**
     *
     */
    function getEditInvestorPayment() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $expVl = array('sqlType' => 'OneField');
        $investor_id  = $fn->getReqParam('investor_id');
        $investor_payments_id  = $fn->getReqParam('investor_payments_id');

        if($investor_payments_id == ''){
            $investor_payments_id  = $fn->getReqParam('investor_payments_id');
        }

        $chitTypeArr = array(
             "Daily"
            ,"Monthly"
        );

        $rows  = "";

        $formAction = "index.php?module=tradingsg_investor&_spAction=EditInvestorPaymentFormSubmit&showHTML=0&investor_payments_id={$investor_payments_id}";

        $SQLPayment ="
        SELECT cp.*
        FROM investor_payments cp
        WHERE investor_payments_id = '{$investor_payments_id}'
        ";
        $resultPayment   = $db->sql_query($SQLPayment);
        $rowPayment = $db->sql_fetchrow($resultPayment);

        $rows .= "
        <form id='portalForm' class='yform columnar' method='post' action='{$formAction}'>
            {$formObj->getDateRow('Date', 'date', $rowPayment['date'])}
            {$formObj->getTBRow('Amount', 'amount', $rowPayment['amount'])}
            {$formObj->getTARow('Notes', 'description', $rowPayment['description'])}
            <input type='hidden' name='investor_payments_id' value='{$investor_payments_id}' />
            <input type='hidden' name='investor_id' value='{$investor_id}' />
        </form>
        ";        

        $text="{$rows}";

        return $text;
    }
}