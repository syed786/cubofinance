Util.createCPObject('cpm.tradingsg.investor');

cpm.tradingsg.investor = {
    init: function(){
    	/* Add Investments Payment */
        $('#AddInvestmentsPayment').live('click', function (e){
            var title = "Add Investments";
            e.preventDefault();
            var investor_id = $(this).attr('investor_id');

            var expObj = {
                validate: true
               ,callbackOnSuccess: function(){
                    var msg = 'Investments Added Successfully';
                    Util.alert(msg, function(){
                        Util.closeAllDialogs();
                        cpm.tradingsg.investor.reloadInvestments(investor_id);
                        //window.location.reload(true);
                    });
                }
            }
            Util.openFormInDialog.call(this, 'InvestmentsPaymentPortalForm', title, 600, 400, expObj);
        });

        /* Edit Investments Payment */
        $('.EditInvestmentsPayment').live('click', function (e){
            var title = "Edit Investments";
            var investor_id = $(this).attr('investor_id');
            e.preventDefault();

            var expObj = {
                validate: true
               ,callbackOnSuccess: function(){
                    Util.closeAllDialogs();
                    Util.alert('Investments Updated Successfully');
                    cpm.tradingsg.investor.reloadInvestments(investor_id);
                }
            }
            Util.openFormInDialog.call(this, 'portalForm', title, 600, 500, expObj);
        });

        /* Delete Investments Payment */
        $('.deleteInvestmentsPayment').live('click', function (e){
            msg = "Do you like to delete the Investments?";
            if (!confirm(msg)){
                return false;
            }
            else{
                Util.showProgressInd();
                var investor_investments_id = $(this).attr('investor_investments_id');
                var investor_id = $(this).attr('investor_id');

                var url = 'index.php?module=tradingsg_investor&_spAction=DeleteInvestmentsPayment&showHTML=0&investor_investments_id=' + investor_investments_id;
                $.get(url, {investor_investments_id: investor_investments_id}, function(html){
                    Util.hideProgressInd();
                    cpm.tradingsg.investor.reloadInvestments(investor_id);
                });
            }
        });

        /* Add Investor Payment */
        $('#AddInvestorPayment').live('click', function (e){
            var title = "Add Investor Payment";
            e.preventDefault();
            var investor_id = $(this).attr('investor_id');

            var expObj = {
                validate: true
               ,callbackOnSuccess: function(){
                    var msg = 'Payment Added Successfully';
                    Util.alert(msg, function(){
                        Util.closeAllDialogs();
                        cpm.tradingsg.investor.reloadPayments(investor_id);
                        //window.location.reload(true);
                    });
                }
            }
            Util.openFormInDialog.call(this, 'InvestorPaymentPortalForm', title, 600, 400, expObj);
        });

        /* Edit Investor Payment */
        $('.EditInvestorPayment').live('click', function (e){
            var title = "Edit Payment";
            var investor_id = $(this).attr('investor_id');
            e.preventDefault();

            var expObj = {
                validate: true
               ,callbackOnSuccess: function(){
                    Util.closeAllDialogs();
                    Util.alert('Payment Updated Successfully');
                    cpm.tradingsg.investor.reloadPayments(investor_id);
                }
            }
            Util.openFormInDialog.call(this, 'portalForm', title, 600, 500, expObj);
        });

        /* Delete Investor Payment */
        $('.deleteInvestorPayment').live('click', function (e){
            msg = "Do you like to delete the Payment?";
            if (!confirm(msg)){
                return false;
            }
            else{
                Util.showProgressInd();
                var investor_payments_id = $(this).attr('investor_payments_id');
                var investor_id = $(this).attr('investor_id');

                var url = 'index.php?module=tradingsg_investor&_spAction=DeleteInvestorPayment&showHTML=0&investor_payments_id=' + investor_payments_id;
                $.get(url, {investor_payments_id: investor_payments_id, investor_id:investor_id}, function(html){
                    Util.hideProgressInd();
                    cpm.tradingsg.investor.reloadPayments(investor_id);
                });
            }
        });
    },

    reloadInvestments: function(investor_id){
        var url = 'index.php?module=tradingsg_investor&_spAction=AddInvestmentsPayment&showHTML=0';
        $.get(url,{investor_id:investor_id}, function(html){
            $('#InvestmentsLinkPortal').html(html);
        });
    },

    reloadPayments: function(investor_id){
        var url = 'index.php?module=tradingsg_investor&_spAction=AddInvestorPayment&showHTML=0';
        $.get(url,{investor_id:investor_id}, function(html){
            $('#PaymentsLinkPortal').html(html);
        });
    },
}