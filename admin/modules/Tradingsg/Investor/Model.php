<?
class CPL_Admin_Modules_Tradingsg_Investor_Model extends CP_Common_Lib_ModuleModelAbstract
{
    /**
     *
     */
    function getSQL() {

        $SQL = "
        SELECT s.*
              ,gc.name AS country_name
        FROM investor s
        LEFT JOIN (geo_country gc) ON (s.address_country = gc.country_code)
        ";

        return $SQL;
    }

    /**
     *
     */
    function setSearchVar($linkRecType = '') {
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $searchVar = Zend_Registry::get('searchVar');
        $searchVar->mainTableAlias = 's';

        $status       = $fn->getReqParam('status');
        $investor_id   = $fn->getReqParam('investor_id');
        $first_name = $fn->getReqParam('first_name');

        if ($investor_id != "") {
            $searchVar->sqlSearchVar[] = "s.investor_id = '{$investor_id}'";
        } else if ($tv['record_id'] != '') {
            $searchVar->sqlSearchVar[] = "s.investor_id = '{$tv['record_id']}'";
        } else {
            $fn->setSearchVarForLinkData($searchVar, $linkRecType, 's.investor_id');


            if ($status != "") {
                $searchVar->sqlSearchVar[] = "s.status = '{$status}'";
            }
            else {
                $searchVar->sqlSearchVar[] = "s.status = 'Active'";
            }

            if ($first_name != "") {
                $searchVar->sqlSearchVar[] = "s.first_name LIKE '%{$first_name}%'";
            }

            if ($_SESSION['userGroupName'] == "investor") {
                $searchVar->sqlSearchVar[] = "s.investor_id = '{$_SESSION['investor_id']}'";
            }

            if ($tv['keyword'] != "") {
                $searchVar->sqlSearchVar[] = "(
                    s.first_name  LIKE '%{$tv['keyword']}%'
                    OR s.email      LIKE '%{$tv['keyword']}%'
                )";
            }

            //------------------------------------------------------------------------//
            if ($tv['special_search'] == "Flagged") {
                $searchVar->sqlSearchVar[] = "s.flag = 1";
            }

            if ($tv['special_search'] == "Not-Flagged") {
                $searchVar->sqlSearchVar[] = "(s.flag != 1 OR s.flag IS null)";
            }

            $searchVar->sortOrder = "s.first_name";
        }
    }

    /**
     *
     */
    function getNewValidate() {
        $validate = Zend_Registry::get('validate');

        $validate->resetErrorArray();
        $validate->validateData('first_name', 'Please enter the investor name');

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    function getAdd(){
        $fn = Zend_Registry::get('fn');
        $validate = Zend_Registry::get('validate');

        if (!$this->getNewValidate()){
            return $validate->getErrorMessageXML();
        }

        $fa = $this->getFields();
        $id = $fn->addRecord($fa);
        $fn->returnAfterNewSave($id);
    }

    /**
     *
     */
    function getEditValidate() {
        $validate = Zend_Registry::get('validate');

        $validate->resetErrorArray();

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    function getSave(){
        $fn = Zend_Registry::get('fn');
        $validate = Zend_Registry::get('validate');
        $cpCfg = Zend_Registry::get('cpCfg');

        if (!$this->getEditValidate()){
            return $validate->getErrorMessageXML();
        }

        $fa = $this->getFields();
        $fa['status'] = 'Active'
        $id = $fn->saveRecord($fa);
        //$fn->returnAfterNewSave($id, $cpCfg['cp.pagetoReturnAfterSave']);
        $fn->returnAfterNewSave($id);
    }

    /**
     *
     */
    function getSaveList(){
        $fn = Zend_Registry::get('fn');
        $fn->getSaveList();
    }

    /**
     *
     */
    function getFields(){
        $fn = Zend_Registry::get('fn');

        $fa = array();
        $fa = $fn->addToFieldsArray($fa, 'first_name');
        $fa = $fn->addToFieldsArray($fa, 'code');
        $fa = $fn->addToFieldsArray($fa, 'website');
        $fa = $fn->addToFieldsArray($fa, 'company_size');
        $fa = $fn->addToFieldsArray($fa, 'industry');
        $fa = $fn->addToFieldsArray($fa, 'source');
        $fa = $fn->addToFieldsArray($fa, 'address_flat');
        $fa = $fn->addToFieldsArray($fa, 'address_street');
        $fa = $fn->addToFieldsArray($fa, 'address_town');
        $fa = $fn->addToFieldsArray($fa, 'address_state');
        $fa = $fn->addToFieldsArray($fa, 'address_country');
        $fa = $fn->addToFieldsArray($fa, 'address_po_code');
        $fa = $fn->addToFieldsArray($fa, 'return_address_flat');
        $fa = $fn->addToFieldsArray($fa, 'return_address_street');
        $fa = $fn->addToFieldsArray($fa, 'return_address_town');
        $fa = $fn->addToFieldsArray($fa, 'return_address_state');
        $fa = $fn->addToFieldsArray($fa, 'return_address_country');
        $fa = $fn->addToFieldsArray($fa, 'phone');
        $fa = $fn->addToFieldsArray($fa, 'fax');
        $fa = $fn->addToFieldsArray($fa, 'group_name');
        $fa = $fn->addToFieldsArray($fa, 'status');
        $fa = $fn->addToFieldsArray($fa, 'category');
        $fa = $fn->addToFieldsArray($fa, 'source');
        $fa = $fn->addToFieldsArray($fa, 'industry');
        $fa = $fn->addToFieldsArray($fa, 'company_size');
        $fa = $fn->addToFieldsArray($fa, 'investor_type');
        $fa = $fn->addToFieldsArray($fa, 'customer_type');
        $fa = $fn->addToFieldsArray($fa, 'alternate_phone');
        $fa = $fn->addToFieldsArray($fa, 'email');
        $fa = $fn->addToFieldsArray($fa, 'address2');

        return $fa;
    }

    /**
     *
     */
    function getAddInvestor() {
        $fn = Zend_Registry::get('fn');
        $validate = Zend_Registry::get('validate');
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpCfg = Zend_Registry::get('cpCfg');

        if (!$this->getAddInvestorValidate()){
            return $validate->getErrorMessageXML();
        }

        $first_name  = $fn->getPostParam('first_name');
        $website    = $fn->getPostParam('website');
        $phone  = $fn->getPostParam('phone');
        $gst_no  = $fn->getPostParam('gst_no');
        $address_flat  = $fn->getPostParam('address_flat');
        $address_street  = $fn->getPostParam('address_street');
        $address_town  = $fn->getPostParam('address_town');
        $address_state  = $fn->getPostParam('address_state');
        $address_country  = $fn->getPostParam('address_country');

        $fa = array();
        $fa['first_name']   = $first_name;
        $fa['website']   = $website;
        $fa['phone'] = $phone;
        $fa['gst_no']     = $gst_no;
        $fa['address_flat']   = $address_flat;
        $fa['address_street']   = $address_street;
        $fa['address_town']   = $address_town;
        $fa['address_state']   = $address_state;
        $fa['address_country']   = $address_country;

        $investor_id = $fn->addRecord($fa, 'investor');

        return $validate->getSuccessMessageXML();
    }

    /**
     *
     */
    function getAddInvestorValidate() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');

        $validate->resetErrorArray();

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    function getInvestorList(){
        $db = Zend_Registry::get('db');
        $fn = Zend_Registry::get('fn');

        $rows = "";

        $json  = array();
        
        $SQL = "
        SELECT investor_id
              ,first_name
        FROM investor 
        ORDER BY first_name
        ";
        $result   = $db->sql_query($SQL);  

        $json[] = array("value" => "", "caption" => "Please Select");
        while ($row = $db->sql_fetchrow($result)) {
            $json[] = array("value" => $row['investor_id'], "caption" => $row['first_name']);
        }
        
        return json_encode($json);
    }

    /**
     *
     */
    function getInvestmentsPaymentFormSubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getInvestmentsPaymentValidate()){
            return $validate->getErrorMessageXML();
        }

        $investor_id = $fn->getPostParam('investor_id');
        $date        = $fn->getPostParam('date');
        $amount      = $fn->getPostParam('amount');
        $description = $fn->getPostParam('description');
        $status      = $fn->getPostParam('status');
        //$chit_type   = $fn->getPostParam('chit_type');

        $fa = array();
        $fa['date']          = $date;
        $fa['amount']        = $amount;
        $fa['status']        = $status;
        $fa['description']   = $description;
        $fa['investor_id']   = $investor_id;
        //$fa['chit_type']   = $chit_type;
        $fa['creation_date'] = date("Y-m-d H:i:s");
        $fa['created_by']    = $fn->getSessionParam('userName');

        $insertPaymentSQL = $dbUtil->getInsertSQLStringFromArray($fa, 'investor_investments');
        $resultPaymentSQL = $db->sql_query($insertPaymentSQL);
        $id               = $db->sql_nextid();

        return $validate->getSuccessMessageXML();
    }

    /**
     *
     */
    function getInvestmentsPaymentValidate() {
        $validate = Zend_Registry::get('validate');

        $validate->resetErrorArray();
        $validate->validateData('amount', 'Please Enter Amount');
        $validate->validateData('date', 'Please Select Date');
        //$validate->validateData('chit_type', 'Please Select Type');

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     *
     */
    function getEditInvestmentsPaymentFormSubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getInvestmentsPaymentValidate()){
            return $validate->getErrorMessageXML();
        }

        $investor_id             = $fn->getPostParam('investor_id');
        $date                    = $fn->getPostParam('date');
        $amount                  = $fn->getPostParam('amount');
        $description             = $fn->getPostParam('description');
        $investor_investments_id = $fn->getPostParam('investor_investments_id');
        $status                  = $fn->getPostParam('status');
        //$chit_type             = $fn->getPostParam('chit_type');

        $fa1 = array();
        $fa1['date']              = $date;
        $fa1['amount']            = $amount;
        $fa1['description']       = $description;
        $fa1['status']            = $status;
        //$fa1['chit_type']       = $chit_type;
        $fa1['modification_date'] = date("Y-m-d H:i:s");
        $fa1['modified_by']       = $fn->getSessionParam('userName');

        $whereConditionPayment = "WHERE investor_investments_id = {$investor_investments_id}" ;
        $sqlUpdatePayment      = $dbUtil->getUpdateSQLStringFromArray($fa1, "investor_investments", $whereConditionPayment);
        $resultUpdatePayment   = $db->sql_query($sqlUpdatePayment);

        return $validate->getSuccessMessageXML();
    }

    /**
     *
     */
    function getDeleteInvestmentsPayment(){
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpCfg = Zend_Registry::get('cpCfg');
        $cpUtil = Zend_Registry::get('cpUtil');

        $investor_id = $fn->getReqParam('investor_id');
        $investor_investments_id = $fn->getReqParam('investor_investments_id');

        $SQL ="
               DELETE FROM investor_investments
               WHERE investor_investments_id = {$investor_investments_id}
               ";
        $result = $db->sql_query($SQL);
    }

    /**
     *
     */
    function getInvestorPaymentFormSubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getInvestorPaymentValidate()){
            return $validate->getErrorMessageXML();
        }

        $investor_id  = $fn->getPostParam('investor_id');
        $date        = $fn->getPostParam('date');
        $amount      = $fn->getPostParam('amount');
        $description = $fn->getPostParam('description');
        //$chit_type   = $fn->getPostParam('chit_type');

        $fa = array();
        $fa['date']          = $date;
        $fa['amount']        = $amount;
        $fa['description']   = $description;
        $fa['investor_id']   = $investor_id;
        //$fa['chit_type']     = $chit_type;
        $fa['creation_date'] = date("Y-m-d H:i:s");
        $fa['created_by']    = $fn->getSessionParam('userName');

        $insertPaymentSQL = $dbUtil->getInsertSQLStringFromArray($fa, 'investor_payments');
        $resultPaymentSQL = $db->sql_query($insertPaymentSQL);

        $this->getInvestmentStatusUpdate($investor_id);

        return $validate->getSuccessMessageXML();
    }

    /**
     *
     */
    function getInvestorPaymentValidate() {
        $validate = Zend_Registry::get('validate');

        $validate->resetErrorArray();
        $validate->validateData('amount', 'Please Enter Amount');
        $validate->validateData('date', 'Please Select Date');
        //$validate->validateData('chit_type', 'Please Select Type');

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     *
     */
    function getEditInvestorPaymentFormSubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getInvestorPaymentValidate()){
            return $validate->getErrorMessageXML();
        }

        $investor_id          = $fn->getPostParam('investor_id');
        $date                 = $fn->getPostParam('date');
        $amount               = $fn->getPostParam('amount');
        $description          = $fn->getPostParam('description');
        $investor_payments_id = $fn->getPostParam('investor_payments_id');
        //$chit_type         = $fn->getPostParam('chit_type');

        $fa1 = array();
        $fa1['date']              = $date;
        $fa1['amount']            = $amount;
        $fa1['description']       = $description;
        //$fa1['chit_type']         = $chit_type;
        $fa1['modification_date'] = date("Y-m-d H:i:s");
        $fa1['modified_by']       = $fn->getSessionParam('userName');

        $whereConditionPayment = "WHERE investor_payments_id = {$investor_payments_id}" ;
        $sqlUpdatePayment      = $dbUtil->getUpdateSQLStringFromArray($fa1, "investor_payments", $whereConditionPayment);
        $resultUpdatePayment   = $db->sql_query($sqlUpdatePayment);

        $this->getInvestmentStatusUpdate($investor_id);

        return $validate->getSuccessMessageXML();
    }

    /**
     *
     */
    function getDeleteInvestorPayment(){
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpCfg = Zend_Registry::get('cpCfg');
        $cpUtil = Zend_Registry::get('cpUtil');

        $investor_id = $fn->getReqParam('investor_id');
        $investor_payments_id = $fn->getReqParam('investor_payments_id');

        $SQL ="
               DELETE FROM investor_payments
               WHERE investor_payments_id = {$investor_payments_id}
               ";
        $result = $db->sql_query($SQL);

        $this->getInvestmentStatusUpdate($investor_id);
    }

    /**
     *
     */
    function getInvestmentStatusUpdate($investor_id){
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        
        $SQLInvestment = "
        SELECT SUM(amount) AS Investments
        FROM investor_investments
        WHERE investor_id = {$investor_id}
        ";
        $resultInvestment = $db->sql_query($SQLInvestment);
        $rowInvestment    = $db->sql_fetchrow($resultInvestment);

        $SQLPayments = "
        SELECT SUM(amount) AS Payments
        FROM investor_payments
        WHERE investor_id = {$investor_id}
        ";
        $resultPayments = $db->sql_query($SQLPayments);
        $rowPayments    = $db->sql_fetchrow($resultPayments);

        $statusInvestment = 'Due';
        if($rowPayments['Payments'] > 0){
            if (round($rowPayments['Payments']) >= round($rowInvestment['Investments'])){
                $statusInvestment = 'Paid';
            } else if (round($rowPayments['Payments']) < round($rowInvestment['Investments'])){
                $statusInvestment = 'Partially Paid';
            }
        }

        /*$SQLStatusUpdate = "
        UPDATE investor_investments SET status = '{$statusInvestment}'
        WHERE investor_id = {$investor_id}
        ";
        $resultStatusUpdate = $db->sql_query($SQLStatusUpdate);*/
    }
}
