<?
class CPL_Admin_Modules_Tradingsg_Investor_Controller extends CP_Common_Lib_ModuleControllerAbstract
{
    function getNewInvestor() {
        return $this->view->getNewInvestor();
    }

    function getAddInvestor() {
        return $this->model->getAddInvestor();
    }

    function getInvestorList(){
        return $this->model->getInvestorList();
    }

    function getAddInvestmentsPayment() {
        return $this->view->getAddInvestmentsPayment();
    }
    
    function getInvestmentsPayment() {
        return $this->view->getInvestmentsPayment();
    }

    function getInvestmentsPaymentFormSubmit(){
        return $this->model->getInvestmentsPaymentFormSubmit();
    }

    function getEditInvestmentsPayment() {
        return $this->view->getEditInvestmentsPayment();
    }

    function getEditInvestmentsPaymentFormSubmit() {
        return $this->model->getEditInvestmentsPaymentFormSubmit();
    }

    function getDeleteInvestmentsPayment() {
        return $this->model->getDeleteInvestmentsPayment();
    }

    function getAddInvestorPayment() {
        return $this->view->getAddInvestorPayment();
    }

	function getInvestorPayment() {
        return $this->view->getInvestorPayment();
    }

    function getEditInvestorPayment() {
        return $this->view->getEditInvestorPayment();
    }

    function getInvestorPaymentFormSubmit() {
        return $this->model->getInvestorPaymentFormSubmit();
    }

    function getEditInvestorPaymentFormSubmit() {
        return $this->model->getEditInvestorPaymentFormSubmit();
    }

    function getDeleteInvestorPayment() {
        return $this->model->getDeleteInvestorPayment();
    }

}