<?
class CPL_Admin_Modules_Tradingsg_Customer_View extends CP_Common_Lib_ModuleViewAbstract
{
    /**
     *
     */
    function getList($dataArray){
        $listObj = Zend_Registry::get('listObj');
        $db      = Zend_Registry::get('db');

        $count   = 0;
        $rows    = '';
        $chit_no = "";

        foreach ($dataArray as $row){
            $email     = $row['email'];
            $website   = $row['website'];

            $rows .= "
            {$listObj->getListRowHeader($row, $count)}
            {$listObj->getGoToDetailText($count, $row['company_name'])}
            {$listObj->getListDataCell($row['mobile'])}
            {$listObj->getListDataCell($row['status'])}
            {$listObj->getListRowEnd($row['company_id'])}
            ";

            $count++ ;
        }

        $text = "
        {$listObj->getListHeader()}
        {$listObj->getListHeaderCell('Name', 'c.company_name')}
        {$listObj->getListHeaderCell('Phone Number', 'c.mobile')}
        {$listObj->getListHeaderCell('Status', 'c.status')}
        {$listObj->getListHeaderEnd()}
        {$rows}
        {$listObj->getListFooter()}
        ";

        return $text;
    }

    /**
     *
     */
    function getNew(){
        $formObj = Zend_Registry::get('formObj');

        $fielset1 = "
        {$formObj->getTBRow('Customer Name', 'company_name')}
        ";

        $text = "
        {$formObj->getFieldSetWrapped('Key Details', $fielset1)}
        ";

        return $text;
    }

    /**
     *
     */
    function getEdit($row){
        $formObj = Zend_Registry::get('formObj');
        $cpCfg = Zend_Registry::get('cpCfg');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');

        $formObj->mode = $tv['action'];

        $discountPercent = '';
        $cstNo = '';
        $tinNo = '';

        $sqlStatus   = $fn->getValueListSQL('companyStatus');
        $sqlSupplier = $fn->getValueListSQL('supplierType');
        $sqlIndustry = $fn->getValueListSQL('companyIndustry');
        $sqlSize     = $fn->getValueListSQL('companySize');
        $sqlSource   = $fn->getValueListSQL('companySource');
        $sqlCategory = $fn->getValueListSQL('companyCategory');       

        $sqlCountry = getCPModelObj('common_geoCountry')->getCountryDDSQL();
        $expCountry = array('detailValue' => $row['country_name']);

        $expVl = array('sqlType' => 'OneField');

        if ($cpCfg['m.tradingsg.company.hasCstNo']) {
            $cstNo = $formObj->getTBRow('Cst No', 'cst_no', $row['cst_no']);
        }

        if ($cpCfg['m.tradingsg.company.hasGstNo']) {
            $gstNo = $formObj->getTBRow('Gst No', 'gst_no', $row['gst_no']);
        }

        if ($cpCfg['m.tradingsg.company.hasTinNo']) {
            $tinNo = $formObj->getTBRow('Tin No', 'tin_no', $row['tin_no']);
        }

        $creation_date = $fn->getCPDate($row['creation_date'], 'd-m-Y-H-i-s');
        $modification_date = $fn->getCPDate($row['modification_date'], 'd-m-Y-H-i-s');

        $statusArr = array(
             "Active"
            ,"Archive"
        );

        $text = "
        <div class='linkPortalWrapper'>
            <div expanded='1' class='header'>
                <div class='floatbox'>
                    <div class='float_left'>Customer Details</div>
                    <div class='toggle'></div>
                    <div class='float_right'>Creation : {$row['created_by']} on {$creation_date} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Modified : {$row['modified_by']} {$modification_date}</div>
                </div>
            </div>
            <div>
                <div class='linkPortalDataWrapper'>
                    <div class='col-md-3'>{$formObj->getTBRow('Name', 'company_name', $row['company_name'])}</div>
                    <div class='col-md-2'>{$formObj->getTBRow('Phone Number', 'mobile', $row['mobile'])}</div>
                    <div class='col-md-3'>{$formObj->getTBRow('Street Address', 'address_flat', $row['address_flat'])}</div>
                    <div class='col-md-2'>{$formObj->getTBRow('Town', 'address_town', $row['address_town'])}</div>
                    <div class='col-md-2'>{$formObj->getTBRow('City', 'address_street', $row['address_street'])}</div>
                    <div class='col-md-2'>{$formObj->getTBRow('State', 'address_state', $row['address_state'])}</div>
                    <div class='col-md-2'>{$formObj->getTBRow('Postal Code', 'address_po_code', $row['address_po_code'])}</div>
                    <div class='col-md-2'>{$formObj->getDDRowByArr('Status', 'status', $statusArr, $row['status'])}</div>
                </div>
            </div>
        </div>
        ";

        return $text;
    }

    /**
     *
     */
    function getPrintDetail($row){
        $db = Zend_Registry::get('db');
        return $this->getDetail($row);
    }

    /**
     *
     */
    function getSearch(){
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $sqlCategory = $fn->getValueListSQL('companyCategory');
        $sqlStatus   = $fn->getValueListSQL('companyStatus');
        $expVl = array('sqlType' => 'OneField');

        $spArray = array(
            "Flagged"
           ,"Not-Flagged"
        );

        $fielset = "
        {$formObj->getTBRow('Customer Name', 'company_name')}
        {$formObj->getDDRowBySQL('Choose Category', 'category', $sqlCategory, 'Client', $expVl)}
        {$formObj->getDDRowBySQL('Status', 'status', $sqlStatus, 'Current', $expVl)}
        {$formObj->getDDRowByArr('Special Search', 'special_search', $spArray)}
        ";

        $text = "
        {$formObj->getFieldSetWrapped('Company Details', $fielset)}
        ";

        return $text;
    }

    /**
     *
     */
    function getRightPanel($row){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $displayLinkData = Zend_Registry::get('displayLinkData');
        $media = Zend_Registry::get('media');

        $record_id = $fn->getIssetParam($row, 'company_id');

        $text = "
        <div id='CustomerFinanceLinkPortal'>{$this->getAddCustomerFinance($record_id)}</div>
        <div id='ClientPaymentLinkPortal'>{$this->getAddClientPayment($record_id)}</div>
        {$displayLinkData->getLinkPortalMain('tradingsg_customer', 'tradingsg_contactLink', 'Contacts Linked', $row)}
        {$media->getRightPanelMediaDisplay('Attachments', 'tradingsg_customer', 'attachment', $row)}
        ";

        return $text;
    }
    /**
     *
     */
    function getAddClientPayment($company_id=''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($company_id == ''){
            $company_id = $fn->getReqParam('company_id');
        }

        $ClientPayment = $this->getAddClientPaymentDetail($company_id);

        $recCount = $fn->getRecordCount('customer_payment', "company_id = '{$company_id}'");

        $header ="
        <thead>
            <tr>
                <th>Date</th>
                <th class='txtRight'>Amount</th>
                <th>Notes</th>
                <th class='portalActBtns'></th>
                <th class='portalActBtns'></th>
            </tr>
        </thead>
        ";

        if($recCount == 0){
            $header = "<tr><td align='center'>No Records Linked<br/><br/></td></tr>";
        }

        $formActionClientPayment = "index.php?module=tradingsg_customer&_spAction=ClientPayment&company_id={$company_id}&showHTML=0";

        $add = "<div class='btn btn-info'>
                    <a id='AddClientPayment' href='{$formActionClientPayment}' company_id='{$company_id}'>
                        Add Payment
                    </a>
                </div>
                ";


        $SQLOverallTotal = "
        SELECT  SUM(amount) AS overall_Total
        FROM customer_payment
        WHERE company_id = '{$company_id}'
        ";
        $resultOverallTotal = $db->sql_query($SQLOverallTotal);
        $rowOverallTotal    = $db->sql_fetchrow($resultOverallTotal);

        $overall_Total = number_format($rowOverallTotal['overall_Total'], 2);

        $text = "
        <div class='linkPortalWrapper tradingsg_customer_paymentLink'>
            <div class='header'>
                <div class='floatbox'>
                    {$add}
                </div>
                <div class='float_right grandTotalPurchasePo'>
                    Total Payment: {$overall_Total}
                </div>
            </div>
            <div class='header' expanded='1'>
                <div class='floatbox'>
                    <div class='float_left rightpanelHeading'>Payments</div>
                    <div class='txtRight'>
                        <span class='count'>({$recCount})</span>
                    </div>
                </div>
            </div>
            <div class='linkPortalDataWrapper'>
                <form>
                    <table class='renewallist'>
                        {$header}
                        <tbody id='AddClientPaymentPortal'>
                            {$ClientPayment}
                        </tbody>
                    </table>
                    <input type='hidden' name='company_id' value='{$company_id}' />
                </form>
            </div>
        </div>
        ";

        return $text;

    }

    /**
     *
     */
    function getAddClientPaymentDetail($company_id = ''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($company_id == ''){
            $company_id = $fn->getReqParam('company_id');
        }

        $customer_payment_id = $fn->getReqParam('customer_payment_id');

        $rows  = "";

        $SQL = "
        SELECT cp.*
        FROM customer_payment cp
        WHERE company_id = '{$company_id}'
        ";
        $result  = $db->sql_query($SQL);
        $numRows = $db->sql_numrows($result);
        $amount  = 0;
        $count   = 1;
        while ($row = $db->sql_fetchrow($result)) {

            $formActionEditClientPayment   = "index.php?module=tradingsg_customer&_spAction=EditClientPayment&customer_payment_id={$row['customer_payment_id']}&company_id={$company_id}&showHTML=0";

            $deleteIcon ="
                <div class='float_right'>
                    <a class='deleteClientPayment' href='#'  customer_payment_id='{$row['customer_payment_id']}' company_id='{$row['company_id']}'>
                        <img src='/cmspilotv30/CP/admin/images/icons/btn_remove.png'>
                    </a>
                </div>
                ";

            $editIcon ="
                <div class='float_right'>
                    <a class='EditClientPayment' href='{$formActionEditClientPayment}' customer_payment_id='{$row['customer_payment_id']}'  company_id='{$row['company_id']}'>
                        <img src='/cmspilotv30/CP/admin/images/icons/btn_edit.png'>
                    </a>
                </div>
                ";

            $amount = number_format($row['amount'], 2);

            $rows .= "
                <tr>
                    <td>{$fn->getCPDate($row['date'], 'd-m-Y')}</td>
                    <td class='txtRight'>{$amount}</td>
                    <td>{$row['description']}</td>
                    <td>
                        {$editIcon}
                    </td>
                    <td>
                        {$deleteIcon}
                    </td>
                </tr>
            ";
            $count++;
        }

        $text="{$rows}";

        return $text;
    }
    /**
     *
     */
    function getClientPayment() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $expVl = array('sqlType' => 'OneField');

        $company_id  = $fn->getReqParam('company_id');
        $date = date("Y-m-d");

        $chitTypeArr = array(
             "Daily"
            ,"Monthly"
        );

        $formAction = "index.php?_topRm=main&module=tradingsg_customer&_spAction=ClientPaymentFormSubmit&showHTML=0";

        $text = "
        <form id='clientPaymentPortalForm' class='yform columnar' method='post' action='{$formAction}'>
            {$formObj->getDateRow('Date', 'date', $date)}
            {$formObj->getTBRow('Amount', 'amount')}
            {$formObj->getTARow('Notes', 'description')}
            <input type='hidden' name='company_id' value='{$company_id}' />
        </form>
        ";
        return $text;
    }
     /**
     *
     */
    function getEditClientPayment() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $expVl = array('sqlType' => 'OneField');
        $company_id  = $fn->getReqParam('company_id');
        $customer_payment_id  = $fn->getReqParam('customer_payment_id');

        if($customer_payment_id == ''){
            $customer_payment_id  = $fn->getReqParam('customer_payment_id');
        }

        $chitTypeArr = array(
             "Daily"
            ,"Monthly"
        );

        $rows  = "";

        $formAction = "index.php?module=tradingsg_customer&_spAction=EditClientPaymentFormSubmit&showHTML=0&customer_payment_id={$customer_payment_id}";

        $SQLPayment="
        SELECT cp.*
        FROM customer_payment cp
        WHERE customer_payment_id = '{$customer_payment_id}'
        ";
        $resultPayment   = $db->sql_query($SQLPayment);
        $rowPayment = $db->sql_fetchrow($resultPayment);

        $rows .= "
        <form id='portalForm' class='yform columnar' method='post' action='{$formAction}'>
            {$formObj->getDateRow('Date', 'date', $rowPayment['date'])}
            {$formObj->getTBRow('Amount', 'amount', $rowPayment['amount'])}
            {$formObj->getTARow('Notes', 'description', $rowPayment['description'])}
            <input type='hidden' name='customer_payment_id' value='{$customer_payment_id}' />
            <input type='hidden' name='company_id' value='{$company_id}' />
        </form>
        ";        

        $text="{$rows}";

        return $text;
    }

    /**
     *
     */
    function getAddCustomerFinance($company_id=''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($company_id == ''){
            $company_id = $fn->getReqParam('company_id');
        }

        $CustomerFinance = $this->getAddCustomerFinanceDetail($company_id);

        $recCount = $fn->getRecordCount('customer_finance', "company_id = '{$company_id}'");

        $header ="
        <thead>
            <tr>
                <th>Date</th>
                <th class='txtRight'>Amount</th>
                <th>Notes</th>
                <th>Status</th>
                <th class='portalActBtns'></th>
                <th class='portalActBtns'></th>
            </tr>
        </thead>
        ";

        if($recCount == 0){
            $header = "<tr><td align='center'>No Records Linked<br/><br/></td></tr>";
        }

        $formActionCustomerFinance = "index.php?module=tradingsg_customer&_spAction=customerFinance&company_id={$company_id}&showHTML=0";

        $add = "<div class='btn btn-info'>
                    <a id='AddCustomerFinance' href='{$formActionCustomerFinance}' company_id='{$company_id}'>
                        Add Finance
                    </a>
                </div>
                ";


        $SQLOverallTotal = "
        SELECT  SUM(amount) AS overall_Total
        FROM customer_finance
        WHERE company_id = '{$company_id}'
        ";
        $resultOverallTotal = $db->sql_query($SQLOverallTotal);
        $rowOverallTotal    = $db->sql_fetchrow($resultOverallTotal);

        $overall_Total = number_format($rowOverallTotal['overall_Total'], 2);

        $SQLOverallBalance = "
        SELECT  SUM(amount) AS overall_paid
        FROM customer_payment
        WHERE company_id = '{$company_id}'
        ";
        $resultOverallBalance = $db->sql_query($SQLOverallBalance);
        $rowOverallBalance    = $db->sql_fetchrow($resultOverallBalance);

        $overallBalance = $rowOverallTotal['overall_Total'] - $rowOverallBalance['overall_paid'];
        $overallBalance = number_format($overallBalance, 2);

        $text = "
        <div class='linkPortalWrapper tradingsg_customer_paymentLink'>
            <div class='header'>
                <div class='floatbox'>
                    {$add}
                </div>
                <div class='float_right grandTotalPurchasePo'>
                    Total Finance: {$overall_Total} | Total Balance: {$overallBalance}
                </div>
            </div>
            <div class='header' expanded='1'>
                <div class='floatbox'>
                    <div class='float_left rightpanelHeading'>Finance</div>
                    <div class='txtRight'>
                        <span class='count'>({$recCount})</span>
                    </div>
                </div>
            </div>
            <div class='linkPortalDataWrapper'>
                <form>
                    <table class='renewallist'>
                        {$header}
                        <tbody id='AddClientPaymentPortal'>
                            {$CustomerFinance}
                        </tbody>
                    </table>
                    <input type='hidden' name='company_id' value='{$company_id}' />
                </form>
            </div>
        </div>
        ";

        return $text;

    }

    /**
     *
     */
    function getAddCustomerFinanceDetail($company_id = ''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($company_id == ''){
            $company_id = $fn->getReqParam('company_id');
        }

        $customer_finance_id = $fn->getReqParam('customer_finance_id');

        $rows  = "";

        $SQL = "
        SELECT cp.*
        FROM customer_finance cp
        WHERE company_id = '{$company_id}'
        ";
        $result  = $db->sql_query($SQL);
        $numRows = $db->sql_numrows($result);
        $amount  = 0;
        $count   = 1;
        while ($row = $db->sql_fetchrow($result)) {

            $formActionEditCustomerFinance   = "index.php?module=tradingsg_customer&_spAction=EditCustomerFinance&customer_finance_id={$row['customer_finance_id']}&company_id={$company_id}&showHTML=0";

            $deleteIcon ="
                <div class='float_right'>
                    <a class='deleteCustomerFinance' href='#'  customer_finance_id='{$row['customer_finance_id']}' company_id='{$row['company_id']}'>
                        <img src='/cmspilotv30/CP/admin/images/icons/btn_remove.png'>
                    </a>
                </div>
                ";

            $editIcon ="
                <div class='float_right'>
                    <a class='EditCustomerFinance' href='{$formActionEditCustomerFinance}' customer_finance_id='{$row['customer_finance_id']}'  company_id='{$row['company_id']}'>
                        <img src='/cmspilotv30/CP/admin/images/icons/btn_edit.png'>
                    </a>
                </div>
                ";

            $amount = number_format($row['amount'], 2);

            $rows .= "
                <tr>
                    <td>{$fn->getCPDate($row['date'], 'd-m-Y')}</td>
                    <td class='txtRight'>{$amount}</td>
                    <td>{$row['description']}</td>
                    <td>{$row['status']}</td>
                    <td>
                        {$editIcon}
                    </td>
                    <td>
                        {$deleteIcon}
                    </td>
                </tr>
            ";
            $count++;
        }

        $text="{$rows}";

        return $text;
    }
    /**
     *
     */
    function getCustomerFinance() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $expVl = array('sqlType' => 'OneField');

        $company_id  = $fn->getReqParam('company_id');
        $date = date("Y-m-d");

        $statusArr = array(
             "Due"
            ,"Partially Paid"
            ,"Paid"
            ,"Cancelled"
        );

        $formAction = "index.php?_topRm=main&module=tradingsg_customer&_spAction=customerFinanceFormSubmit&showHTML=0";

        $text = "
        <form id='customerFinancePortalForm' class='yform columnar' method='post' action='{$formAction}'>
            {$formObj->getDateRow('Date', 'date', $date)}
            {$formObj->getTBRow('Amount', 'amount')}
            {$formObj->getTARow('Notes', 'description')}
            {$formObj->getDDRowByArr('Status', 'status', $statusArr, 'Due')}
            <input type='hidden' name='company_id' value='{$company_id}' />
        </form>
        ";
        return $text;
    }
     /**
     *
     */
    function getEditCustomerFinance() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $expVl = array('sqlType' => 'OneField');
        $company_id  = $fn->getReqParam('company_id');
        $customer_finance_id  = $fn->getReqParam('customer_finance_id');

        if($customer_finance_id == ''){
            $customer_finance_id  = $fn->getReqParam('customer_finance_id');
        }

        $statusArr = array(
             "Due"
            ,"Partially Paid"
            ,"Paid"
            ,"Cancelled"
        );

        $rows  = "";

        $formAction = "index.php?module=tradingsg_customer&_spAction=EditCustomerFinanceFormSubmit&showHTML=0&customer_finance_id={$customer_finance_id}";

        $SQLPayment="
        SELECT cp.*
        FROM customer_finance cp
        WHERE customer_finance_id = '{$customer_finance_id}'
        ";
        $resultPayment   = $db->sql_query($SQLPayment);
        $rowPayment = $db->sql_fetchrow($resultPayment);

        $rows .= "
        <form id='portalForm' class='yform columnar' method='post' action='{$formAction}'>
            {$formObj->getDateRow('Date', 'date', $rowPayment['date'])}
            {$formObj->getTBRow('Amount', 'amount', $rowPayment['amount'])}
            {$formObj->getTARow('Notes', 'description', $rowPayment['description'])}
            {$formObj->getDDRowByArr('Status', 'status', $statusArr, $rowPayment['status'])}
            <input type='hidden' name='customer_finance_id' value='{$customer_finance_id}' />
        </form>
        ";        

        $text="{$rows}";

        return $text;
    }

    /**
     *
     */
    function getAddOldGoldPayment($company_id=''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($company_id == ''){
            $company_id = $fn->getReqParam('company_id');
        }

        $OldGoldPayment = $this->getAddOldGoldPaymentDetail($company_id);

        $recCount = $fn->getRecordCount('old_gold_payments', "company_id = '{$company_id}'");

        $header ="
        <thead>
            <tr>
                <th>Date</th>
                <th>Description</th>
                <th class='txtRight'>Amount</th>
                <th>Gms</th>
                <th>Rate</th>
                <th>Category</th>
                <th class='portalActBtns'></th>
                <th class='portalActBtns'></th>
            </tr>
        </thead>
        ";

        if($recCount == 0){
            $header = "<tr><td align='center'>No Records Linked<br/><br/></td></tr>";
        }

        $formActionOldGoldPayment = "index.php?module=tradingsg_customer&_spAction=OldGoldPayment&company_id={$company_id}&showHTML=0";

        $add = "<div class='actBtns'>
                    <a id='AddOldGoldPayment' href='{$formActionOldGoldPayment}' company_id='{$company_id}'>
                        Add
                    </a>
                </div>
                ";


        $text = "
        <div class='linkPortalWrapper tradingsg_old_gold_paymentLink'>
            <div class='header' expanded='1'>
                <div class='floatbox'>
                    <div class='float_left rightpanelHeading'>Old Gold Payment To Customer(Without Sales)</div>
                    <div class='txtRight'>
                        <span class='count'>({$recCount})</span>
                    </div>
                </div>
            </div>
            <div class='linkPortalDataWrapper'>
                <form>
                    <table class='renewallist'>
                        {$header}
                        <tbody id='AddOldGoldPaymentPortal'>
                            {$OldGoldPayment}
                        </tbody>
                    </table>
                    <input type='hidden' name='company_id' value='{$company_id}' />
                </form>
            </div>
            {$add}
        </div>
        ";

        return $text;

    }
    /**
     *
     */
    function getAddOldGoldPaymentDetail($company_id = ''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($company_id == ''){
            $company_id = $fn->getReqParam('company_id');
        }

        $old_gold_payments_id = $fn->getReqParam('old_gold_payments_id');

        $rows  = "";

        $SQL = "
        SELECT gp.*
        FROM old_gold_payments gp
        WHERE company_id = '{$company_id}'
        ";
        $result  = $db->sql_query($SQL);
        $numRows = $db->sql_numrows($result);
        $amount  = 0;
        $count   = 1;
        while ($row = $db->sql_fetchrow($result)) {

            $formActionEditOldGoldPayment   = "index.php?module=tradingsg_customer&_spAction=EditOldGoldPayment&old_gold_payments_id={$row['old_gold_payments_id']}&company_id={$company_id}&showHTML=0";

            $deleteIcon ="
                <div class='float_right'>
                    <a class='deleteOldGoldPayment' href='#'  old_gold_payments_id='{$row['old_gold_payments_id']}' company_id='{$row['company_id']}'>
                        <img src='/cmspilotv30/CP/admin/images/icons/btn_remove.png'>
                    </a>
                </div>
                ";

            $editIcon ="
                <div class='float_right'>
                    <a class='EditOldGoldPayment' href='{$formActionEditOldGoldPayment}' old_gold_payments_id='{$row['old_gold_payments_id']}'  company_id='{$row['company_id']}'>
                        <img src='/cmspilotv30/CP/admin/images/icons/btn_edit.png'>
                    </a>
                </div>
                ";

            $amount = number_format($row['amount'], 2);

            $rows .= "
                <tr>
                    <td>{$fn->getCPDate($row['date'], 'd-m-Y')}</td>
                    <td>{$row['description']}</td>
                    <td class='txtRight'>{$amount}</td>
                    <td>{$row['gms']}</td>
                    <td>{$row['rate']}</td>
                    <td>{$row['category']}</td>
                    <td>
                        {$editIcon}
                    </td>
                    <td>
                        {$deleteIcon}
                    </td>
                </tr>
            ";
            $count++;
        }

        $text="{$rows}";

        return $text;
    }
    /**
     *
     */
    function getOldGoldPayment() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $expVl = array('sqlType' => 'OneField');

        $company_id  = $fn->getReqParam('company_id');
        $sqlCategory = $fn->getValueListSQL('companyCategory');       


        $formAction = "index.php?_topRm=main&module=tradingsg_customer&_spAction=OldGoldPaymentFormSubmit&showHTML=0";

        $text = "
        <form id='oldGoldPaymentPortalForm' class='yform columnar' method='post' action='{$formAction}'>
            {$formObj->getDateRow('Date', 'date')}
            {$formObj->getTARow('Description', 'description')}
            {$formObj->getTBRow('Amount', 'amount')}
            {$formObj->getTBRow('Gms', 'gms')}
            {$formObj->getTBRow('Rate', 'rate')}
            {$formObj->getDDRowBySQL('Category', 'category', $sqlCategory, '', $expVl)}
            <input type='hidden' name='company_id' value='{$company_id}' />
        </form>
        ";
        return $text;
    }
     /**
     *
     */
    function getEditOldGoldPayment() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $formObj = Zend_Registry::get('formObj');

        $expVl = array('sqlType' => 'OneField');
        $company_id  = $fn->getReqParam('company_id');
        $old_gold_payments_id  = $fn->getReqParam('old_gold_payments_id');
        $sqlCategory = $fn->getValueListSQL('companyCategory');       

        if($old_gold_payments_id == ''){
            $old_gold_payments_id  = $fn->getReqParam('old_gold_payments_id');
        }

        $rows  = "";

        $formAction = "index.php?module=tradingsg_customer&_spAction=EditOldGoldPaymentFormSubmit&showHTML=0&old_gold_payments_id={$old_gold_payments_id}";

        $SQLOldGold="
        SELECT gp.*
        FROM old_gold_payments gp
        WHERE old_gold_payments_id = '{$old_gold_payments_id}'
        ";
        $resultOldGold   = $db->sql_query($SQLOldGold);
        $rowOldGold = $db->sql_fetchrow($resultOldGold);

        $rows .= "
        <form id='portalForm' class='yform columnar' method='post' action='{$formAction}'>
            {$formObj->getDateRow('Date', 'date', $rowOldGold['date'])}
            {$formObj->getTARow('Description', 'description', $rowOldGold['description'])}
            {$formObj->getTBRow('Amount', 'amount', $rowOldGold['amount'])}
            {$formObj->getTBRow('Gms', 'gms', $rowOldGold['gms'])}
            {$formObj->getTBRow('Rate', 'rate', $rowOldGold['rate'])}
            {$formObj->getDDRowBySQL('Category', 'category', $sqlCategory, $rowOldGold['category'], $expVl)}
            <input type='hidden' name='old_gold_payments_id' value='{$old_gold_payments_id}' />
        </form>
        ";        

        $text="{$rows}";

        return $text;
    }

    /**
     *
     */
    function getProductKeyPortal($company_id=''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($company_id == ''){
            $company_id = $fn->getReqParam('company_id');
        }

        $ProductKey = $this->getProductKeyLink($company_id);

        $recCount = $fn->getRecordCount('lead_history', "company_id = '{$company_id}'");

        $header ="
        <thead>
            <tr>
                <th>Product Key</th>
                <th>No Of Users</th>
                <th>Status</th>
                <th>Activated Date</th>
                <th>Linked By & Date</th>
            </tr>
        </thead>
        ";

        if($recCount == 0){
            $header = "<tr><td align='center'>No Records Linked<br/><br/></td></tr>";
        }

        $formActionProductKeyLink = "index.php?module=tradingsg_customer&_spAction=AddProductKeyToCompany&company_id={$company_id}&showHTML=0";

        $add = "<div class='actBtns'>
                    <a class='AddProductKeyLinkButton'  href='{$formActionProductKeyLink}' company_id={$company_id}>Generate Key</a>
                </div>
                ";

        if($recCount > 0){
            $add = "";
        }

        $text = "
        <div class='linkPortalWrapper tradingsg_customer_productKeyLink'>
            <div class='header' expanded='1'>
                <div class='floatbox'>
                    <div class='float_left'>Product Key Linked</div>
                    <div class='txtRight'>
                        <span class='count' id='AddProductKeyPortalCount'>({$recCount})</span>
                        <div class='toggle'></div>
                    </div>
                </div>
            </div>
            <div class='linkPortalDataWrapper'>
                <form>
                    <table class='ProductKeyList'>
                        {$header}
                        <tbody id='AddProductKeyPortal'>
                            {$ProductKey}
                        </tbody>
                    </table>
                    <input type='hidden' name='company_id' value='{$company_id}' />
                </form>
            </div>
            {$add}
        </div>
        ";

        return $text;

    }

    /**
     *
     */
    function getProductKeyLink($company_id = ''){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        if($company_id == ''){
            $company_id = $fn->getReqParam('company_id');
        }

        $rows  = "";

        $SQL="
        SELECT product_name 
              ,trial_key
              ,active_key
              ,status
              ,active_date
              ,no_of_users
              ,linked_date
              ,linked_by
        FROM lead_history
        WHERE company_id = '{$company_id}'
        ";
        $result   = $db->sql_query($SQL);
        $numRows = $db->sql_numrows($result);

        $count = 1;
        $qty_balance = '';
        while ($row = $db->sql_fetchrow($result)) {
            $rows .= "
            <tr>
                <td>{$row['product_name']}</td>
                <td>{$row['trial_key']}</td>
                <td>{$row['active_key']}</td>
                <td>{$row['no_of_users']}</td>
                <td>{$row['status']}</td>
                <td>{$row['active_date']}</td>
                <td><i>{$row['linked_by']} - {$row['linked_date']}</i></td>
            </tr>
            ";
            $count++;
        }

        $text="{$rows}";

        return $text;
    }


    /**
     *
     */
    function getQuickSearch() {
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');

        $status   = $fn->getReqParam('status');

        $sqlStatus = $fn->getValueListSQL('companyStatus');

        $spArray = array(
            "Flagged"
           ,"Not-Flagged"
        );

        $statusArr = array(
            "Active"
           ,"Archive"
        );

        $text = "
        <td>
            <select name='status'>
                <option value=''>Status</option>
                {$cpUtil->getDropDown1($statusArr, $status)}
            </select>
        </td>
        <td>
            <select name='special_search'>
                <option value=''>Special Search</option
                {$cpUtil->getDropDown1($spArray, $tv['special_search'])}
           </select>
        </td>
        ";

        return $text;
    }

    /**
     *
     */
    function getAddProductKeyToCompany() {
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $cpCfg = Zend_Registry::get('cpCfg');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');

        $company_id = $fn->getReqParam('company_id');

        $formAction = "index.php?module=tradingsg_customer&_spAction=AddProductKeyToCompanySubmit&showHTML=0";
        $expNoEdit = array('isEditable' => 0);

        $sqlLatestKey = "
        SELECT CASE 
               WHEN MAX(lead_history_id)
               THEN MAX(lead_history_id) + 1
               ELSE 1
               END AS lead_history_id
        FROM lead_history
        WHERE (status = 'Active' OR company_id IS NOT NULL)
        ";
        $resultLatestKey  = $db->sql_query($sqlLatestKey);
        $numRowsLatestKey = $db->sql_numrows($resultLatestKey);
        $rowLatestKey     = $db->sql_fetchrow($resultLatestKey);

        if($numRowsLatestKey == 0){
            $rowLatestKey['lead_history_id'] = 1;
        }

        $productKeyRec = $fn->getRecordByCondition('lead_history', "lead_history_id = '{$rowLatestKey['lead_history_id']}'");

        $text = "
        <form id='AddProductKeyLinkForm' class='AddProductKeyLinkForm yform columnar' method='post' action='{$formAction}'>
            {$formObj->getTBRow('Product Key', 'radnom_no', $productKeyRec['radnom_no'], $expNoEdit)}
            {$formObj->getTBRow('No Of Users', 'no_of_users', '')}
            <input type='hidden' name='company_id' value='{$company_id}' />
            <input type='hidden' name='lead_history_id' value='{$rowLatestKey['lead_history_id']}' />
        </form>
        ";

        return $text;
    }
    /**
     *
     */
    function getOldGoldPortal($row){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        $rows  = "";
        
        $SQLOrder = "
        SELECT o.*
              ,c.company_id
        FROM `order` o
        LEFT JOIN (company c) ON (c.company_id = o.company_id)
        WHERE o.company_id = {$row['company_id']}
        AND (o.old_gold_916_amount > 0
        OR o.old_gold_nonkdm_amount > 0)
        ";

        $resultOrder   = $db->sql_query($SQLOrder);
        $recCount = $db->sql_numrows($resultOrder);
        while ($rowOrder = $db->sql_fetchrow($resultOrder)) {

            $order_date = $fn->getCPDate($rowOrder['order_date'], 'd-m-Y');
            $old_gold_gram = $rowOrder['old_gold_916_grams'] + $rowOrder['old_gold_nonkdm_grams'];
            $old_gold_amount = $rowOrder['old_gold_916_amount'] + $rowOrder['old_gold_nonkdm_amount'];
            $old_gold_gram = number_format($old_gold_gram, 2);
            $old_gold_amount = number_format($old_gold_amount, 2);
            
            $OrderEditLink = "index.php?_topRm=order&module=tradingsg_order&_action=edit&order_id={$rowOrder['order_id']}";
            
            $modObj = getCPModuleObj('tradingsg_pos');
            $order_amount = $modObj->view->getTotalAmount($rowOrder['order_id']);
            $order_amount = number_format($order_amount, 2);
            
            if($rowOrder['order_id'] < 10){
                $orderNo = '0000' . $rowOrder['order_id'];
            }
            else if($rowOrder['order_id'] <= 99){
                $orderNo = '000' . $rowOrder['order_id'];
            }
            else if($rowOrder['order_id'] <= 999){
                $orderNo = '00' . $rowOrder['order_id'];
            }
            else if($rowOrder['order_id'] <= 9999){
                $orderNo = '0' . $rowOrder['order_id'];
            }
            else{
                $orderNo = $rowOrder['order_id'];
            }

            $rows .= "
            <tr>
                <td><a href='{$OrderEditLink}' target='_blank'>
                        <u>{$orderNo}</u>
                    </a>
                </td>
                <td>{$order_date}</td>
                <td align='right'>{$order_amount}</td>
                <td align='right'>{$old_gold_amount}($old_gold_gram)</td>
            </tr>
            ";
        }

        $header ="
        <tr>
          <th>Order Code</th>
          <th>Date</th>
          <th class='txtRight'>Order Amount</th>
          <th class='txtRight'>Amount Deducted(gms)</th>
        </tr>
        ";

        if($recCount == 0){
            $header = "<tr><td align='center'>No Records Linked<br/><br/></td></tr>";
        }

        $text = "
        <div class='linkPortalWrapper tradingsg_customer_orderLink'>
          <div class='panel panel-primary'>
            <div class='panel-heading'>
              <div expanded='1'>
                  <div class='floatbox'>
                      <div class='float_left RightPanelHeading'>Purchased using old gold</div>
                      <div class='txtRight'>
                          <span class='count' id='relatedorderPortalCount'>({$recCount})</span>
                          <div class='toggle'></div>
                      </div>
                  </div>
              </div>
            </div>
            <div class='panel-body'>
              <div class='linkPortalDataWrapper'>
                  <form>
                      <table class='relatedorderList'>
                          <thead>
                              {$header}
                          </thead>
                          <tbody id='relatedorderPortal'>
                              {$rows}
                          </tbody>
                      </table>
                  </form>
              </div>
            </div>
          </div>
        </div>
        ";


        return $text;
    }

    /**
     *
     */
    function getChitPaymentPortal($row){
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $formObj = Zend_Registry::get('formObj');
        $dbUtil = Zend_Registry::get('dbUtil');

        $rows  = "";
        

        $SQLOrder = "
        SELECT o.*
              ,c.company_id
        FROM `order` o
        LEFT JOIN (company c) ON (c.company_id = o.company_id)
        WHERE o.company_id = {$row['company_id']}
        AND o.chit_amount > 0
        ";

        $resultOrder   = $db->sql_query($SQLOrder);
        $recCount = $db->sql_numrows($resultOrder);
        while ($rowOrder = $db->sql_fetchrow($resultOrder)) {
            $order_date = $fn->getCPDate($rowOrder['order_date'], 'd-m-Y');
            $OrderEditLink = "index.php?_topRm=order&module=tradingsg_order&_action=edit&order_id={$rowOrder['order_id']}";
            
            $modObj = getCPModuleObj('tradingsg_pos');
            $order_amount = $modObj->view->getTotalAmount($rowOrder['order_id']);
            $order_amount = number_format($order_amount, 2);            

            $chit_amount = number_format($rowOrder['chit_amount'], 2);            
            $chit_no = $rowOrder['chit_no'];

            if($rowOrder['order_id'] < 10){
                $orderNo = '0000' . $rowOrder['order_id'];
            }
            else if($rowOrder['order_id'] <= 99){
                $orderNo = '000' . $rowOrder['order_id'];
            }
            else if($rowOrder['order_id'] <= 999){
                $orderNo = '00' . $rowOrder['order_id'];
            }
            else if($rowOrder['order_id'] <= 9999){
                $orderNo = '0' . $rowOrder['order_id'];
            }
            else{
                $orderNo = $rowOrder['order_id'];
            }

            $rows .= "
            <tr>
                <td><a href='{$OrderEditLink}' target='_blank'>
                        <u>{$orderNo}</u>
                    </a>
                </td>
                <td>{$order_date}</td>
                <td align='right'>{$order_amount}</td>
                <td align='right'>{$chit_amount}($chit_no)</td>
            </tr>
            ";
        }

        $header ="
        <tr>
          <th>Order Code</th>
          <th>Date</th>
          <th class='txtRight'>Order Amount</th>
          <th class='txtRight'>Amount Deducted(Chit No)</th>
        </tr>
        ";

        if($recCount == 0){
            $header = "<tr><td align='center'>No Records Linked<br/><br/></td></tr>";
        }


        $text = "
        <div class='linkPortalWrapper tradingsg_customer_orderLink'>
          <div class='panel panel-primary'>
            <div class='panel-heading'>
              <div expanded='1'>
                  <div class='floatbox'>
                      <div class='float_left RightPanelHeading'>Purchased using chit payment</div>
                      <div class='txtRight'>
                          <span class='count' id='relatedorderPortalCount'>({$recCount})</span>
                          <div class='toggle'></div>
                      </div>
                  </div>
              </div>
            </div>
            <div class='panel-body'>
              <div class='linkPortalDataWrapper'>
                  <form>
                      <table class='relatedorderList'>
                          <thead>
                              {$header}
                          </thead>
                          <tbody id='relatedorderPortal'>
                              {$rows}
                          </tbody>
                      </table>
                  </form>
              </div>
            </div>
          </div>
        </div>
        ";

        return $text;
    }

}