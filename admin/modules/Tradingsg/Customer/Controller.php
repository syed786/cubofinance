<?
class CPL_Admin_Modules_Tradingsg_Customer_Controller extends CP_Common_Lib_ModuleControllerAbstract
{

	function getAddCompany(){
        return $this->model->getAddCompany();
    }

    function getAddCompanyFormSubmit(){
        return $this->model->getAddCompanyFormSubmit();
    }

    function getCompanyLinkDisplay(){
        return $this->view->getCompanyLinkDisplay();
    }

    function getDeleteCompanyLinkRecord(){
        return $this->model->getDeleteCompanyLinkRecord();
    }

    function getProductKeyPortal(){
        return $this->view->getProductKeyPortal();
    }

    function getLinkProductKeyForCompany(){
        return $this->model->getLinkProductKeyForCompany();
    }

    function getAddProductKeyToCompany(){
        return $this->view->getAddProductKeyToCompany();
    }

    function getAddProductKeyToCompanySubmit(){
        return $this->model->getAddProductKeyToCompanySubmit();
    }

    function getAddClientPayment() {
        return $this->view->getAddClientPayment();
    }
    
    function getClientPayment() {
        return $this->view->getClientPayment();
    }

    function getClientPaymentFormSubmit(){
        return $this->model->getClientPaymentFormSubmit();
    }

    function getClientPaymentValidate(){
        return $this->model->getClientPaymentValidate();
    }

    function getEditClientPayment() {
        return $this->view->getEditClientPayment();
    }

    function getEditClientPaymentFormSubmit() {
        return $this->model->getEditClientPaymentFormSubmit();
    }

    function getDeleteClientPayment() {
        return $this->model->getDeleteClientPayment();
    }

    function getAddOldGoldPayment() {
        return $this->view->getAddOldGoldPayment();
    }
    
    function getOldGoldPayment() {
        return $this->view->getOldGoldPayment();
    }

    function getOldGoldPaymentFormSubmit(){
        return $this->model->getOldGoldPaymentFormSubmit();
    }

    function getOldGoldPaymentValidate(){
        return $this->model->getOldGoldPaymentValidate();
    }

    function getEditOldGoldPayment() {
        return $this->view->getEditOldGoldPayment();
    }

    function getEditOldGoldPaymentFormSubmit(){
        return $this->model->getEditOldGoldPaymentFormSubmit();
    }

    function getDeleteOldGoldPayment() {
        return $this->model->getDeleteOldGoldPayment();
    }

    function getAddCustomerFinance() {
        return $this->view->getAddCustomerFinance();
    }
    
    function getCustomerFinance() {
        return $this->view->getCustomerFinance();
    }

    function getCustomerFinanceFormSubmit(){
        return $this->model->getCustomerFinanceFormSubmit();
    }

    function getCustomerFinanceValidate(){
        return $this->model->getCustomerFinanceValidate();
    }

    function getEditCustomerFinance() {
        return $this->view->getEditCustomerFinance();
    }

    function getEditCustomerFinanceFormSubmit() {
        return $this->model->getEditCustomerFinanceFormSubmit();
    }

    function getDeleteCustomerFinance() {
        return $this->model->getDeleteCustomerFinance();
    }
}