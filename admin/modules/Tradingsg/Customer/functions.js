Util.createCPObject('cpm.tradingsg.customer');

cpm.tradingsg.customer = {
    init: function(){

        $('#portalForm select#fld_product_group_id').livequery('change', function(){
           cpm.tradingsg.customer.loadCategoryDropdown.call(this);
        });

        $(".formulaPad input[name='cost_price']").livequery('change', function(){
            var cost_price = $(this).val();
            service_cost = $(".formulaPad input[name='service_cost']").val();
            discount = $(".formulaPad input[name='discount']").val();
            var url = "index.php?module=tradingsg_customer&_spAction=formulaPadCalc&showHTML=0";
            $.get(url, {service_cost: service_cost, discount: discount, cost_price: cost_price}, function(json){
                $('.formulaPad .serviceCostValue').html(json.serviceCostValue);
                $('.formulaPad .sellingPrice').html(json.sellingPrice);
                $('.formulaPad .discountSellingPrice').html(json.discountSellingPrice);
                $('.formulaPad .serviceCostOutput').html(json.serviceCostOutput);
                $('.formulaPad .serviceCostTotal').html(json.serviceCostTotal);
            });
        });

        $(".formulaPad input[name='service_cost']").livequery('change', function(){
            var service_cost = $(this).val();
            cost_price = $(".formulaPad input[name='cost_price']").val();
            discount = $(".formulaPad input[name='discount']").val();
            var url = "index.php?module=tradingsg_customer&_spAction=formulaPadCalc&showHTML=0";
            $.get(url, {service_cost: service_cost, discount: discount, cost_price: cost_price}, function(json){
                $('.formulaPad .serviceCostValue').html(json.serviceCostValue);
                $('.formulaPad .sellingPrice').html(json.sellingPrice);
                $('.formulaPad .discountSellingPrice').html(json.discountSellingPrice);
                $('.formulaPad .serviceCostOutput').html(json.serviceCostOutput);
                $('.formulaPad .serviceCostTotal').html(json.serviceCostTotal);
            });
        });

        $(".formulaPad input[name='discount']").livequery('change', function(){
            var discount = $(this).val();
            cost_price = $(".formulaPad input[name='cost_price']").val();
            service_cost = $(".formulaPad input[name='service_cost']").val();
            var url = "index.php?module=tradingsg_customer&_spAction=formulaPadCalc&showHTML=0";
            $.get(url, {service_cost: service_cost, discount: discount, cost_price: cost_price}, function(json){
                $('.formulaPad .discountValue').html(json.discountValue);
                $('.formulaPad .sellingPrice').html(json.sellingPrice);
                $('.formulaPad .discountSellingPrice').html(json.discountSellingPrice);
                $('.formulaPad .serviceCostOutput').html(json.serviceCostOutput);
                $('.formulaPad .serviceCostTotal').html(json.serviceCostTotal);
            });
        });

        $('#addCompanyLink').livequery('click', function (e){
            var title = "New CompanyLink";
            var company_id = $(this).attr('company_id');
            e.preventDefault();

            var expObj = {
                validate: true
                ,callbackOnSuccess: function(){
                Util.closeAllDialogs();
                reloadCompanyList.reloadCompanyListobj(company_id);
                }
            }
            Util.openFormInDialog.call(this, 'portalFormCompanyLink', title, 589, 253, expObj);
        });

        $('.deleteCompanyLinkRecord').livequery('click', function (){
            var company_id       = $(this).attr('company_id');
            var company_group_id = $(this).attr('company_group_id');
            var other_company_id = $(this).attr('other_company_id');
            var url ='index.php?module=tradingsg_customer&_spAction=deleteCompanyLinkRecord&showHTML=0'
              $.get(url, {company_id:company_id, company_group_id:company_group_id, other_company_id:other_company_id}, function(html){
                reloadCompanyList.reloadCompanyListobj(company_id);
              });
        });

        /* Add Product Key*/
        $('.AddProductKeyLinkButton').live('click', function (e){
                var title = "Add Product Key";
                var company_id = $(this).attr('company_id');
                e.preventDefault();
                var expObj = {
                    validate: true
                   ,callbackOnSuccess: function(){
                        var msg = 'Key Added Successfully';
                        Util.alert(msg, function(){
                            Util.closeAllDialogs();
                            reloadCompanyList.reloadProductKeyLink(company_id);
                        });
                    }
                }
            Util.openFormInDialog.call(this, 'AddProductKeyLinkForm', title, 550, 252, expObj);
        });

        /*$('.AddProductKeyLinkButton').livequery('click', function (e){
            var msg = "Are you sure to link key?";
            var company_id = $("#record_id").val();
            e.preventDefault();
            if (confirm(msg)){
                Util.showProgressInd();
                var url = "index.php?module=tradingsg_customer&_spAction=linkProductKeyForCompany&company_id=" + company_id + "&showHTML=0";
                $.get(url, function(){
                    reloadCompanyList.reloadProductKeyLink(company_id);
                });
            }
        });*/

        /* Add Client Payment */
        $('#AddClientPayment').live('click', function (e){
                var title = "Add Payment";
                e.preventDefault();
                var company_id = $(this).attr('company_id');

                var expObj = {
                    validate: true
                   ,callbackOnSuccess: function(){
                        var msg = 'Client Payment Added Successfully';
                        Util.alert(msg, function(){
                            Util.closeAllDialogs();
                            cpm.tradingsg.customer.reloadclientpayment(company_id);
                            cpm.tradingsg.customer.reloadcustomerfinance(company_id);
                            //window.location.reload(true);
                        });
                    }
                }
                Util.openFormInDialog.call(this, 'clientPaymentPortalForm', title, 600, 400, expObj);
        });

        /* Edit Client Payment */
        $('.EditClientPayment').live('click', function (e){
            var title = "Edit Payment";
            var company_id = $(this).attr('company_id');
            e.preventDefault();

            var expObj = {
                validate: true
               ,callbackOnSuccess: function(){
                    Util.closeAllDialogs();
                    Util.alert('Payment Updated Successfully');
                    cpm.tradingsg.customer.reloadclientpayment(company_id);
                    cpm.tradingsg.customer.reloadcustomerfinance(company_id);
                }
            }
            Util.openFormInDialog.call(this, 'portalForm', title, 600, 500, expObj);
        });

        /* Delete Client Payment */
        $('.deleteClientPayment').live('click', function (e){
            msg = "Do you like to delete the Client Payment?";
            if (!confirm(msg)){
                return false;
            }
            else{
                Util.showProgressInd();
                var customer_payment_id = $(this).attr('customer_payment_id');
                var company_id = $(this).attr('company_id');

                var url = 'index.php?module=tradingsg_customer&_spAction=DeleteClientPayment&showHTML=0&customer_payment_id=' + customer_payment_id;
                $.get(url, {customer_payment_id: customer_payment_id,company_id: company_id}, function(html){
                    Util.hideProgressInd();
                    cpm.tradingsg.customer.reloadclientpayment(company_id);
                    cpm.tradingsg.customer.reloadcustomerfinance(company_id);
                });
            }
        });

        /* Add Customer Finance */
        $('#AddCustomerFinance').live('click', function (e){
                var title = "Add Finance";
                e.preventDefault();
                var company_id = $(this).attr('company_id');

                var expObj = {
                    validate: true
                   ,callbackOnSuccess: function(){
                        var msg = 'Customer Finance Added Successfully';
                        Util.alert(msg, function(){
                            Util.closeAllDialogs();
                            cpm.tradingsg.customer.reloadcustomerfinance(company_id);
                            //window.location.reload(true);
                        });
                    }
                }
                Util.openFormInDialog.call(this, 'customerFinancePortalForm', title, 600, 400, expObj);
        });

        /* Edit Customer Finance */
        $('.EditCustomerFinance').live('click', function (e){
            var title = "Edit Payment";
            var company_id = $(this).attr('company_id');
            e.preventDefault();

            var expObj = {
                validate: true
               ,callbackOnSuccess: function(){
                    Util.closeAllDialogs();
                    Util.alert('Finance Updated Successfully');
                    cpm.tradingsg.customer.reloadcustomerfinance(company_id);
                }
            }
            Util.openFormInDialog.call(this, 'portalForm', title, 600, 500, expObj);
        });

        /* Delete Customer Finance */
        $('.deleteCustomerFinance').live('click', function (e){
            msg = "Do you like to delete the customer finance?";
            if (!confirm(msg)){
                return false;
            }
            else{
                Util.showProgressInd();
                var customer_finance_id = $(this).attr('customer_finance_id');
                var company_id = $(this).attr('company_id');

                var url = 'index.php?module=tradingsg_customer&_spAction=DeleteCustomerFinance&showHTML=0&customer_finance_id=' + customer_finance_id;
                $.get(url, {customer_finance_id: customer_finance_id}, function(html){
                    Util.hideProgressInd();
                    cpm.tradingsg.customer.reloadcustomerfinance(company_id);
                });
            }
        });

        /* Add OldGold Payment */
        $('#AddOldGoldPayment').live('click', function (e){
                var title = "Add OldGold Payment";
                e.preventDefault();
                var company_id = $(this).attr('company_id');

                var expObj = {
                    validate: true
                   ,callbackOnSuccess: function(){
                        var msg = 'OldGold Payment Added Successfully';
                        Util.alert(msg, function(){
                            Util.closeAllDialogs();
                            cpm.tradingsg.customer.reloadoldGoldpayment(company_id);
                        });
                    }
                }
                Util.openFormInDialog.call(this, 'oldGoldPaymentPortalForm', title, 600, 400, expObj);
        });

        /* Edit OldGold Payment */
        $('.EditOldGoldPayment').live('click', function (e){
            var title = "Edit OldGold Payment";
            var company_id = $(this).attr('company_id');
            e.preventDefault();

            var expObj = {
                validate: true
               ,callbackOnSuccess: function(){
                    Util.closeAllDialogs();
                    Util.alert('OldGold Payment Updated Successfully');
                    cpm.tradingsg.customer.reloadoldGoldpayment(company_id);
                }
            }
            Util.openFormInDialog.call(this, 'portalForm', title, 600, 500, expObj);
        });

        /* Delete OldGold Payment */
        $('.deleteOldGoldPayment').live('click', function (e){
            msg = "Do you like to delete the OldGold Payment?";
            if (!confirm(msg)){
                return false;
            }
            else{
                Util.showProgressInd();
                var old_gold_payments_id = $(this).attr('old_gold_payments_id');
                var company_id = $(this).attr('company_id');

                var url = 'index.php?module=tradingsg_customer&_spAction=DeleteOldGoldPayment&showHTML=0&old_gold_payments_id=' + old_gold_payments_id;
                $.get(url, {old_gold_payments_id: old_gold_payments_id}, function(html){
                    Util.hideProgressInd();
                    cpm.tradingsg.customer.reloadoldGoldpayment(company_id);
                });
            }
        });


    },

    reloadclientpayment: function(company_id){
        var url = 'index.php?module=tradingsg_customer&_spAction=AddClientPayment&showHTML=0';
        $.get(url,{company_id:company_id}, function(html){
            $('#ClientPaymentLinkPortal').html(html);
            //Util.hideProgressInd();
        });
    },

    reloadcustomerfinance: function(company_id){
        var url = 'index.php?module=tradingsg_customer&_spAction=AddCustomerFinance&showHTML=0';
        $.get(url,{company_id:company_id}, function(html){
            $('#CustomerFinanceLinkPortal').html(html);
            //Util.hideProgressInd();
        });
    },

    reloadoldGoldpayment: function(company_id){
        var url = 'index.php?module=tradingsg_customer&_spAction=AddOldGoldPayment&showHTML=0';
        $.get(url,{company_id:company_id}, function(html){
            $('#OldGoldPaymentLinkPortal').html(html);
        });
    },

    loadCategoryDropdown: function(){
        $(this).each(function(){
            ProductGroupId = $(this).val();
            var url = $('#scopeRootAlias').val() + 'index.php?module=tradingsg_product&_spAction=categoryJsonByProductGroupId&showHTML=0'

            $.getJSON(url, {product_group_id: ProductGroupId}, function(data) {
                $('#portalForm select#fld_category_id').cp_loadSelect(data);
            });
        });
    }
}

var reloadCompanyList ={
    reloadCompanyListobj: function(company_id){
             var url = 'index.php?module=tradingsg_customer&_spAction=companyLinkDisplay&showHTML=0';
            $.get(url, {company_id: company_id}, function(html){
                $('#companyLinkPortal').html(html);
                Util.hideProgressInd();
             });
    },

    reloadProductKeyLink: function(company_id){
        var url = 'index.php?module=tradingsg_customer&_spAction=ProductKeyPortal&showHTML=0';
        $.get(url, {company_id: company_id}, function(html){
            Util.hideProgressInd();
            $('#productKeyLinkPortal').html(html);
        });
    },
}