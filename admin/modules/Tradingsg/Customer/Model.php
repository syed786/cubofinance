<?
class CPL_Admin_Modules_Tradingsg_Customer_Model extends CP_Common_Lib_ModuleModelAbstract
{
    /**
     *
     */
    function getSQL() {

        $SQL = "
        SELECT c.*
              ,gc.name AS country_name
        FROM company c
        LEFT JOIN (geo_country gc) ON (c.address_country = gc.country_code)
        ";

        return $SQL;
    }

    /**
     *
     */
    function setSearchVar($linkRecType = '') {
        $tv = Zend_Registry::get('tv');
        $fn = Zend_Registry::get('fn');
        $searchVar = Zend_Registry::get('searchVar');
        $searchVar->mainTableAlias = 'c';

        $status       = $fn->getReqParam('status');
        $company_id   = $fn->getReqParam('company_id');
        $company_name = $fn->getReqParam('company_name');

        if ($company_id != "") {
            $searchVar->sqlSearchVar[] = "c.company_id = '{$company_id}'";
        } else if ($tv['record_id'] != '') {
            $searchVar->sqlSearchVar[] = "c.company_id = '{$tv['record_id']}'";
        } else {
            $fn->setSearchVarForLinkData($searchVar, $linkRecType, 'c.company_id');

            if ($status != "") {
                $searchVar->sqlSearchVar[] = "c.status = '{$status}'";
            }

            if ($company_name != "") {
                $searchVar->sqlSearchVar[] = "c.company_name LIKE '%{$company_name}%'";
            }

            if ($tv['keyword'] != "") {
                $searchVar->sqlSearchVar[] = "(
                    c.company_name  LIKE '%{$tv['keyword']}%'
                )";
            }

            //------------------------------------------------------------------------//
            if ($tv['special_search'] == "Flagged") {
                $searchVar->sqlSearchVar[] = "c.flag = 1";
            }

            if ($tv['special_search'] == "Not-Flagged") {
                $searchVar->sqlSearchVar[] = "(c.flag != 1 OR c.flag IS null)";
            }

            $searchVar->sortOrder = "c.company_name";
        }
    }

    /**
     *
     */
    function getNewValidate() {
        $validate = Zend_Registry::get('validate');

        $validate->resetErrorArray();
        $validate->validateData('company_name', 'Please enter the company name');

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    function getAdd(){
        $fn = Zend_Registry::get('fn');
        $validate = Zend_Registry::get('validate');

        if (!$this->getNewValidate()){
            return $validate->getErrorMessageXML();
        }

        $fa = $this->getFields();
        
        $id = $fn->addRecord($fa);
        $fn->returnAfterNewSave($id);
    }

    /**
     *
     */
    function getEditValidate() {
        $validate = Zend_Registry::get('validate');

        $validate->resetErrorArray();
        //$validate->validateData('category', 'Please enter Category');

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    function getSave(){
        $fn = Zend_Registry::get('fn');
        $validate = Zend_Registry::get('validate');
        $cpCfg = Zend_Registry::get('cpCfg');

        if (!$this->getEditValidate()){
            return $validate->getErrorMessageXML();
        }

        $fa = $this->getFields();

        $id = $fn->saveRecord($fa);
        $fn->returnAfterNewSave($id, $cpCfg['cp.pagetoReturnAfterSave']);
    }

    /**
     *
     */
    function getSaveList(){
        $fn = Zend_Registry::get('fn');
        $fn->getSaveList();
    }

    /**
     *
     */
    function getFields(){
        $fn = Zend_Registry::get('fn');

        $fa = array();
        $fa = $fn->addToFieldsArray($fa, 'company_name');
        $fa = $fn->addToFieldsArray($fa, 'code');
        $fa = $fn->addToFieldsArray($fa, 'website');
        $fa = $fn->addToFieldsArray($fa, 'company_size');
        $fa = $fn->addToFieldsArray($fa, 'industry');
        $fa = $fn->addToFieldsArray($fa, 'source');
        $fa = $fn->addToFieldsArray($fa, 'address_flat');
        $fa = $fn->addToFieldsArray($fa, 'address_street');
        $fa = $fn->addToFieldsArray($fa, 'address_town');
        $fa = $fn->addToFieldsArray($fa, 'address_state');
        $fa = $fn->addToFieldsArray($fa, 'address_country');
        $fa = $fn->addToFieldsArray($fa, 'address_po_code');
        $fa = $fn->addToFieldsArray($fa, 'billing_address_flat');
        $fa = $fn->addToFieldsArray($fa, 'billing_address_street');
        $fa = $fn->addToFieldsArray($fa, 'billing_address_town');
        $fa = $fn->addToFieldsArray($fa, 'billing_address_state');
        $fa = $fn->addToFieldsArray($fa, 'billing_address_country');
        $fa = $fn->addToFieldsArray($fa, 'mobile');
        $fa = $fn->addToFieldsArray($fa, 'fax');
        $fa = $fn->addToFieldsArray($fa, 'group_name');
        $fa = $fn->addToFieldsArray($fa, 'status');
        $fa = $fn->addToFieldsArray($fa, 'category');
        $fa = $fn->addToFieldsArray($fa, 'source');
        $fa = $fn->addToFieldsArray($fa, 'industry');
        $fa = $fn->addToFieldsArray($fa, 'company_size');
        $fa = $fn->addToFieldsArray($fa, 'supplier_type');
        $fa = $fn->addToFieldsArray($fa, 'customer_type');
        $fa = $fn->addToFieldsArray($fa, 'mark_up_percentage');
        $fa = $fn->addToFieldsArray($fa, 'cst_no');
        $fa = $fn->addToFieldsArray($fa, 'tin_no');
        $fa = $fn->addToFieldsArray($fa, 'gst_no');
        $fa = $fn->addToFieldsArray($fa, 'chit_no');

        return $fa;
    }
    /**
     *
     */
    function getClientPaymentFormSubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getClientPaymentValidate()){
            return $validate->getErrorMessageXML();
        }

        $company_id  = $fn->getPostParam('company_id');
        $date        = $fn->getPostParam('date');
        $amount      = $fn->getPostParam('amount');
        $description = $fn->getPostParam('description');
        $status       = $fn->getPostParam('status');

        $fa = array();
        $fa['date']          = $date;
        $fa['amount']        = $amount;
        $fa['description']   = $description;
        $fa['company_id']    = $company_id;
        $fa['creation_date'] = date("Y-m-d H:i:s");
        $fa['created_by']    = $fn->getSessionParam('userName');
        $fa['status']       = $status;

        $insertPaymentSQL = $dbUtil->getInsertSQLStringFromArray($fa, 'customer_payment');
        $resultPaymentSQL = $db->sql_query($insertPaymentSQL);

        /*$SQL = "
        SELECT SUM(amount) as total_amount
        FROM customer_payment
        WHERE company_id = {$company_id}
        ";
        $result = $db->sql_query($SQL);
        $row = $db->sql_fetchrow($result);

        $SQLCF = "
        SELECT SUM(amount) as outstanding_amount
        FROM customer_finance
        WHERE company_id = {$company_id}
        ";
        $resultCF = $db->sql_query($SQLCF);
        $rowCF = $db->sql_fetchrow($resultCF);

        if($rowCF['outstanding_amount'] <= $row['total_amount']){
            $financeStatus     = 'Paid';            
        } else if ($rowCF['outstanding_amount'] > $row['total_amount']){
            $financeStatus     = 'Partially Paid';            
        }

        $SQLUPDATE = "
        UPDATE `customer_finance`
        SET status = '{$financeStatus}'
        WHERE company_id = {$company_id}
        ";
        $resultUPDATE = $db->sql_query($SQLUPDATE);*/

        return $validate->getSuccessMessageXML();
    }
    /**
     *
     */
    function getClientPaymentValidate() {
        $validate = Zend_Registry::get('validate');

        $validate->resetErrorArray();
        $validate->validateData('amount', 'Please Enter Amount');
        $validate->validateData('date', 'Please Select Date');

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     *
     */
    function getEditClientPaymentFormSubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getClientPaymentValidate()){
            return $validate->getErrorMessageXML();
        }

        $company_id        = $fn->getPostParam('company_id');
        $date              = $fn->getPostParam('date');
        $amount            = $fn->getPostParam('amount');
        $description       = $fn->getPostParam('description');
        $customer_payment_id = $fn->getPostParam('customer_payment_id');
        $status       = $fn->getPostParam('status');

        $fa1 = array();
        $fa1['date']              = $date;
        $fa1['amount']            = $amount;
        $fa1['description']       = $description;
        $fa1['status']       = $status;
        $fa1['modification_date'] = date("Y-m-d H:i:s");
        $fa1['modified_by']       = $fn->getSessionParam('userName');

        $whereConditionPayment = "WHERE customer_payment_id = {$customer_payment_id}" ;
        $sqlUpdatePayment      = $dbUtil->getUpdateSQLStringFromArray($fa1, "customer_payment", $whereConditionPayment);
        $resultUpdatePayment   = $db->sql_query($sqlUpdatePayment);

        /*$SQL = "
        SELECT SUM(amount) as total_amount
        FROM customer_payment
        WHERE company_id = {$company_id}
        ";
        $result = $db->sql_query($SQL);
        $row = $db->sql_fetchrow($result);

        $SQLCF = "
        SELECT SUM(amount) as outstanding_amount
        FROM customer_finance
        WHERE company_id = {$company_id}
        ";
        $resultCF = $db->sql_query($SQLCF);
        $rowCF = $db->sql_fetchrow($resultCF);

        if($rowCF['outstanding_amount'] <= $row['total_amount']){
            $financeStatus     = 'Paid';            
        } else if ($rowCF['outstanding_amount'] > $row['total_amount']){
            $financeStatus     = 'Partially Paid';            
        }

        $SQLUPDATE = "
        UPDATE `customer_finance`
        SET status = '{$financeStatus}'
        WHERE company_id = {$company_id}
        ";
        $resultUPDATE = $db->sql_query($SQLUPDATE);*/

        return $validate->getSuccessMessageXML();
    }
    /**
     *
     */
    function getDeleteClientPayment(){
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpCfg = Zend_Registry::get('cpCfg');
        $cpUtil = Zend_Registry::get('cpUtil');

        $company_id = $fn->getReqParam('company_id');
        $customer_payment_id = $fn->getReqParam('customer_payment_id');

        $SQL ="
               DELETE FROM customer_payment
               WHERE customer_payment_id = {$customer_payment_id}
               ";
        $result = $db->sql_query($SQL);
    
        /*$SQL1 = "
        SELECT SUM(amount) as total_amount
        FROM customer_payment
        WHERE company_id = {$company_id}
        ";
        $result1 = $db->sql_query($SQL1);
        $row = $db->sql_fetchrow($result1);

        $SQLCF = "
        SELECT SUM(amount) as outstanding_amount
        FROM customer_finance
        WHERE company_id = {$company_id}
        ";
        $resultCF = $db->sql_query($SQLCF);
        $rowCF = $db->sql_fetchrow($resultCF);

        if($rowCF['outstanding_amount'] <= $row['total_amount']){
            $financeStatus     = 'Paid';            
        } else if ($row['total_amount'] == 0){
            $financeStatus     = 'Due';            
        } else if ($rowCF['outstanding_amount'] > $row['total_amount']){
            $financeStatus     = 'Partially Paid';            
        }

        $SQLUPDATE = "
        UPDATE `customer_finance`
        SET status = '{$financeStatus}'
        WHERE company_id = {$company_id}
        ";
        $resultUPDATE = $db->sql_query($SQLUPDATE);*/
    }

    /**
     *
     */
    function getCustomerFinanceFormSubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getCustomerFinanceValidate()){
            return $validate->getErrorMessageXML();
        }

        $company_id  = $fn->getPostParam('company_id');
        $date        = $fn->getPostParam('date');
        $amount      = $fn->getPostParam('amount');
        $description = $fn->getPostParam('description');
        $status = $fn->getPostParam('status');

        $fa = array();
        $fa['date']          = $date;
        $fa['amount']        = $amount;
        $fa['description']   = $description;
        $fa['status']   = $status;
        $fa['company_id']    = $company_id;
        $fa['creation_date'] = date("Y-m-d H:i:s");
        $fa['created_by']    = $fn->getSessionParam('userName');

        $insertPaymentSQL = $dbUtil->getInsertSQLStringFromArray($fa, 'customer_finance');
        $resultPaymentSQL = $db->sql_query($insertPaymentSQL);

        return $validate->getSuccessMessageXML();
    }
    /**
     *
     */
    function getCustomerFinanceValidate() {
        $validate = Zend_Registry::get('validate');

        $validate->resetErrorArray();
        $validate->validateData('amount', 'Please Enter Amount');
        $validate->validateData('date', 'Please Select Date');

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     *
     */
    function getEditCustomerFinanceFormSubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getCustomerFinanceValidate()){
            return $validate->getErrorMessageXML();
        }

        $company_id        = $fn->getPostParam('company_id');
        $date              = $fn->getPostParam('date');
        $amount            = $fn->getPostParam('amount');
        $description       = $fn->getPostParam('description');
        $customer_finance_id = $fn->getPostParam('customer_finance_id');
        $status = $fn->getPostParam('status');

        $fa1 = array();
        $fa1['date']              = $date;
        $fa1['amount']            = $amount;
        $fa1['description']       = $description;
        $fa1['modification_date'] = date("Y-m-d H:i:s");
        $fa1['modified_by']       = $fn->getSessionParam('userName');
        $fa1['status']   = $status;

        $whereConditionPayment = "WHERE customer_finance_id = {$customer_finance_id}" ;
        $sqlUpdatePayment      = $dbUtil->getUpdateSQLStringFromArray($fa1, "customer_finance", $whereConditionPayment);
        $resultUpdatePayment   = $db->sql_query($sqlUpdatePayment);

        return $validate->getSuccessMessageXML();
    }
    /**
     *
     */
    function getDeleteCustomerFinance(){
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpCfg = Zend_Registry::get('cpCfg');
        $cpUtil = Zend_Registry::get('cpUtil');

        $company_id = $fn->getReqParam('company_id');
        $customer_finance_id = $fn->getReqParam('customer_finance_id');

        $SQL ="
               DELETE FROM customer_finance
               WHERE customer_finance_id = {$customer_finance_id}
               ";
        $result = $db->sql_query($SQL);
    }

    /**
     *
     */
    function getOldGoldPaymentFormSubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getOldGoldPaymentValidate()){
            return $validate->getErrorMessageXML();
        }

        $company_id    = $fn->getPostParam('company_id');
        $date          = $fn->getPostParam('date');
        $amount        = $fn->getPostParam('amount');
        $description   = $fn->getPostParam('description');
        $gms           = $fn->getPostParam('gms');
        $rate          = $fn->getPostParam('rate');
        $category      = $fn->getPostParam('category');

        $fa = array();

        $fa['date']            = $date;
        $fa['amount']          = $amount;
        $fa['description']     = $description;
        $fa['gms']             = $gms;
        $fa['rate']            = $rate;
        $fa['category']        = $category;
        $fa['company_id']      = $company_id;
        $fa['creation_date']   = date("Y-m-d H:i:s");
        $fa['created_by']      = $fn->getSessionParam('userName');

        $insertOldGoldSQL = $dbUtil->getInsertSQLStringFromArray($fa, 'old_gold_payments');
        $resultOldGoldSQL = $db->sql_query($insertOldGoldSQL);

        return $validate->getSuccessMessageXML();
    }
    /**
     *
     */
    function getOldGoldPaymentValidate() {
        $validate = Zend_Registry::get('validate');

        $validate->resetErrorArray();
        $validate->validateData('amount', 'Please Enter Amount');
        $validate->validateData('date', 'Please Select Date');

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }
    /**
     *
     */
    function getEditOldGoldPaymentFormSubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getOldGoldPaymentValidate()){
            return $validate->getErrorMessageXML();
        }

        $company_id            = $fn->getPostParam('company_id');
        $date                  = $fn->getPostParam('date');
        $amount                = $fn->getPostParam('amount');
        $description           = $fn->getPostParam('description');
        $gms                   = $fn->getPostParam('gms');
        $rate                  = $fn->getPostParam('rate');
        $category              = $fn->getPostParam('category');
        $old_gold_payments_id  = $fn->getPostParam('old_gold_payments_id');

        $fa1 = array();

        $fa1['date']                 = $date;
        $fa1['amount']               = $amount;
        $fa1['description']          = $description;
        $fa1['gms']                  = $gms;
        $fa1['rate']                 = $rate;
        $fa1['category']             = $category;
        $fa1['old_gold_payments_id'] = $old_gold_payments_id;
        $fa1['modification_date']    = date("Y-m-d H:i:s");
        $fa1['modified_by']          = $fn->getSessionParam('userName');

        $whereConditionOldGold = "WHERE old_gold_payments_id = {$old_gold_payments_id}" ;
        $sqlUpdateOldGold      = $dbUtil->getUpdateSQLStringFromArray($fa1, "old_gold_payments", $whereConditionOldGold);
        $resultUpdateOldGold   = $db->sql_query($sqlUpdateOldGold);

        return $validate->getSuccessMessageXML();
    }
    /**
     *
     */
    function getDeleteOldGoldPayment(){
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpCfg = Zend_Registry::get('cpCfg');
        $cpUtil = Zend_Registry::get('cpUtil');

        $company_id = $fn->getReqParam('company_id');
        $old_gold_payments_id = $fn->getReqParam('old_gold_payments_id');

        $SQL ="
               DELETE FROM old_gold_payments
               WHERE old_gold_payments_id = {$old_gold_payments_id}
               ";
        $result = $db->sql_query($SQL);
    }

    /**
     *
     */

    function getExportData($dataArray){
        $phpExcel = includeCPClass('Lib', 'PhpExcelExportWrapper', 'PhpExcelExportWrapper');

        $fa = array(
              'company_id'      => $phpExcel->getFldObj('Company ID')
             ,'company_name'    => $phpExcel->getFldObj('Company Name')
             ,'category'        => $phpExcel->getFldObj('Category')
             ,'company_size'    => $phpExcel->getFldObj('Company Size')
             ,'industry'        => $phpExcel->getFldObj('Industry')
             ,'source'          => $phpExcel->getFldObj('Source')
             ,'website'         => $phpExcel->getFldObj('Website')
             ,'phone'           => $phpExcel->getFldObj('Phone')
             ,'fax'             => $phpExcel->getFldObj('Fax')

             ,'address_flat'    => $phpExcel->getFldObj('Address Flat')
             ,'address_street'  => $phpExcel->getFldObj('Address Street')
             ,'address_town'    => $phpExcel->getFldObj('Address Town')
             ,'address_state'   => $phpExcel->getFldObj('Address State')
             ,'address_country' => $phpExcel->getFldObj('Address Country')

             ,'status'          => $phpExcel->getFldObj('Status')
             ,'comment_by'      => $phpExcel->getFldObj('Comment By')
             ,'notes'           => $phpExcel->getFldObj('Notes')
        );

        $config = array(
             'fldsArr'   => $fa
            ,'dataArray' => $dataArray
        );

        return $phpExcel->exportData($config);
    }

    /**
     *
     */
    function getImportData(){
        $phpExcel = includeCPClass('Lib', 'PhpExcelImportWrapper');

        $fa = array(
              'chit_no'       => $phpExcel->getImportFldObj('Chit No')
             ,'start_month'   => $phpExcel->getImportFldObj('Month')
             ,'start_year'    => $phpExcel->getImportFldObj('Year')
             ,'company_name'  => $phpExcel->getImportFldObj('Name')
             ,'address_flat'  => $phpExcel->getImportFldObj('Address')
             ,'mobile'        => $phpExcel->getImportFldObj('Contact No')
             ,'amount'        => $phpExcel->getImportFldObj('Amount')
             ,'chit_category' => $phpExcel->getImportFldObj('Chit Category')
        );

        $fa['chit_no']['refOnly']       = true;
        $fa['start_month']['refOnly']   = true;
        $fa['start_year']['refOnly']    = true;
        $fa['amount']['refOnly']        = true;
        $fa['chit_category']['refOnly'] = true;

        /****************************************/
        $config = array(
             'module'              => 'tradingsg_company'
            ,'matchFieldArr'       => array('chit_no')
            ,'fldsArr'             => $fa
            ,'callbackAfterInsert' => 'callbackAfterImportInsert'
        );

        return $phpExcel->importData($config);
    }

    /**
     *
     */
    function callbackAfterImportInsert($company_id, $fa) {
        $cpCfg = Zend_Registry::get('cpCfg');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        $chit_no       = $fa['chit_no'];
        $start_month   = $fa['start_month'];
        $start_year    = $fa['start_year'];
        $amount        = $fa['amount'];
        $chit_category = $fa['chit_category'];

        if($amount == ""){
            $amount = 0;
        }

        $month          = date_parse($start_month);
        $monthConverted = $month['month'];

        $faCp = array();
        $faCp['chit_no']    = $chit_no;
        $faCp['date']       = $start_year.'-'.$monthConverted.'-01';
        $faCp['company_id'] = $company_id;
        $faCp['amount']     = $amount;
        $faCp['chit_type']  = $chit_category;
        $faCp = $fn->addCreationDetailsToFieldsArray($faCp, 'customer_payment');

        $SQLClientPayment    = $dbUtil->getInsertSQLStringFromArray($faCp, 'customer_payment');
        $resultClientPayment = $db->sql_query($SQLClientPayment);
    }
    /**
     *
     */
    function getTradingsgCustomerTradingsgContactLinkSQL($id) {

        return "
        SELECT a.contact_id
              ,a.first_name
              ,a.email
              ,a.phone_direct
              ,a.mobile
              ,a.position
              ,a.department
        FROM company b, contact a
        WHERE a.company_id = b.company_id
          AND b.company_id = {$id}
        ";
    }
    /**
     *
     */
    function getTradingsgCompanyTradingsgDiscountLinkSQL($id) {

        return "
        SELECT d.discount_id
              ,pg.title
              ,c.title AS category_title
              ,d.margin
              ,d.discount_percent
        FROM discount d
        LEFT JOIN (product_group pg) ON (d.product_group_id = pg.product_group_id)
        LEFT JOIN (category c) ON (d.category_id = c.category_id)
        WHERE d.company_id = {$id}
        ORDER BY pg.sort_order
        ";
    }

    /**
     *
     */
    function getTradingsgCompanyTradingsgCompanyGroupLinkSQL1($id) {

        return "
        SELECT a.company_id
              ,a.company_name
              ,a.status
        FROM company_group b, company a
        WHERE a.company_id = b.company_id
          AND b.company_id = {$id}
        ";
    }

    /**
     *
     */
    function getAddProductKeyToCompanyValidate() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $db = Zend_Registry::get('db');

        $validate->resetErrorArray();
        $validate->validateData('no_of_users', 'Please enter the no of users');

        if (count($validate->errorArray) == 0) {
            return true;
        } else {
            return false;
        }
    }


    /**
     *
     */
    function getAddProductKeyToCompanySubmit() {
        $validate = Zend_Registry::get('validate');
        $fn = Zend_Registry::get('fn');
        $dbUtil = Zend_Registry::get('dbUtil');
        $db = Zend_Registry::get('db');

        if (!$this->getAddProductKeyToCompanyValidate()){
            return $validate->getErrorMessageXML();
        }

        $company_id       = $fn->getReqParam('company_id');
        $lead_history_id  = $fn->getReqParam('lead_history_id');
        $no_of_users      = $fn->getPostParam('no_of_users');

        $fa = array();
        $fa['company_id']  = $company_id;
        $fa['no_of_users'] = $no_of_users;
        $fa['linked_date'] = date("Y-m-d H:i:s");
        $fa['linked_by']   = $fn->getSessionParam('userName');

        $whereCondition = "WHERE lead_history_id = {$lead_history_id}";
        $SQL    = $dbUtil->getUpdateSQLStringFromArray($fa, "lead_history", $whereCondition);
        $result = $db->sql_query($SQL);

        return $validate->getSuccessMessageXML();
    }

  }
