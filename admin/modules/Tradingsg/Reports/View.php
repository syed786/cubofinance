<?
class CPL_Admin_Modules_Tradingsg_Reports_View extends CP_Admin_Modules_Tradingsg_Reports_View
{

    var $jssKeys = array('jqForm-3.15');

    /**
     *
     */
    function getList() {
        $listObj = Zend_Registry::get('listObj');
        $cpUtil  = Zend_Registry::get('cpUtil');

        $rowCounter = 0;
        $rows = "";

        $text = "
        <div class='floatbox'>
            <div class='float_left'>
                <a href='#' class='cpBack'>back</a>
            </div>
            <div class='float_right'>
                {$this->getReportsDropdown()}
            </div>
        </div>
        <div id='reportSearchPanel' class='ui-corner-all'>
        </div>
        <div id='reportContainer' class='ui-corner-all'>
        </div>
        <script type='text/javascript' src='https://www.google.com/jsapi'></script>
        <script type='text/javascript'>
            google.load('visualization', '1.0', {'packages':['corechart']});
        </script>
		";

        return $text;
    }

    /**
     *
     */
    function getReportsDropdown() {
        $listObj = Zend_Registry::get('listObj');
        $cpUtil  = Zend_Registry::get('cpUtil');
        $cpCfg  = Zend_Registry::get('cpCfg');

        $rowCounter = 0;

        $repArrSrc = $this->model->reportsArray;

        $repArr = array();
        foreach($repArrSrc AS $key => $val){
            $repArr[$key] = $val['title'];
        }

        $text = "
        <table class='search'>
            <tr>
                <td>
                    <select name='report' class='report'>
                        <option value=''>Please Choose the Report</option>
                        <optgroup label='Finance Reports'>
                            <option value='dailyCollectionReport'>Daily Collection Report</option>
                            <option value='investorReport'>Investor Report</option>
                            <option value='topCustomerOutstanding'>Customer Outstanding</option>
                            <option value='financeReport'>Finance Report</option>
                        </optgroup>
                    </select>
                </td>
            </tr>
        </table>
        ";

        return $text;
    }

    /**
     *
     */
    function getSearch() {
        $formObj = Zend_Registry::get('formObj');
        $fn = Zend_Registry::get('fn');
        $tv = Zend_Registry::get('tv');
        $dbUtil = Zend_Registry::get('dbUtil');
        $cpUtil = Zend_Registry::get('cpUtil');
        $pager = Zend_Registry::get('pager');
        $db = Zend_Registry::get('db');
        $cpCfg = Zend_Registry::get('cpCfg');
        $reportsArray = $this->model->reportsArray;

        $report = $fn->getReqParam('report');
        $url = "";

        $year         	  = $fn->getReqParam('year');
        $month        	  = $fn->getReqParam('month');
        $sort_order   	  = $fn->getReqParam('sort_order');
        $search_by    	  = $fn->getReqParam('search_by');
        $active_start 	  = $fn->getReqParam('active_start');
        $active_end   	  = $fn->getReqParam('active_end');
        $course_id    	  = $fn->getReqParam('course_id');
        $subject_id   	  = $fn->getReqParam('subject_id');
        $batch_id     	  = $fn->getReqParam('batch_id');
        $status       	  = $fn->getReqParam('status');
        $staff_id     	  = $fn->getReqParam('staff_id');
        $teacher_id   	  = $fn->getReqParam('teacher_id');
        $product_id   	  = $fn->getReqParam('product_id');
        $company_id   	  = $fn->getReqParam('company_id');
        $sortOrder        = $fn->getReqParam('sortOrder');
        $supplier_id      = $fn->getReqParam('supplier_id');
        $section_id       = $fn->getReqParam('section_id');
        $record_type      = $fn->getReqParam('record_type');
        $category_id      = $fn->getReqParam('category_id');
        $product_category = $fn->getReqParam('product_category');
        $start_date   	  = $fn->getReqParam('start_date');
        $end_date 	  	  = $fn->getReqParam('end_date');
        $product_name     = $fn->getReqParam('product_name');
        $order_status     = $fn->getReqParam('order_status');

        /*if ($start_date == '') {
            $start_date = date('Y-m-d', mktime (0,0,0,date("m")-6, date("d"), date("Y")));
        }

        if ($end_date == '') {
            $end_date = date('Y-m-d');
        }*/

        $searchFldsArr = $reportsArray[$report]['searchFlds'];

        $typeArr = array (
                'Quote'
               );

        if ($year == '') {
            $year = date('Y');
        }

        $arr = array (
                '01' => 'January'
               ,'02' => 'February'
               ,'03' => 'March'
               ,'04' => 'April'
               ,'05' => 'May'
               ,'06' => 'June'
               ,'07' => 'July'
               ,'08' => 'August'
               ,'09' => 'September'
               ,'10' => 'October'
               ,'11' => 'November'
               ,'12' => 'December'
               );

        if ($month == '') {
            $month = date('m');
        }

        $rows = '';
        foreach($searchFldsArr AS $searchFld){

            if ($report == 'dailyCollectionReport'){

                $rows .= "
                <td class='dateRange'>
                    Date Range:
                    <input type='text' allowEdit='1' name='start_date' class='fld_date'
                    id='fld_start_date' value='{$start_date}' />
                    <input type='text' allowEdit='1' name='end_date' class='fld_date'
                    id='fld_end_date' value='{$end_date}' />
                </td>
                ";
            }

            if ($report == 'topCustomerOutstanding'){

                /*$rows .= "
                <td class='dateRange'>
                    Date Range:
                    <input type='text' allowEdit='1' name='start_date' class='fld_date'
                    id='fld_start_date' value='{$start_date}' />
                    <input type='text' allowEdit='1' name='end_date' class='fld_date'
                    id='fld_end_date' value='{$end_date}' />
                </td>
                ";*/
            }

            if ($report == 'financeReport'){

                $sqlYear = "
                SELECT DISTINCT DATE_FORMAT(cf.date, '%Y') AS contact_date
                FROM  customer_finance cf where cf.date != ''
                ";

                $rows .= "
                <td>
                    <select name='year' class='leadStaffYearFilter'>
                        <option value=''>Choose Year</option>
                        {$dbUtil->getDropDownFromSQLCols1($db, $sqlYear, $year)}
                    </select>
                </td>
                ";

                $rows .= "
                <td>
                    <select name='month' class='ml10 mr10 month'>
                        <option value=''>Choose Month</option>
                        {$cpUtil->getDropDownFromArr($arr, $month)}
                    </select>
                </td>
                ";
            }
        }

        $text = "
        <form id='reportSearch'>
        <table class='search'>
            <tr>
                <td class='resetLink'><a href='javascript:void(0);' onClick=\"javascript:$('#reportSearch').clearForm();\">reset</a></td>
                {$rows}
                <td>
                    <input type='hidden' name='report' value='{$report}'>
                    <input type='hidden' id='reportName' value='{$report}'>
                    <input type='submit' value='GO' class='button'>
                </td>
            </tr>
        </table>
        </form>
        <script>
        </script>
        ";

        return $text;
    }

    function getDisplayReport($text){
        $fn = Zend_Registry::get('fn');
        $pager = Zend_Registry::get('pager');
        $report = $fn->getReqParam('report');

        $searchQueryString = $pager->removeQueryString(array("_spAction"));
        $exportLink = "{$searchQueryString}&_spAction=exportData&report={$report}&showHTML=0";

        $text = "
        <div>
            <a href='{$exportLink}' class='exportLink button'>
                <u1>Export to Excel</u1>
            </a>
            {$text}
        </div>
        ";

        return $text;

        $json = array();
        $json['html'] = $text;

        return json_encode($json);
    }


    /**
     *
     */
    function getMonthFilterValues() {
        return "
        <option value=''>Month Filter</option>
        <option value='01'>January</option>
        <option value='02'>February</option>
        <option value='03'>March</option>
        <option value='04'>April</option>
        <option value='05'>May</option>
        <option value='06'>June</option>
        <option value='07'>July</option>
        <option value='08'>August</option>
        <option value='09'>September</option>
        <option value='10'>October</option>
        <option value='11'>November</option>
        <option value='12'>December</option>
        ";
    }

}