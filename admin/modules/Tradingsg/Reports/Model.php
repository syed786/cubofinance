<?
class CPL_Admin_Modules_Tradingsg_Reports_Model extends CP_Admin_Modules_Tradingsg_Reports_Model
{
    var $reportsArray = array();

    function __construct() {
        $cpUtil  = Zend_Registry::get('cpUtil');

        $this->reportsArray = array(
            'dailyCollectionReport' => $this->getReportObj('dailyCollectionReport', 'Daily Collection Report')
           ,'investorReport'        => $this->getReportObj('investorReport', 'Investor Report')
           ,'topCustomerOutstanding' => $this->getReportObj('topCustomerOutstanding', 'Customer Outstanding')
           ,'financeReport' => $this->getReportObj('financeReport', 'Finance Report')
        );

    }

    function getReportObj($name, $title, $searchFlds = array('dateRange')) {

        //searchFldType: uptoDate, dateRange, activeRange
        $arr = array(
             'name' => $name
            ,'title' => $title
            ,'searchFlds' => $searchFlds
        );

        return $arr;
    }
    /**
     *
     */
     function getIncomeByCourse($SQLNeeded = '') {
        $db = Zend_Registry::get('db');
        $fn = Zend_Registry::get('fn');
        $text = "";
        $rows = "";
        $sqlStartDate = "";
        $sqlEndDate = "";

        $start_date = $fn->getReqParam('start_date');
        $end_date   = $fn->getReqParam('end_date');
        $status     = $fn->getReqParam('specialSearch');

        if ($status == ''){
            $status = 'Due';
        }

        if ($start_date != ''){
            $sqlStartDate = " AND o.creation_date >= '{$start_date}'";
        }

        if ($end_date != ''){
            $sqlEndDate = " AND o.creation_date <= '{$end_date}'";
        }

        //$SQL =  $this->getTraineeByCourseSQL();

        $SQL = "
        SELECT ABS( ABS( SUM( oi.unit_price ) ) ) AS total
              ,c.title as course_title
        FROM `order` o
        JOIN order_item oi ON oi.order_id = o.order_id
        LEFT JOIN course c ON c.course_id = oi.record_id
        WHERE o.order_status = '{$status}'
        {$sqlStartDate}
        {$sqlEndDate}
        GROUP BY oi.record_id
        ORDER BY course_title
        ";

        if ($SQLNeeded == 1){
            return $SQL;
        }

        $result = $db->sql_query($SQL);
        $resultTable = $db->sql_query($SQL);

        $rows = array(
         'course_title'
        ,'total'
        );

        $columns = array(
        'Course'
        ,'Total'
        );

        $text .= $fn->getTableRowsColumns($resultTable, $rows, $columns);

        return $text;
    }

    /**
     *
     */
    function getCategoryByProductJSON(){
        $db = Zend_Registry::get('db');
        $fn = Zend_Registry::get('fn');

        $rows = "";
        $category  = $fn->getReqParam('category');

        $json  = array();
        
        if ($category == ""){
            return json_encode($json);
        }
        else{
          if($category == 'GOLD'){
              $appendSql = "AND c.title IN ('GOLD - 916/KDM', 'GOLD - NON.KDM')";
          }
          else{
              $appendSql = "AND c.title IN ('SILVER', 'SILVER - 92.5')";
          }
        }

        $SQL = "
        SELECT p.title
        FROM product p
        LEFT JOIN (product_company pc) ON (pc.product_id = p.product_id)
        LEFT JOIN (category c)         ON (c.category_id = p.category_id)
        WHERE p.published = 1
        {$appendSql}
        GROUP BY UPPER(p.title)
        ";
        $result   = $db->sql_query($SQL);  

        $json[] = array("value" => "", "caption" => "Select Product");
        while ($row = $db->sql_fetchrow($result)) {
            $json[] = array("value" => $row['title'], "caption" => $row['title']);
        }
        
        return json_encode($json);
    }

}
