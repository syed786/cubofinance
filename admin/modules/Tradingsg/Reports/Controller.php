<?
class CPL_Admin_Modules_Tradingsg_Reports_Controller extends CP_Admin_Modules_Tradingsg_Reports_Controller
{
    function getSearch(){
        return $this->view->getSearch();
    }

    function getDisplayReport(){
        $fn = Zend_Registry::get('fn');

        set_time_limit(50000);
        $report = $fn->getReqParam('report');
        $fnName = 'get' . ucfirst($report);
        $text = $this->$fnName();
        return $this->view->getDisplayReport($text);
    }

    function getExportData(){
        $fn = Zend_Registry::get('fn');

        set_time_limit(50000);
        $report = $fn->getReqParam('report');
        $fnName = 'get' . ucfirst($report) . 'Export';
        return $this->$fnName();
    }

    function getDailyCollectionReport() {
        $wDailyCollectionReport = getCPWidgetObj('tradingsg_dailyCollectionReport');
        return $wDailyCollectionReport->getWidget();
    }

    function getDailyCollectionReportExport() {
        $wDailyCollectionReport = getCPWidgetObj('tradingsg_dailyCollectionReport');
        return $wDailyCollectionReport->model->getExportToExcel();
    }

    function getInvestorReport() {
        $wInvestorReport = getCPWidgetObj('tradingsg_investorReport');
        return $wInvestorReport->getWidget();
    }

    function getInvestorReportExport() {
        $wInvestorReport = getCPWidgetObj('tradingsg_investorReport');
        return $wInvestorReport->model->getExportToExcel();
    }

    function getTopCustomerOutstanding() {
        $wTopCustomerOutstanding = getCPWidgetObj('tradingsg_topCustomerOutstanding');
        return $wTopCustomerOutstanding->getWidget();
    }

    function getTopCustomerOutstandingExport() {
        $wTopCustomerOutstanding = getCPWidgetObj('tradingsg_topCustomerOutstanding');
        return $wTopCustomerOutstanding->model->getExportToExcel();
    }

    function getFinanceReport() {
        $wFinanceReport = getCPWidgetObj('tradingsg_financeReport');
        return $wFinanceReport->getWidget();
    }

    function getFinanceReportExport() {
        $wFinanceReport = getCPWidgetObj('tradingsg_financeReport');
        return $wFinanceReport->model->getExportToExcel();
    }
}