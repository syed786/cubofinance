<? 

$LANGARR = array();

$LANGARR['cp.email.footerText'] = "<a href=\"http://sample2.testpilotweb.com\">Visit our website</a>";
$LANGARR['cp.footer.leftText'] = "© 2010, All rights reserved 
<a class=\'jqui-dialog\' href=\'/index.php?module=webBasic_content&_spAction=spContent&ct=Disclaimer&showHTML=0\'>Disclaimer</a> |
<a class=\'jqui-dialog\' href=\'/index.php?module=webBasic_content&_spAction=spContent&ct=Privacy+Policy&showHTML=0\'>Privacy Policy</a>
";
$LANGARR['cp.footer.rightText'] = "";
$LANGARR['cp.form.btn.cancel'] = "Cancel";
$LANGARR['cp.form.btn.submit'] = "Submit";
$LANGARR['cp.form.fld.address1.lbl'] = "Address 1";
$LANGARR['cp.form.fld.address2.lbl'] = "Address 2";
$LANGARR['cp.form.fld.addressCountry.lbl'] = "Country";
$LANGARR['cp.form.fld.antiSpamCode.lbl'] = "Anti Spam Code";
$LANGARR['cp.form.fld.area.lbl'] = "Area";
$LANGARR['cp.form.fld.captchaCode.err'] = "Invalid Anti Spam Code";
$LANGARR['cp.form.fld.city.lbl'] = "City/Town";
$LANGARR['cp.form.fld.comments.err'] = "Please enter the message";
$LANGARR['cp.form.fld.comments.lbl'] = "Comments";
$LANGARR['cp.form.fld.companyName.lbl'] = "Company Name";
$LANGARR['cp.form.fld.country.err'] = "Please select the country";
$LANGARR['cp.form.fld.email.err'] = "Please enter the email address";
$LANGARR['cp.form.fld.email.lbl'] = "Email";
$LANGARR['cp.form.fld.firstName.err'] = "Please enter the first name";
$LANGARR['cp.form.fld.firstName.lbl'] = "First Name";
$LANGARR['cp.form.fld.lastName.err'] = "Please enter the last name";
$LANGARR['cp.form.fld.lastName.lbl'] = "Last Name";
$LANGARR['cp.form.fld.mobile.lbl'] = "Mobile";
$LANGARR['cp.form.fld.password.err'] = "Please enter the password";
$LANGARR['cp.form.fld.password.lbl'] = "Password";
$LANGARR['cp.form.fld.phone.err'] = "Please enter the phone";
$LANGARR['cp.form.fld.phone.lbl'] = "Phone";
$LANGARR['cp.form.fld.poCode.lbl'] = "Zip Code";
$LANGARR['cp.form.fld.state.lbl'] = "State";
$LANGARR['cp.form.lbl.pleaseSelect'] = "Please Select";
$LANGARR['cp.form.mandatoryInfo'] = "<span class=\"required\">All fields marked <em class=\"requiredStar\">*</em> are compulsory.</span>";
$LANGARR['cp.lbl.back'] = "Back To List";
$LANGARR['cp.lbl.more'] = "more";
$LANGARR['cp.pager.displaying'] = "Displaying";
$LANGARR['cp.pager.next'] = "Next >";
$LANGARR['cp.pager.of'] = "of";
$LANGARR['cp.pager.page'] = "Page";
$LANGARR['cp.pager.previous'] = "< Previous";
$LANGARR['cp.pager.to'] = "to";
$LANGARR['feedbackForm'] = "Feedback Form";
$LANGARR['m.webBasic.contactUs.form.enquiry.email.notifyBody'] = "<table>

<tr>
    <td colspan=\"2\"><u><b>Web Enquiry - Email</b></u></td>
</tr>

<tr>
   <td>First Name</td>
   <td>[[first_name]]</td>
</tr>

<tr>
   <td>Last Name</td>
   <td>[[last_name]]</td>
</tr>

<tr>
   <td>E-mail Address</td>
   <td>[[email]]</td>
</tr>

<tr>
   <td>Country</td>
   <td>[[country]]</td>
</tr>

<tr>
   <td>Comments</td>
   <td>[[comments]]</td>
</tr>

<tr>
   <td>Submitted On</td>
   <td>[[currentDate]]</td>
</tr>

</table>";
$LANGARR['m.webBasic.contactUs.form.enquiry.email.notifySubject'] = "Web Enquiry";
$LANGARR['m.webBasic.contactUs.form.enquiry.heading'] = "Enquiry Form";
$LANGARR['m.webBasic.contactUs.form.enquiry.message.success'] = "<p>Thanks for contacting us. We will respond to your enquiry as soon as possible</p>

<br />
Please <a href=\"/\">click here</a> to return to the homepage
";
$LANGARR['message'] = "Message";
$LANGARR['p.common.siteSearch.btn.search'] = "Search";
$LANGARR['p.common.siteSearch.form.search.fld.keyword.lbl'] = "Keyword";
$LANGARR['p.common.siteSearch.lbl.page'] = "Page";
$LANGARR['p.common.siteSearch.lbl.searchResultsFor'] = "Search Results for";
$LANGARR['p.common.siteSearch.lbl.section'] = "Section";
$LANGARR['p.common.siteSearch.lbl.title'] = "Title";
$LANGARR['p.common.siteSearch.message.noRecords'] = "Sorry. No results found.";
$LANGARR['w.content.record.whatsnew.heading'] = "What\'s New";
$LANGARR['w.member.loginForm.form.lbl.login'] = "Login";

/*** FROM VALUE LIST TABLE ***/
$LANGARR['New'] = "New";
$LANGARR['Old'] = "Old";
