<? 

$LANGARR = array();

$LANGARR['cp.form.lbl.pleaseSelect'] = "Please Select";
$LANGARR['cp.label.delete'] = "Delete";
$LANGARR['cp.label.discountAmount'] = "Discount Amt";
$LANGARR['cp.label.discountType'] = "Discount Type";
$LANGARR['cp.label.itemCode'] = "Item Code";
$LANGARR['cp.label.itemName'] = "Item Name";
$LANGARR['cp.label.qty'] = "Qty";
$LANGARR['cp.label.serialNo'] = "S.No";
$LANGARR['cp.label.total'] = "Total";
$LANGARR['cp.label.unit'] = "UOM";
$LANGARR['cp.label.unitPrice'] = "Unit Price";
