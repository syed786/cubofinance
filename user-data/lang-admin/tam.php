<? 

$LANGARR = array();

$LANGARR['cp.form.lbl.pleaseSelect'] = "Please Select";
$LANGARR['cp.label.delete'] = "அழி";
$LANGARR['cp.label.discountAmount'] = "தள்ளுபடி தொகை";
$LANGARR['cp.label.discountType'] = "தள்ளுபடி வகை";
$LANGARR['cp.label.itemCode'] = "பொருளின் குறியீடு";
$LANGARR['cp.label.itemName'] = "பொருளின் பெயர்";
$LANGARR['cp.label.qty'] = "அளவு";
$LANGARR['cp.label.serialNo'] = "வ.எண்";
$LANGARR['cp.label.total'] = "மொத்த தொகை";
$LANGARR['cp.label.unit'] = "அலகு";
$LANGARR['cp.label.unitPrice'] = "அலகு விலை";
