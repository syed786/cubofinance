<? 

$LANGARR = array();

$LANGARR['cp.email.footerText'] = "<a href=\"http://sample2.testpilotweb.com\">Visit our website</a>";
$LANGARR['cp.footer.leftText'] = "© 2010, All rights reserved 
<a class='jqui-dialog' href='/index.php?module=webBasic_content&_spAction=spContent&ct=Disclaimer&showHTML=0'>Disclaimer</a> |
<a class='jqui-dialog' href='/index.php?module=webBasic_content&_spAction=spContent&ct=Privacy+Policy&showHTML=0'>Privacy Policy</a>
";
$LANGARR['cp.footer.rightText'] = "";
$LANGARR['cp.form.btn.cancel'] = "Cancel";
$LANGARR['cp.form.btn.submit'] = "Submit";
$LANGARR['cp.form.fld.address1.err'] = "Please enter address1";
$LANGARR['cp.form.fld.address1.lbl'] = "Address 1";
$LANGARR['cp.form.fld.address2.err'] = "Please enter address2";
$LANGARR['cp.form.fld.address2.lbl'] = "Address 2";
$LANGARR['cp.form.fld.addressArea.err'] = "Please enter the area";
$LANGARR['cp.form.fld.addressCity.err'] = "Please enter the city";
$LANGARR['cp.form.fld.addressCountry.lbl'] = "Country";
$LANGARR['cp.form.fld.antiSpamCode.lbl'] = "Anti Spam Code";
$LANGARR['cp.form.fld.area.lbl'] = "Area";
$LANGARR['cp.form.fld.captchaCode.err'] = "Invalid Anti Spam Code";
$LANGARR['cp.form.fld.city.lbl'] = "City/Town";
$LANGARR['cp.form.fld.comments.err'] = "Please enter the message";
$LANGARR['cp.form.fld.comments.lbl'] = "Comments";
$LANGARR['cp.form.fld.companyName.lbl'] = "Company Name";
$LANGARR['cp.form.fld.confirmPassword.lbl'] = "Confirm Password";
$LANGARR['cp.form.fld.country.err'] = "Please select the country";
$LANGARR['cp.form.fld.email.err'] = "Please enter the email address";
$LANGARR['cp.form.fld.email.err.alreadyExists'] = "Sorry, the email you have entered already exists in our database";
$LANGARR['cp.form.fld.email.err.notFound'] = "Sorry, the email address you entered is not found in our database.";
$LANGARR['cp.form.fld.email.lbl'] = "Email";
$LANGARR['cp.form.fld.firstName.err'] = "Please enter the first name";
$LANGARR['cp.form.fld.firstName.lbl'] = "First Name";
$LANGARR['cp.form.fld.lastName.err'] = "Please enter the last name";
$LANGARR['cp.form.fld.lastName.lbl'] = "Last Name";
$LANGARR['cp.form.fld.mobile.lbl'] = "Mobile";
$LANGARR['cp.form.fld.password.err'] = "Please enter the password";
$LANGARR['cp.form.fld.password.lbl'] = "Password";
$LANGARR['cp.form.fld.phone.err'] = "Please enter the phone";
$LANGARR['cp.form.fld.phone.lbl'] = "Phone";
$LANGARR['cp.form.fld.poCode.lbl'] = "Zip Code";
$LANGARR['cp.form.fld.state.lbl'] = "State";
$LANGARR['cp.form.lbl.pleaseSelect'] = "Please Select";
$LANGARR['cp.form.lgnd.addressDetails'] = "Address Details";
$LANGARR['cp.form.mandatoryInfo'] = "<span class=\"required\">All fields marked <em class=\"requiredStar\">*</em> are compulsory.</span>";
$LANGARR['cp.lbl.back'] = "Back To List";
$LANGARR['cp.lbl.backToList'] = "Back To List";
$LANGARR['cp.lbl.more'] = "more";
$LANGARR['cp.pager.displaying'] = "Displaying";
$LANGARR['cp.pager.next'] = "Next >";
$LANGARR['cp.pager.of'] = "of";
$LANGARR['cp.pager.page'] = "Page";
$LANGARR['cp.pager.previous'] = "< Previous";
$LANGARR['cp.pager.to'] = "to";
$LANGARR['feedbackForm'] = "Feedback Form";
$LANGARR['m.ecommerce.basket.form.new.email.notifyBody'] = "        <table>

        <tr>
            <td colspan='2'><u><b>New Order</b></u></td>
        </tr>

        <tr>
           <td>First Name</td>
           <td>[[first_name]]</td>
        </tr>

        <tr>
           <td>Last Name</td>
           <td>[[last_name]]</td>
        </tr>

        <tr>
           <td>E-mail Address</td>
           <td>[[email]]</td>
        </tr>

        <tr>
           <td>Phone</td>
           <td>[[phone]]</td>
        </tr>

        <tr>
           <td>Address1</td>
           <td>[[address1]]</td>
        </tr>

        <tr>
           <td>Address2</td>
           <td>[[address2]]</td>
        </tr>

        <tr>
           <td>Area</td>
           <td>[[address_area]]</td>
        </tr>

        <tr>
           <td>City</td>
           <td>[[address_city]]</td>
        </tr>

        <tr>
           <td>State</td>
           <td>[[address_state]]</td>
        </tr>

        <tr>
           <td>Country</td>
           <td>[[address_country]]</td>
        </tr>

        <tr>
           <td>Submitted On</td>
           <td>[[currentDate]]</td>
        </tr>
        </table>";
$LANGARR['m.ecommerce.basket.form.new.email.notifySubject'] = "New Order from the Website";
$LANGARR['m.membership.contact.form.new.email.notifyBody'] = "<table>

<tr>
    <td colspan=\"2\"><u><b>New Member Registration Info</b></u></td>
</tr>

<tr>
   <td>First Name</td>
   <td>[[first_name]]</td>
</tr>

<tr>
   <td>Last Name</td>
   <td>[[last_name]]</td>
</tr>

<tr>
   <td>E-mail Address</td>
   <td>[[email]]</td>
</tr>

<tr>
   <td>Submitted On</td>
   <td>[[currentDate]]</td>
</tr>

</table>";
$LANGARR['m.membership.contact.form.new.email.notifySubject'] = "New Member Registration";
$LANGARR['m.membership.contact.form.new.email.notifyUserBody'] = "<html>
<body>
<style>
body {
   font: 12px Arial;
}
</style>

Dear [[first_name]] [[last_name]],<br> <br>

You have succesfully registered with Gorilla glue.<br><br>
Your  Login is: [[email]] <br>
Your Password is: [[pass_word]] <br><br>

To start shopping <a href=\"[[activation_link]]\"><u>click here</u></a>.<br><br>

Remember to keep checking back for news of our latest offers.<br><br>

Gorilla glue team     

</body>
</html>";
$LANGARR['m.membership.contact.form.new.email.notifyUserSubject'] = "Thanks for your registration";
$LANGARR['m.membership.contact.form.new.heading'] = "Register with Us Now!";
$LANGARR['m.membership.contact.form.new.info'] = "<p>Dear our valued customer</p>
<p>Please fill in your details so that we can make it easier for you when purchasing again.</p>
<p>If you are having difficulties in accessing your profile please do not hesitate to contact us on <a href=\"mailto:info@mycompany.com\">info@mycompany.com</a></p>
";
$LANGARR['m.membership.contact.form.new.message.sucecss'] = "Thank you for registering with us. 
<br>
<div class='button'>
<a href='/eng/login/'>Login Now</a>
</div>";
$LANGARR['m.webBasic.contactUs.form.enquiry.email.notifyBody'] = "<table>

<tr>
    <td colspan=\"2\"><u><b>Web Enquiry - Email</b></u></td>
</tr>

<tr>
   <td>First Name</td>
   <td>[[first_name]]</td>
</tr>

<tr>
   <td>Last Name</td>
   <td>[[last_name]]</td>
</tr>

<tr>
   <td>E-mail Address</td>
   <td>[[email]]</td>
</tr>

<tr>
   <td>Country</td>
   <td>[[country]]</td>
</tr>

<tr>
   <td>Comments</td>
   <td>[[comments]]</td>
</tr>

<tr>
   <td>Submitted On</td>
   <td>[[currentDate]]</td>
</tr>

</table>";
$LANGARR['m.webBasic.contactUs.form.enquiry.email.notifySubject'] = "Web Enquiry";
$LANGARR['m.webBasic.contactUs.form.enquiry.heading'] = "Enquiry Form";
$LANGARR['m.webBasic.contactUs.form.enquiry.message.success'] = "<p>Thanks for contacting us. We will respond to your enquiry as soon as possible</p>

<br />
Please <a href=\"/\">click here</a> to return to the homepage
";
$LANGARR['message'] = "Message";
$LANGARR['p.common.siteSearch.btn.search'] = "Search";
$LANGARR['p.common.siteSearch.form.search.fld.keyword.lbl'] = "Keyword";
$LANGARR['p.common.siteSearch.lbl.page'] = "Page";
$LANGARR['p.common.siteSearch.lbl.searchResultsFor'] = "Search Results for";
$LANGARR['p.common.siteSearch.lbl.section'] = "Section";
$LANGARR['p.common.siteSearch.lbl.title'] = "Title";
$LANGARR['p.common.siteSearch.message.noRecords'] = "Sorry. No results found.";
$LANGARR['p.member.forgotPassword.form.heading'] = "Recover My Password";
$LANGARR['p.member.forgotPassword.form.info'] = "Please enter your email address and click submit. We will the send the instructions to recover your password to your email";
$LANGARR['p.member.login.form.err.invalidLogin'] = "Invalid email or password";
$LANGARR['p.member.login.lbl.welcome'] = "Welcome";
$LANGARR['p.paymentMethods.lbl.paypal'] = "Paypal";
$LANGARR['w.content.record.whatsnew.heading'] = "What's New";
$LANGARR['w.ecommerce.addToCart.lbl.viewBasket'] = "View Basket";
$LANGARR['w.ecommerce.basket.btn.buyNow'] = "Buy Now";
$LANGARR['w.ecommerce.basket.btn.checkout'] = "Checkout";
$LANGARR['w.ecommerce.basket.btn.confirmOrder'] = "Confirm Order";
$LANGARR['w.ecommerce.basket.btn.continueShopping'] = "Continue Shopping";
$LANGARR['w.ecommerce.basket.btn.editDetails'] = "Edit Details";
$LANGARR['w.ecommerce.basket.btn.emptyCart'] = "Empty Cart";
$LANGARR['w.ecommerce.basket.btn.saveContinue'] = "Save & Continue";
$LANGARR['w.ecommerce.basket.emptyCart.info'] = "Sorry.. Your basket is empty. Please click \"Continue Shopping\" to add items into the basket.";
$LANGARR['w.ecommerce.basket.heading'] = "Shopping Basket";
$LANGARR['w.ecommerce.basket.lbl.grossTotal'] = "Gross Total";
$LANGARR['w.ecommerce.basket.lbl.netTotal'] = "Net Total";
$LANGARR['w.ecommerce.basket.lbl.picture'] = "Picture";
$LANGARR['w.ecommerce.basket.lbl.quantity'] = "Quantity";
$LANGARR['w.ecommerce.basket.lbl.shippingCharge'] = "Shipping Charge";
$LANGARR['w.ecommerce.basket.lbl.title'] = "Title";
$LANGARR['w.ecommerce.basket.lbl.total'] = "Total";
$LANGARR['w.ecommerce.basket.lbl.unitPrices'] = "Unit Price";
$LANGARR['w.ecommerce.confirmOrder.heading'] = "Confirm Order";
$LANGARR['w.ecommerce.paymentMethods.heading'] = "Payment Methods";
$LANGARR['w.ecommerce.shippingDetails.form.heading'] = "Shipping Details";
$LANGARR['w.ecommerce.shippingDetails.form.lgnd.personalDetails'] = "Personal Details";
$LANGARR['w.member.changePassword.form.fld.newPassword.lbl'] = "New Password";
$LANGARR['w.member.changePassword.form.fld.oldPassword.err'] = "The old password you have entered is not correct";
$LANGARR['w.member.changePassword.form.fld.oldPassword.lbl'] = "Old Password";
$LANGARR['w.member.changePassword.form.fld.password.err.compare'] = "Password & confirm password are not the same";
$LANGARR['w.member.changePassword.form.fld.password.err.length'] = "Please enter the password";
$LANGARR['w.member.changePassword.heading'] = "Change Password";
$LANGARR['w.member.changePassword.info'] = "To change your password, please enter the information in the boxes below. Passwords should be a minimum of 6 characters (and maximum of 20), either alpha or numeric or a combination of both. ";
$LANGARR['w.member.changePassword.message.success'] = "Your password has been changed successfully..";
$LANGARR['w.member.loginForm.btn.register'] = "Create My Account";
$LANGARR['w.member.loginForm.form.lbl.forgotPassword'] = "Lost my password";
$LANGARR['w.member.loginForm.form.lbl.login'] = "Login";
$LANGARR['w.member.loginForm.form.lbl.userLogin'] = "User Login";
$LANGARR['w.member.loginForm.heading'] = "Returning User";
$LANGARR['w.member.loginForm.info'] = "Please enter your email and password <br><br>

<p><strong>If you are having difficulties in accessing your profile please do not hesitate to contact us on <a href=\"mailto:info@mycompany.com\">info@mycompany.com</a> </strong></p>
";
$LANGARR['w.member.loginForm.lbl.saveLogin'] = "Save Login";
$LANGARR['w.member.loginForm.register.heading'] = "New User";
$LANGARR['w.member.loginForm.register.info'] = "if you don't have an account you can create a new profile here";
$LANGARR['w.membership.contact.form.fld.newsletterSubscribed.lbl'] = "Subscribed for Newsletter";

/*** FROM VALUE LIST TABLE ***/
$LANGARR['Paid'] = "Subscribed for Newsletter";
$LANGARR['Due'] = "Subscribed for Newsletter";
$LANGARR['Cancelled'] = "Subscribed for Newsletter";
$LANGARR['Tele Call'] = "Subscribed for Newsletter";
$LANGARR['Direct Meeting'] = "Subscribed for Newsletter";
$LANGARR['Automotive Industry'] = "Subscribed for Newsletter";
$LANGARR['
Pharmaceautical'] = "Subscribed for Newsletter";
$LANGARR['Medical'] = "Subscribed for Newsletter";
$LANGARR['Advertisement'] = "Subscribed for Newsletter";
$LANGARR['Website'] = "Subscribed for Newsletter";
$LANGARR['Directory'] = "Subscribed for Newsletter";
$LANGARR['Refferal'] = "Subscribed for Newsletter";
$LANGARR['Not Interested'] = "Subscribed for Newsletter";
$LANGARR['Follow up'] = "Subscribed for Newsletter";
$LANGARR['High Win Ratio'] = "Subscribed for Newsletter";
$LANGARR['Converted to Sales'] = "Subscribed for Newsletter";
$LANGARR['Supplier'] = "Subscribed for Newsletter";
$LANGARR['Client'] = "Subscribed for Newsletter";
$LANGARR['Fully Automatic'] = "Subscribed for Newsletter";
$LANGARR['Partly Automatic'] = "Subscribed for Newsletter";
$LANGARR['Large'] = "Subscribed for Newsletter";
$LANGARR['Small'] = "Subscribed for Newsletter";
$LANGARR['Direct'] = "Subscribed for Newsletter";
$LANGARR['Referrel'] = "Subscribed for Newsletter";
$LANGARR['Highly secure'] = "Subscribed for Newsletter";
$LANGARR['Authentic'] = "Subscribed for Newsletter";
$LANGARR['Mr'] = "Subscribed for Newsletter";
$LANGARR['Mrs'] = "Subscribed for Newsletter";
$LANGARR['Miss'] = "Subscribed for Newsletter";
$LANGARR['USD'] = "Subscribed for Newsletter";
$LANGARR['INR'] = "Subscribed for Newsletter";
$LANGARR['SAR'] = "Subscribed for Newsletter";
$LANGARR['EUR'] = "Subscribed for Newsletter";
$LANGARR['STANDARD'] = "Subscribed for Newsletter";
$LANGARR['GOLD'] = "Subscribed for Newsletter";
$LANGARR['PREMIUM'] = "Subscribed for Newsletter";
$LANGARR['SPECIAL'] = "Subscribed for Newsletter";
$LANGARR['Location 1'] = "Subscribed for Newsletter";
$LANGARR['Location 2'] = "Subscribed for Newsletter";
$LANGARR['Location 3'] = "Subscribed for Newsletter";
$LANGARR['In Progress'] = "Subscribed for Newsletter";
$LANGARR['Client Confirmed'] = "Subscribed for Newsletter";
$LANGARR['On Hold'] = "Subscribed for Newsletter";
$LANGARR['Cancelled'] = "Subscribed for Newsletter";
$LANGARR['Completed'] = "Subscribed for Newsletter";
$LANGARR['Due'] = "Subscribed for Newsletter";
$LANGARR['Paid'] = "Subscribed for Newsletter";
$LANGARR['Late'] = "Subscribed for Newsletter";
$LANGARR['Cash'] = "Subscribed for Newsletter";
$LANGARR['Cheque'] = "Subscribed for Newsletter";
$LANGARR['Credit Card'] = "Subscribed for Newsletter";
$LANGARR['GIRO'] = "Subscribed for Newsletter";
$LANGARR['KGS'] = "Subscribed for Newsletter";
$LANGARR['PCS'] = "Subscribed for Newsletter";
$LANGARR['Mtrs'] = "Subscribed for Newsletter";
$LANGARR['EA'] = "Subscribed for Newsletter";
$LANGARR['Gms'] = "Subscribed for Newsletter";
$LANGARR['Low'] = "Subscribed for Newsletter";
$LANGARR['Medium'] = "Subscribed for Newsletter";
$LANGARR['High'] = "Subscribed for Newsletter";
$LANGARR['General Quotation'] = "Subscribed for Newsletter";
$LANGARR['Requirement from Client'] = "Subscribed for Newsletter";
$LANGARR['Bulk Order'] = "Subscribed for Newsletter";
$LANGARR['Small Order'] = "Subscribed for Newsletter";
